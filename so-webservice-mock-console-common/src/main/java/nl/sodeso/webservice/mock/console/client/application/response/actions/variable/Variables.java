package nl.sodeso.webservice.mock.console.client.application.response.actions.variable;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionConfig;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class Variables extends WidgetCollectionField<Variable> {

    public Variables() {
        super(new WidgetCollectionConfig().setEnableSwitching(true));
    }

    @Override
    public WidgetSupplier<Variable> createWidgetSupplier() {
        return callback -> callback.created(new Variable());
    }
}
