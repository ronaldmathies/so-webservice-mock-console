package nl.sodeso.webservice.mock.console.client.application.upload.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadedEndpointDetails;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadEndpointWizardContext;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("webservicemockconsole/endpoint.webservicemock-import")
public interface UploadRpc extends XsrfProtectedService {

    UploadedEndpointDetails prepareEndpointImport(ServiceOption serviceOption, String uuid);
    Boolean finishEndpointImport(ServiceOption serviceOption, UploadEndpointWizardContext context);
}
