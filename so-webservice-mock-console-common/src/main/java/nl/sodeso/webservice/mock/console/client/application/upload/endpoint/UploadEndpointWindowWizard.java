package nl.sodeso.webservice.mock.console.client.application.upload.endpoint;

import nl.sodeso.gwt.ui.client.form.button.CloseButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.wizard.WizardWindow;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.steps.FileUploadEndpointStep;

import static nl.sodeso.webservice.mock.console.client.application.Resources.UPLOAD_ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class UploadEndpointWindowWizard extends WizardWindow<UploadEndpointWizardContext> {

    private static final String KEY_IMPORT = "import";

    private static UploadEndpointWindowWizard instance = null;

    public UploadEndpointWindowWizard() {
        super(new PopupWindowPanel(KEY_IMPORT, UPLOAD_ENDPOINT_I18N.title(), WindowPanel.Style.INFO), new UploadEndpointWizardContext());
        getWindow().showCollapseButton(true);
        getWindow().setWidth("800px");

        getWindow().addToFooter(Align.LEFT, new CloseButton.WithLabel((event) -> setVisible(false)));
    }

    public static UploadEndpointWindowWizard instance() {
        if (instance == null) {
            instance = new UploadEndpointWindowWizard();
        }

        return instance;
    }

    public void resetWizard() {
        setWizardStep(new FileUploadEndpointStep(null));
    }

}
