package nl.sodeso.webservice.mock.console.client.application.response.conditions.body;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionOption;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Condition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import static nl.sodeso.webservice.mock.console.client.application.Resources.BODY_CONDITION_I18N;

/**
 * @author Ronald Mathies
 */
public class BodyCondition extends Condition {

    private static final String KEY_BODY_CONDITION = "body-condition";
    private static final String KEY_EXPRESSION_TYPE = "expression-type";
    private static final String KEY_EXPRESSION = "expression";
    private static final String KEY_MATCHES = "matches";

    private OptionType<ExpressionOption> expressionOption = new OptionType<>(ExpressionOption.XPATH);
    private StringType expression = new StringType();
    private StringType matches = new StringType();

    private transient ExpressionComboboxField expressionComboboxField = null;
    private transient TextField<StringType> expressionField = null;
    private transient TextField<StringType> matchesField = null;


    private transient EntryWithSingleWidget entry = null;

    public BodyCondition() {
    }

    public ConditionOption getConditionOption() {
        return ConditionOption.BODY;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        expressionComboboxField = new ExpressionComboboxField(expressionOption, false);
        expressionComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_EXPRESSION_TYPE, ValidationMessage.Level.ERROR, expressionOption));

        expressionField = new TextField<>(KEY_EXPRESSION, this.expression);
        expressionField.addValidationRule(
                new MandatoryValidationRule(KEY_EXPRESSION, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return expressionField.getValue();
                    }
                }
        );

        matchesField = new TextField<>(KEY_MATCHES, this.matches);
        matchesField.addValidationRule(
                new MandatoryValidationRule(KEY_MATCHES, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return matchesField.getValue();
                    }
                }
        );
        EntryForm form = new EntryForm(null)
                .addEntries(
                        new EntryWithDocumentation(KEY_BODY_CONDITION, BODY_CONDITION_I18N.bodyConditionDocumentation()),
//                        new EntryWithDocumentation(KEY_EXPRESSION_TYPE, BODY_CONDITION_I18N.expressionTypeDocumentation()),
                        new EntryWithLabelAndWidget(KEY_EXPRESSION_TYPE, BODY_CONDITION_I18N.expressionType(), expressionComboboxField),
//                        new EntryWithDocumentation(KEY_EXPRESSION, BODY_CONDITION_I18N.expressionDocumentation()),
                        new EntryWithLabelAndWidget(KEY_EXPRESSION, BODY_CONDITION_I18N.expression(), expressionField),
                        new EntryWithLabelAndWidget(KEY_MATCHES, BODY_CONDITION_I18N.matches(), new InsertFunctionButton(matchesField))
                );

        expressionOption.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(expressionComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                form.setEntriesVisible(!ExpressionOption.NONE.equals(expressionOption.getValue()), KEY_EXPRESSION);
            }
        });

        LegendPanel legendPanel = new LegendPanel(null, BODY_CONDITION_I18N.bodyCondition());
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_BODY_CONDITION, legendPanel);
        return entry;
    }

    public OptionType<ExpressionOption> getExpressionOption() {
        return expressionOption;
    }

    public void setExpressionOption(OptionType<ExpressionOption> expressionOption) {
        this.expressionOption = expressionOption;
    }

    public StringType getExpression() {
        return expression;
    }

    public void setExpression(StringType expression) {
        this.expression = expression;
    }

    public StringType getMatches() {
        return matches;
    }

    public void setMatches(StringType matches) {
        this.matches = matches;
    }
}