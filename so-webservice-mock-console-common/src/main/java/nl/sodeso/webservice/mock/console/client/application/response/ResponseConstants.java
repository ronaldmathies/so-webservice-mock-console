package nl.sodeso.webservice.mock.console.client.application.response;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface ResponseConstants extends Messages {

    @DefaultMessage("Responses")
    String responses();

    @DefaultMessage("Responses")
    String responseMenuItem();

    @DefaultMessage("Response")
    String responsePanel();

    @DefaultMessage("Endpoint")
    String endpointField();

    @DefaultMessage("Name")
    String name();

    @DefaultMessage("Message")
    String message();

    @DefaultMessage("Type")
    String responseType();

    @DefaultMessage("HTTP Status Code")
    String status();

    @DefaultMessage("HTTP Content Type")
    String contentType();

    @DefaultMessage("The following field can be used to specify the body of the response.")
    String bodyDocumentation();

    @DefaultMessage("Body")
    String body();

    @DefaultMessage("Redirect URL")
    String redirectUrl();

    @DefaultMessage("Filename")
    String filename();

    @DefaultMessage("File")
    String file();

    @DefaultMessage("Base 64 Encoded Data")
    String data();

    @DefaultMessage("Add additional actions for creating or deleting cookies.")
    String cookiesDocumentation();

    @DefaultMessage("Cookies ({0})")
    String cookies(int numberOfCookies);

    @DefaultMessage("Add additional response headers here, these headers will be added to the response along with some headers that are created by default (for example the status code and content-type).")
    String headersDocumentation();

    @DefaultMessage("Conditions are used to determine which response should be given to what kind of request, it is important to fine-tune the conditions so that only one response is applicable to a request, if multiple responses are found then the first one it finds will be used.")
    String conditionsDocumentation();

    @DefaultMessage("Conditions ({0})")
    String conditions(int numberOfConditions);

    @DefaultMessage("Actions are used to influence the handling of the request.")
    String actionsDocumentation();

    @DefaultMessage("Actions ({0})")
    String actions(int numberOfActions);

    @DefaultMessage("Add additional placeholdersAction here.")
    String placeholdersActionDocumentation();

    @DefaultMessage("Placeholders Action")
    String placeholdersAction();

    @DefaultMessage("The timeout action will delay the response by the amount of seconds specified.")
    String timeoutActionDocumentation();

    @DefaultMessage("Timeout Action")
    String timeoutAction();

    @DefaultMessage("The exception action can be used to simulate a server side exception.")
    String exceptionActionDocumentation();

    @DefaultMessage("Exception Action")
    String exceptionAction();

    @DefaultMessage("Headers ({0})")
    String headers(int numberOfHeaders);

    @DefaultMessage("Query Values ({0})")
    String queryValues(int numberOfQueryValues);

    @DefaultMessage("Filter")
    String filter();

    @DefaultMessage("Select a specific endpoint to display all responses for that endpoint or select All to display all responses for all endpoints.")
    String endpointDocumentation();

    @DefaultMessage("Endpoint")
    String endpoint();

    @DefaultMessage("Domain")
    String domain();

    @DefaultMessage("Enable this option to prevent transfer of headers from the proxy request to the callers request.")
    String ignoreHeadersDocumentation();

    @DefaultMessage("Ignore headers")
    String ignoreHeaders();

    @DefaultMessage("Enable this option to prevent trasnfer of cookies from the proxy request to the callers request.")
    String ignoreCookiesDocumentation();

    @DefaultMessage("Ignore cookies")
    String ignoreCookies();

    @DefaultMessage("Connection timeout")
    String connectionTimeout();

    @DefaultMessage("Seconds")
    String connectionTimeoutHint();

    @DefaultMessage("Read timeout")
    String readTimeout();

    @DefaultMessage("Seconds")
    String readTimeoutHint();

    @DefaultMessage("Duplicate")
    String duplicateTitle();

    @DefaultMessage("Please enter a new name for the duplicate response:")
    String duplicateQuestion();
}