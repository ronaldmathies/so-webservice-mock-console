package nl.sodeso.webservice.mock.console.client.application.response.type;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class RedirectResponseType extends ResponseType {

    private static final String KEY_REDIRECT_URL = "redirect-url";

    private StringType redirectUrl = new StringType();

    private transient EntryForm form = null;
    private transient TextField<StringType> redirectUrlField = null;

    public RedirectResponseType() {}

    public EntryForm toForm() {
        if (form != null) {
            return form;
        }

        redirectUrlField = new TextField<>(KEY_REDIRECT_URL, redirectUrl);

        form = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_REDIRECT_URL, RESPONSE_I18N.redirectUrl(), new InsertFunctionButton(redirectUrlField)));

        return form;
    }

    public StringType getRedirectUrl() {
        return this.redirectUrl;
    }

    public void setRedirectUrl(StringType redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

}
