package nl.sodeso.webservice.mock.console.client.application.service;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Service implements Serializable {

    private String name;
    private String version;

    private String ipAddress;
    private int port;

    public Service() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
