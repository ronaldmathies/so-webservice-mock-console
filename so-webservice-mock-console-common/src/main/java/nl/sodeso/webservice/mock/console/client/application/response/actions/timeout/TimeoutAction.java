package nl.sodeso.webservice.mock.console.client.application.response.actions.timeout;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.response.actions.Action;
import nl.sodeso.webservice.mock.console.client.application.response.actions.ActionOption;

import static nl.sodeso.webservice.mock.console.client.application.Resources.TIMEOUT_I18N;

/**
 * @author Ronald Mathies
 */
public class TimeoutAction extends Action {

    private static final String KEY_TIMEOUT_ACTION = "timeout-action";
    private static final String KEY_SECONDS = "seconds";

    private StringType seconds = new StringType();
    private transient TextField<StringType> secondsTextField = null;

    private transient EntryWithSingleWidget entry = null;

    public TimeoutAction() {}

    public ActionOption getActionOption() {
        return ActionOption.TIMEOUT;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        secondsTextField = new TextField<>(KEY_SECONDS, seconds)
            .addValidationRules(
                new MandatoryValidationRule(KEY_SECONDS, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return secondsTextField.getValue();
                    }
                }
            );

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_SECONDS, TIMEOUT_I18N.secondsDocumentation()),
                new EntryWithLabelAndWidget(KEY_SECONDS, TIMEOUT_I18N.seconds(), secondsTextField).setHint(TIMEOUT_I18N.secondsHint())
            );

        LegendPanel legendPanel = new LegendPanel(null, "Timeout Action");
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_TIMEOUT_ACTION, legendPanel);
        return entry;    }

    public StringType getSeconds() {
        return seconds;
    }

    public void setSeconds(StringType seconds) {
        this.seconds = seconds;
    }
}
