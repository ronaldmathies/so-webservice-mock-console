package nl.sodeso.webservice.mock.console.client.application.response.actions;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionConfig;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplierCallback;
import nl.sodeso.webservice.mock.console.client.application.response.actions.exception.ExceptionAction;
import nl.sodeso.webservice.mock.console.client.application.response.actions.timeout.TimeoutAction;
import nl.sodeso.webservice.mock.console.client.application.response.actions.variable.VariableAction;

/**
 * @author Ronald Mathies
 */

public class Actions extends WidgetCollectionField<Action> {

    public Actions() {
        super(new WidgetCollectionConfig().setEnableSwitching(true));
    }

    @Override
    public WidgetSupplier<Action> createWidgetSupplier() {
        return (WidgetSupplierCallback<Action> callback) -> {
            final ChooseActionQuestionPanel questionPanel = new ChooseActionQuestionPanel(option -> {
                ActionOption actionOption = option.getValue();
                if (actionOption.equals(ActionOption.TIMEOUT)) {
                    callback.created(new TimeoutAction());
                } else if (actionOption.equals(ActionOption.EXCEPTION)) {
                    callback.created(new ExceptionAction());
                } else if (actionOption.equals(ActionOption.VARIABLE)) {
                    callback.created(new VariableAction());
                }
            }, option -> {
                for (Action action : getWidgets()) {

                    ActionOption actionOption = action.getActionOption();
                    if ((actionOption.equals(ActionOption.EXCEPTION) && option.equals(ActionOption.EXCEPTION)) ||
                            (actionOption.equals(ActionOption.TIMEOUT) && option.equals(ActionOption.TIMEOUT))) {
                        return false;
                    }

                }

                return true;
            });
            questionPanel.center(true, true);
        };
    }

}
