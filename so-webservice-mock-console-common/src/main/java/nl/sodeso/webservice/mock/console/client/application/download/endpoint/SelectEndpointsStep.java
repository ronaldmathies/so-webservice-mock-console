package nl.sodeso.webservice.mock.console.client.application.download.endpoint;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.MultiSelectionModel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.endpoint.rpc.EndpointRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * @author Ronald Mathies
 */
public class SelectEndpointsStep extends AbstractWizardStep<DownloadEndpointWizardContext> {

    private transient LegendPanel legendPanel = null;

    private transient CellTableField<DownloadEndpointSummaryItem> endpointsTable = null;
    private transient AbstractWizardStep<DownloadEndpointWizardContext> selectEndpointOptionsStep = null;

    private transient EndpointRpcGateway gateway = GWT.create(EndpointRpcGateway.class);

    public SelectEndpointsStep(AbstractWizardStep<DownloadEndpointWizardContext> callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep<DownloadEndpointWizardContext> getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep<DownloadEndpointWizardContext> getNextStep() {
        if (selectEndpointOptionsStep == null) {
            selectEndpointOptionsStep = new SelectEndpointOptionsStep(this);
        }

        return selectEndpointOptionsStep;
    }

    @Override
    public Widget getBodyWidget() {
        if (legendPanel != null) {
            return legendPanel;
        }

        endpointsTable = new CellTableField<>("endpoints", 512);
        endpointsTable.setFullHeight(true);

        TextColumn<DownloadEndpointSummaryItem> labelColumn = new TextColumn<DownloadEndpointSummaryItem>() {
            @Override
            public String getValue(DownloadEndpointSummaryItem item) {
                return item.getLabel();
            }
        };
        labelColumn.setDefaultSortAscending(true);
        endpointsTable.addColumn(labelColumn); // new TextHeader("Endpoint name")
        endpointsTable.setColumnWidth(0, 100, Style.Unit.PCT);

        endpointsTable.setSelectionModel(new MultiSelectionModel<>());
        endpointsTable.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);

        ColumnSortEvent.ListHandler<DownloadEndpointSummaryItem> listHandler = new ColumnSortEvent.ListHandler<>(endpointsTable.getListDataProvider().getList());
        listHandler.setComparator(labelColumn, Comparator.comparing(DownloadEndpointSummaryItem::getLabel));
        endpointsTable.addColumnSortHandler(listHandler);

        ColumnSortList.ColumnSortInfo columnSortInfo = new ColumnSortList.ColumnSortInfo(labelColumn, true);
        endpointsTable.getColumnSortList().push(columnSortInfo);

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntries(
                new EntryWithDocumentation(null, "Select one or more endpoints to export data from, after selecting the endpoints you will be able to select what part of the data for each individual selected endpoint you would like to export."),
                new EntryWithDocumentation(null, ""),
                new EntryWithContainer(null, endpointsTable)
        );

        legendPanel = new LegendPanel("step-1", "Step 1: Select endpoints to export");
        legendPanel.add(entryForm);

        legendPanel.addAttachHandler(event -> {
            if (event.isAttached()) {
                gateway.findSummaries(ServiceManager.instance().getService(), new DefaultAsyncCallback<ArrayList<EndpointSummaryItem>>() {
                    @Override
                    public void success(ArrayList<EndpointSummaryItem> result) {
                        // TODO: Keep old selection, new list might replace existing list.
                        ArrayList<DownloadEndpointSummaryItem> selectableResult = new ArrayList<>();

                        for (EndpointSummaryItem endpointSummaryItem : result) {
                            selectableResult.add(new DownloadEndpointSummaryItem(endpointSummaryItem));
                        }

                        getWizardContext().setEndpointSummaryItems(selectableResult);
                        endpointsTable.replaceAll(getWizardContext().getEndpointSummaryItems());
                    }
                });
            }
        });

        return legendPanel;
    }

    @Override
    public void onBeforeNext(Trigger trigger) {
        submit();
        super.onBeforeNext(trigger);
    }

    private void submit() {
        endpointsTable.getUnselected().forEach(item -> item.setSelected(false));
        endpointsTable.getSelected().forEach(item -> item.setSelected(true));
    }
}
