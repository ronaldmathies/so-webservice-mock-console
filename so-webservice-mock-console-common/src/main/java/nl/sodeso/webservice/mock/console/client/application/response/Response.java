package nl.sodeso.webservice.mock.console.client.application.response;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.Resources;
import nl.sodeso.webservice.mock.console.client.application.endpoint.Endpoint;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.generic.HttpStatusComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.HttpStatusOption;
import nl.sodeso.webservice.mock.console.client.application.generic.cookie.Cookies;
import nl.sodeso.webservice.mock.console.client.application.generic.header.Headers;
import nl.sodeso.webservice.mock.console.client.application.logging.Logging;
import nl.sodeso.webservice.mock.console.client.application.response.actions.Actions;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Conditions;
import nl.sodeso.webservice.mock.console.client.application.response.type.*;

import java.io.Serializable;

import static nl.sodeso.webservice.mock.console.client.application.Resources.ENDPOINT_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class Response implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_NAME = "name";
    private static final String KEY_GENERAL = "general";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_RESPONSE_TYPE = "response-type";
    private static final String KEY_RESPONSE_CONTAINER = "response-container";
    private static final String KEY_HTTP_STATUS = "status";
    private static final String KEY_COOKIES = "cookies";
    private static final String KEY_HEADERS = "headers";
    private static final String KEY_CONDITIONS = "conditions";
    private static final String KEY_ACTIONS = "actions";
    private static final String KEY_LOGGING = "logging";

    private boolean isExisting = false;
    private StringType name = new StringType("");
    private OptionType<HttpStatusOption> httpStatusOption = new OptionType<>();
    private BooleanType isDeleteAfterUse = new BooleanType(false);

    private OptionType<ResponseTypeOption> responseTypeOption = new OptionType<>(ResponseTypeOption.MESSAGE);
    private ResponseType responseType = new MessageResponseType();

    private Cookies cookies = new Cookies();
    private Headers headers = new Headers();
    private Conditions conditions = new Conditions();
    private Actions actions = new Actions();
    private Logging logging = new Logging();

    private EndpointSummaryItem endpoint = null;

    private transient EntryForm entryForm = null;

    private transient LegendPanel generalLegendPanel = null;
    private transient TextField nameTextField = null;
    private transient HttpStatusComboboxField httpStatusComboboxField = null;

    private transient LegendPanel responseLegendPanel = null;
    private transient ResponseTypeComboboxField responseTypeComboboxField = null;
    private transient FlowPanel responseTypeContainer = null;

    private transient LegendPanel cookiesLegendPanel = null;
    private transient LegendPanel headersLegendPanel = null;

    private transient LegendPanel conditionsLegendPanel = null;
    private transient LegendPanel actionsLegendPanel = null;

    private transient LegendPanel loggingLegendPanel = null;

    public Response() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupGeneralPanel();
        setupResponsePanel();
        setupCookies();
        setupHeaders();
        setupConditions();
        setupActions();
        setupLogging();

        entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithContainer(KEY_GENERAL, this.generalLegendPanel),
                new EntryWithContainer(KEY_MESSAGE, this.responseLegendPanel),
                new EntryWithContainer(KEY_COOKIES, this.cookiesLegendPanel),
                new EntryWithContainer(KEY_HEADERS, this.headersLegendPanel),
                new EntryWithContainer(KEY_CONDITIONS, this.conditionsLegendPanel),
                new EntryWithContainer(KEY_ACTIONS, this.actionsLegendPanel),
                new EntryWithContainer(KEY_LOGGING, this.loggingLegendPanel));

        this.nameTextField.setFocus();

        return this.entryForm;
    }

    private void setupGeneralPanel() {
        nameTextField = new TextField<>(KEY_NAME, this.name)
            .addValidationRules(
                new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return nameTextField.getValue();
                    }
                },
                new NameUniqueValidationRule() {

                    @Override
                    public String getResponseName() {
                        return name.getValue();
                    }

                    @Override
                    public String getEndpointName() {
                        return endpoint.getLabel();
                    }

                    @Override
                    public boolean isApplicable() {
                        return super.isApplicable() && !isExisting();
                    }
                }
            );

        httpStatusComboboxField = new HttpStatusComboboxField(httpStatusOption);
        httpStatusComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_HTTP_STATUS, ValidationMessage.Level.ERROR, httpStatusOption));

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_NAME, RESPONSE_I18N.name(), nameTextField),
                new EntryWithLabelAndWidget(KEY_HTTP_STATUS, RESPONSE_I18N.status(), httpStatusComboboxField)
            );

        this.generalLegendPanel = new LegendPanel(null, RESPONSE_I18N.responsePanel());
        this.generalLegendPanel.add(form);
    }

    private void setupResponsePanel() {
        this.responseTypeComboboxField = new ResponseTypeComboboxField(responseTypeOption)
            .addValidationRule(new ComboboxMandatoryValidationRule(KEY_RESPONSE_TYPE, ValidationMessage.Level.ERROR, responseTypeOption));

        this.responseTypeContainer = new FlowPanel();

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_RESPONSE_TYPE, RESPONSE_I18N.responseType(), responseTypeComboboxField),
                new EntryWithContainer(KEY_RESPONSE_CONTAINER, responseTypeContainer)
            );

        this.responseTypeOption.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(this.responseTypeComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                responseTypeContainer.clear();

                if (responseTypeOption.getValue().isMessage()) {
                    responseType = new MessageResponseType();
                } else if (responseTypeOption.getValue().isRedirect()) {
                    responseType = new RedirectResponseType();
                } else if (responseTypeOption.getValue().isProxy()) {
                    responseType = new ProxyResponseType();
                } else if (responseTypeOption.getValue().isDownload()) {
                    responseType = new DownloadResponseType();
                }

                responseTypeContainer.add(responseType.toForm());
            }
        });

        this.responseLegendPanel = new LegendPanel(null, RESPONSE_I18N.message());
        this.responseLegendPanel.addAttachHandler(event -> {
            if (event.isAttached()) {
                if (responseType != null) {
                    responseTypeContainer.add(responseType.toForm());
                }
            }
        });

        this.responseLegendPanel.add(form);
    }

    private void setupCookies() {
        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_COOKIES, RESPONSE_I18N.cookiesDocumentation(), Align.LEFT),
                new EntryWithContainer(null, cookies.toEditForm()));

        this.cookiesLegendPanel = new LegendPanel(KEY_COOKIES, RESPONSE_I18N.cookies(cookies.getWidgets().size()));
        this.cookiesLegendPanel.add(form);
        this.cookiesLegendPanel.showCollapseButton(true);
        this.cookiesLegendPanel.setCollapsed(true);

        cookies
            .addWidgetAddedEventHandler(event -> cookiesLegendPanel.setTitle(RESPONSE_I18N.cookies(cookies.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> cookiesLegendPanel.setTitle(RESPONSE_I18N.cookies(cookies.getWidgets().size())));
    }

    private void setupHeaders() {
        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_HEADERS, RESPONSE_I18N.headersDocumentation(), Align.LEFT),
                new EntryWithContainer(null, headers.toEditForm()));

        this.headersLegendPanel = new LegendPanel(KEY_HEADERS, RESPONSE_I18N.headers(headers.getWidgets().size()));
        this.headersLegendPanel.add(form);
        this.headersLegendPanel.showCollapseButton(true);
        this.headersLegendPanel.setCollapsed(true);

        headers
            .addWidgetAddedEventHandler(event -> headersLegendPanel.setTitle(RESPONSE_I18N.headers(headers.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> headersLegendPanel.setTitle(RESPONSE_I18N.headers(headers.getWidgets().size())));
    }

    private void setupConditions() {
        EntryForm form = new EntryForm(null)
                .addEntries(
                        new EntryWithDocumentation(KEY_HEADERS, RESPONSE_I18N.conditionsDocumentation(), Align.LEFT),
                        new EntryWithContainer(null, conditions.toEditForm()));

        this.conditionsLegendPanel = new LegendPanel(KEY_CONDITIONS, RESPONSE_I18N.conditions(conditions.getWidgets().size()));
        this.conditionsLegendPanel.add(form);
        this.conditionsLegendPanel.showCollapseButton(true);
        this.conditionsLegendPanel.setCollapsed(true);

        conditions
            .addWidgetAddedEventHandler(event -> conditionsLegendPanel.setTitle(RESPONSE_I18N.conditions(conditions.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> conditionsLegendPanel.setTitle(RESPONSE_I18N.conditions(conditions.getWidgets().size())));
    }

    private void setupActions() {
        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_ACTIONS, RESPONSE_I18N.actionsDocumentation(), Align.LEFT),
                new EntryWithContainer(null, actions.toEditForm()));

        this.actionsLegendPanel = new LegendPanel(KEY_HEADERS, RESPONSE_I18N.actions(actions.getWidgets().size()));
        this.actionsLegendPanel.add(form);
        this.actionsLegendPanel.showCollapseButton(true);
        this.actionsLegendPanel.setCollapsed(true);

        actions
            .addWidgetAddedEventHandler(event -> actionsLegendPanel.setTitle(RESPONSE_I18N.actions(actions.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> actionsLegendPanel.setTitle(RESPONSE_I18N.actions(actions.getWidgets().size())));
    }

    private void setupLogging() {
        this.loggingLegendPanel = new LegendPanel(null, ENDPOINT_I18N.loggingPanel());
        this.loggingLegendPanel.add(logging.toEditForm());
        this.loggingLegendPanel.showCollapseButton(true);
        this.loggingLegendPanel.setCollapsed(!logging.getEnableLogging().getValue());
    }

    public void preSaveTrigger(Trigger trigger) {
        responseType.preSaveAction(trigger);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entryForm);
    }


    public boolean isExisting() {
        return isExisting;
    }

    public void setIsExisting(boolean isExisting) {
        this.isExisting = isExisting;
    }

    public StringType getName() {
        return this.name;
    }

    public void setName(StringType name) {
        this.name = name;
    }

    public OptionType<HttpStatusOption> getHttpStatusOption() {
        return httpStatusOption;
    }

    public void setHttpStatusOption(OptionType<HttpStatusOption> httpStatusOption) {
        this.httpStatusOption = httpStatusOption;
    }

    public BooleanType getIsDeleteAfterUse() {
        return isDeleteAfterUse;
    }

    public void setIsDeleteAfterUse(BooleanType isDeleteAfterUse) {
        this.isDeleteAfterUse = isDeleteAfterUse;
    }

    public OptionType<ResponseTypeOption> getResponseTypeOption() {
        return responseTypeOption;
    }

    public void setResponseTypeOption(OptionType<ResponseTypeOption> responseTypeOption) {
        this.responseTypeOption = responseTypeOption;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }

    public Cookies getCookies() {
        return cookies;
    }

    public void setCookies(Cookies cookies) {
        this.cookies = cookies;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public Conditions getConditions() {
        return conditions;
    }

    public void setConditions(Conditions conditions) {
        this.conditions = conditions;
    }

    public Actions getActions() {
        return actions;
    }

    public void setActions(Actions actions) {
        this.actions = actions;
    }

    public Logging getLogging() {
        return this.logging;
    }

    public void setLogging(Logging logging) {
        this.logging = logging;
    }

    protected EndpointSummaryItem getEndpoint() {
        return endpoint;
    }

    protected void setEndpoint(EndpointSummaryItem endpoint) {
        this.endpoint = endpoint;
    }
}
