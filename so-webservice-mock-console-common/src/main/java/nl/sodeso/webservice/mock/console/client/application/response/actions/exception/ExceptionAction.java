package nl.sodeso.webservice.mock.console.client.application.response.actions.exception;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.webservice.mock.console.client.application.response.actions.Action;
import nl.sodeso.webservice.mock.console.client.application.response.actions.ActionOption;

import static nl.sodeso.webservice.mock.console.client.application.Resources.EXCEPTION_I18N;

/**
 * @author Ronald Mathies
 */
public class ExceptionAction extends Action {

    private static final String KEY_EXCEPTION_ACTION = "exception-action";
    private static final String KEY_EXCEPTION        = "exception";

    private transient EntryWithSingleWidget entry = null;

    public ExceptionAction() {
    }

    public ActionOption getActionOption() {
        return ActionOption.EXCEPTION;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_EXCEPTION, EXCEPTION_I18N.exceptionDocumentation())
            );

        LegendPanel legendPanel = new LegendPanel(null, EXCEPTION_I18N.exceptionAction());
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_EXCEPTION_ACTION, legendPanel);
        return entry;
    }

}