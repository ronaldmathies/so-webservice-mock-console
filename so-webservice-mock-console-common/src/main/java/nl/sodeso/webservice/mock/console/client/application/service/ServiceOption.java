package nl.sodeso.webservice.mock.console.client.application.service;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class ServiceOption extends DefaultOption {

    private String version;
    private String ipAddress;
    private int port;

    public ServiceOption() {
    }

    public ServiceOption(String name) {
        super(name, name);
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
