package nl.sodeso.webservice.mock.console.client.application.response.conditions.method;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class MethodComboboxField extends ComboboxField<MethodOption> {

    public MethodComboboxField(OptionType<MethodOption> optionValue) {
        super(optionValue);
        addItems(MethodOption.getOptions());
    }
}
