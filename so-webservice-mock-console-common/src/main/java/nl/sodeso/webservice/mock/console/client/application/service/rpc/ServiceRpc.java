package nl.sodeso.webservice.mock.console.client.application.service.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("webservicemockconsole/endpoint.webservicemock-service")
public interface ServiceRpc extends XsrfProtectedService {

    ArrayList<ServiceOption> asOptions();

}
