package nl.sodeso.webservice.mock.console.client.application.response.type;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextAreaField;
import nl.sodeso.gwt.ui.client.form.input.TextAreaToolsField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.generic.ContentTypeComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.ContentTypeOption;

import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class MessageResponseType extends ResponseType {

    private static final String KEY_HTTP_CONTENTTYPE = "content-type";
    private static final String KEY_HTTP_BODY = "body";

    private OptionType<ContentTypeOption> contentTypeOption = new OptionType<>(ContentTypeOption.APPLICATION_XML);
    private StringType otherContentTypeOption = new StringType();
    private StringType body = new StringType();

    private transient EntryForm form = null;

    private transient ContentTypeComboboxField contentTypeComboboxField = null;
    private transient TextAreaToolsField bodyToolsField = null;
    private transient TextAreaField bodyField = null;

    public MessageResponseType() {}

    public EntryForm toForm() {
        if (form != null) {
            return form;
        }

        contentTypeComboboxField = new ContentTypeComboboxField(contentTypeOption, otherContentTypeOption);

        bodyField = new TextAreaField(KEY_HTTP_BODY, body);
        bodyField.setVisibleLines(12);
        bodyToolsField = new TextAreaToolsField(RESPONSE_I18N.body(), bodyField);
        bodyToolsField.addZoomOption();

        form = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_HTTP_CONTENTTYPE, RESPONSE_I18N.contentType(), contentTypeComboboxField),
                new EntryWithLabelAndWidget(KEY_HTTP_BODY, RESPONSE_I18N.body(), bodyToolsField));

        return form;
    }

    public StringType getBody() {
        return body;
    }

    public void setBody(StringType body) {
        this.body = body;
    }

    public OptionType<ContentTypeOption> getContentTypeOption() {
        return contentTypeOption;
    }

    public void setContentTypeOption(OptionType<ContentTypeOption> contentTypeOption) {
        this.contentTypeOption = contentTypeOption;
    }

    public StringType getOtherContentTypeOption() {
        return otherContentTypeOption;
    }

    public void setOtherContentTypeOption(StringType otherContentTypeOption) {
        this.otherContentTypeOption = otherContentTypeOption;
    }

}
