package nl.sodeso.webservice.mock.console.client.application.upload.endpoint.steps;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadEndpointWizardContext;

import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.UPLOAD_ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class SelectUploadEndpointOptionsStep extends AbstractWizardStep<UploadEndpointWizardContext> {

    private static final String KEY_INCLUDE_REQUESTS = "include-responses";
    private static final String KEY_INCLUDE_RESPONSES = "include-requests";
    private static final String KEY_STEP = "step-";

    private BooleanType includeRequests = new BooleanType(false);
    private BooleanType includeResponses = new BooleanType(false);

    private transient LegendPanel legendPanel = null;
    private transient RadioButtonGroupField includeRequestsGroupField = null;
    private transient RadioButtonGroupField includeResponsesGroupField = null;

    private transient AbstractWizardStep<UploadEndpointWizardContext> chooseEndpointNameStep = null;

    public SelectUploadEndpointOptionsStep(AbstractWizardStep<UploadEndpointWizardContext> callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep<UploadEndpointWizardContext> getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep<UploadEndpointWizardContext> getNextStep() {
        if (this.chooseEndpointNameStep == null) {
            this.chooseEndpointNameStep = new ChooseEndpointNameStep(this);
        }

        return this.chooseEndpointNameStep;
    }

    @Override
    public Widget getBodyWidget() {
        if (this.legendPanel != null) {
            return this.legendPanel;
        }

        this.includeRequestsGroupField = new RadioButtonGroupField(KEY_INCLUDE_REQUESTS, this.includeRequests);
        this.includeRequestsGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.includeRequestsGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.includeRequestsGroupField.setAlignment(Align.HORIZONTAL);

        this.includeResponsesGroupField = new RadioButtonGroupField(KEY_INCLUDE_RESPONSES, this.includeResponses);
        this.includeResponsesGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.includeResponsesGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.includeResponsesGroupField.setAlignment(Align.HORIZONTAL);

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntries(
                new EntryWithLabelAndWidget(KEY_INCLUDE_RESPONSES, UPLOAD_ENDPOINT_I18N.importResponses(), this.includeResponsesGroupField),
                new EntryWithLabelAndWidget(KEY_INCLUDE_REQUESTS, UPLOAD_ENDPOINT_I18N.importRequests(), this.includeRequestsGroupField)
        );

        entryForm.addAttachHandler(event -> {
            includeRequests.setValue(getWizardContext().getUploadedEndpointDetails().isContainsRequests());
            includeResponses.setValue(getWizardContext().getUploadedEndpointDetails().isContainsResponses());

            entryForm.setEntriesVisible(includeRequests.getValue(), KEY_INCLUDE_REQUESTS);
            entryForm.setEntriesVisible(includeResponses.getValue(), KEY_INCLUDE_RESPONSES);
        });

        this.legendPanel = new LegendPanel(KEY_STEP + getStepIndex(), UPLOAD_ENDPOINT_I18N.importOptionsTitle(getStepIndex()));
        this.legendPanel.add(entryForm);

        return this.legendPanel;
    }

    @Override
    public void onBeforePrevious(Trigger trigger) {
        submit();
        super.onBeforePrevious(trigger);
    }

    @Override
    public void onBeforeNext(Trigger trigger) {
        submit();
        super.onBeforeNext(trigger);
    }

    private void submit() {
        getWizardContext().setIncludeRequests(this.includeRequests.getValue());
        getWizardContext().setIncludeResponses(this.includeResponses.getValue());
    }
}
