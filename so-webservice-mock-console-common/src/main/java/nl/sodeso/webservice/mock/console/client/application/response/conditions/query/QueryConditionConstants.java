package nl.sodeso.webservice.mock.console.client.application.response.conditions.query;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface QueryConditionConstants extends Messages {

    @DefaultMessage("Query Condition")
    String queryCondition();

    @DefaultMessage("Use the query condition to limit this response by testing the URLs query values. Query values are the name / value combinations that appear at the end of the URL ( for example: ?compayId=10 ).")
    String queryDocumentation();

    @DefaultMessage("Name")
    String name();

    @DefaultMessage("companyId")
    String namePlaceholder();

    @DefaultMessage("Value")
    String value();

    @DefaultMessage("10")
    String valuePlaceholder();
}