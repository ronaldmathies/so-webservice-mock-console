package nl.sodeso.webservice.mock.console.client.application.ui.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.webservice.mock.console.client.application.Resources;

/**
 * Button representing an <code>Copy</code> button.
 *
 * @author Ronald Mathies
 */
public class RequestsButton {

    // Keys for so-key entries.
    public static final String KEY = "Requests";

    public static class WithLabel extends SimpleButton {

        public WithLabel() {
            this(null);
        }

        public WithLabel(ClickHandler handler) {
            super(KEY, Resources.UI_I18N.Requests(), Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

}
