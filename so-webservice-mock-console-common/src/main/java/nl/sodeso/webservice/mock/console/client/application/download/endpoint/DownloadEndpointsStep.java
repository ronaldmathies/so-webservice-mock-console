package nl.sodeso.webservice.mock.console.client.application.download.endpoint;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.table.summary.SummaryTableField;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
import nl.sodeso.webservice.mock.console.client.application.download.rpc.DownloadRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class DownloadEndpointsStep extends AbstractWizardStep<DownloadEndpointWizardContext> {

    private transient LegendPanel legendPanel = null;
    private transient SummaryTableField<DownloadEndpointSummaryItem> endpointsTable = null;

    private DownloadRpcGateway gateway = GWT.create(DownloadRpcGateway.class);

    public DownloadEndpointsStep(AbstractWizardStep<DownloadEndpointWizardContext> callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep<DownloadEndpointWizardContext> getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep<DownloadEndpointWizardContext> getNextStep() {
        return null;
    }

    @Override
    public Widget getBodyWidget() {
        if (legendPanel != null) {
            return legendPanel;
        }

        endpointsTable = new SummaryTableField<>();
        endpointsTable.setFullHeight(true);

        endpointsTable.addSelectionChangeHandler(event -> {
            if (endpointsTable.hasSelection()) {
                DownloadEndpointSummaryItem summaryItem = endpointsTable.getFirstSelected();
                Window.open(GWT.getModuleBaseURL() + "file.webservicemock-export-download?file=" + summaryItem.getExportFilename(), "_self", null);
            }
        });


        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithContainer(null, endpointsTable));

        legendPanel = new LegendPanel("step-3", "Step 3: Download Endpoint(s)");
        legendPanel.add(entryForm);

        legendPanel.addAttachHandler(event -> {
            if (event.isAttached()) {
                gateway.prepareEndpointExport(ServiceManager.instance().getService(), getWizardContext(), new DefaultAsyncCallback<ArrayList<DownloadEndpointSummaryItem>>() {
                    @Override
                    public void success(ArrayList<DownloadEndpointSummaryItem> result) {
                        getWizardContext().setEndpointSummaryItems(result);

                        ArrayList<DownloadEndpointSummaryItem> selectedItems = new ArrayList<>();
                        for (DownloadEndpointSummaryItem downloadEndpointSummaryItem : result) {
                            if (downloadEndpointSummaryItem.isSelected()) {
                                selectedItems.add(downloadEndpointSummaryItem);
                            }
                        }

                        endpointsTable.replaceAll(selectedItems);
                    }
                });

            }
        });

        return legendPanel;
    }
}
