package nl.sodeso.webservice.mock.console.client.application.endpoint;

import nl.sodeso.gwt.ui.client.form.table.summary.AbstractSummaryItem;

/**
 * @author Ronald Mathies
 */
public class EndpointSummaryItem extends AbstractSummaryItem {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String getLabel() {
        return this.name;
    }

}
