package nl.sodeso.webservice.mock.console.client.application.response.actions;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface ActionConstants extends Messages {

    @DefaultMessage("Select the type of action to add to the response:")
    String actionDocumentation();

    @DefaultMessage("Add Action")
    String title();

    @DefaultMessage("Action")
    String action();
}