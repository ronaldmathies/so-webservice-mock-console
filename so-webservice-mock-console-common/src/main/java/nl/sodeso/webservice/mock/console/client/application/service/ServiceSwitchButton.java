package nl.sodeso.webservice.mock.console.client.application.service;

import nl.sodeso.gwt.ui.client.controllers.navigation.NavigationButton;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.query.QueryCondition;
import nl.sodeso.webservice.mock.console.client.application.service.events.ServiceChangedEvent;
import nl.sodeso.webservice.mock.console.client.application.service.events.ServiceChangedEventHandler;

/**
 * @author Ronald Mathies
 */
public class ServiceSwitchButton extends NavigationButton {

    private static final String KEY = "switch-endpoints";

    public ServiceSwitchButton() {
        super(KEY, Align.LEFT, Icon.Database, "No service");

        addClickHandler((event) -> {
            switchEndpoint();
        });

        ServiceManager.instance().addServiceChangedEventHandler(new ServiceChangedEventHandler() {
            @Override
            public void onEvent(ServiceChangedEvent event) {
                setText(event.getService().getKey());
            }
        });
    }

    private void switchEndpoint() {
        final ChooseServiceQuestionPanel questionPanel = new ChooseServiceQuestionPanel(option -> {
            ServiceManager.instance().setService(option.getValue());
        });
        questionPanel.center(true, true);
    }

}
