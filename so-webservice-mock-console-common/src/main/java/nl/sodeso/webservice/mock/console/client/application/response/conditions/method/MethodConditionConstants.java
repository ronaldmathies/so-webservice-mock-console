package nl.sodeso.webservice.mock.console.client.application.response.conditions.method;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface MethodConditionConstants extends Messages {


    @DefaultMessage("The method condition is used to limit this response for Requests by testing if the request method is the same as the method specified.")
    String methodConditionDocumentation();

    @DefaultMessage("Method")
    String method();

    @DefaultMessage("Method Condition")
    String methodCondition();

}