package nl.sodeso.webservice.mock.console.client.application.service;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.webservice.mock.console.client.application.service.rpc.ServiceRpcGateway;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class ServiceComboboxField extends ComboboxField<ServiceOption> {

    public ServiceComboboxField(OptionType<ServiceOption> optionValue) {
        super(optionValue);

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                ServiceRpcGateway gateway = GWT.create(ServiceRpcGateway.class);
                gateway.asOptions(new DefaultAsyncCallback<ArrayList<ServiceOption>>() {
                    @Override
                    public void success(ArrayList<ServiceOption> result) {
                        ServiceComboboxField.this.replaceItemsRetainSelection(result);
                    }
                });
            }
        });
    }
}
