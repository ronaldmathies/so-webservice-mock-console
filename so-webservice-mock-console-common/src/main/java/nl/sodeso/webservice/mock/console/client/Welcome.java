package nl.sodeso.webservice.mock.console.client;

import nl.sodeso.gwt.ui.client.controllers.center.AbstractWelcomeCenterPanel;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

/**
 * @author Ronald Mathies
 */
public class Welcome extends AbstractWelcomeCenterPanel {

    @Override
    public void beforeAddWelcomePanel(Trigger trigger) {
        trigger.fire();
    }

}
