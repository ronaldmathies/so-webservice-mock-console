package nl.sodeso.webservice.mock.console.client.application.response.type;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public abstract class ResponseType implements Serializable {

    public abstract EntryForm toForm();

    public void preSaveAction(Trigger trigger) {
        trigger.fire();
    }

}
