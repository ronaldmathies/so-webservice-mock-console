package nl.sodeso.webservice.mock.console.client.application.response.functions.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class FunctionRpcGateway implements RpcGateway<FunctionRpcAsync>, FunctionRpcAsync {

    /**
     * {@inheritDoc}
     */
    public FunctionRpcAsync getRpcAsync() {
        return GWT.create(FunctionRpc.class);
    }
}
