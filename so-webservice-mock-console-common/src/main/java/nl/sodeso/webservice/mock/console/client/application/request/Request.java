package nl.sodeso.webservice.mock.console.client.application.request;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.LabelField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.LongType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.generic.cookie.Cookies;
import nl.sodeso.webservice.mock.console.client.application.generic.header.Headers;
import nl.sodeso.webservice.mock.console.client.application.generic.query.Queries;

import java.io.Serializable;

import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class Request implements Serializable {

    private static final String KEY_GENERAL = "general";
    private static final String KEY_ID = "id";
    private static final String KEY_METHOD = "method";
    private static final String KEY_REQUEST_URL = "request-url";
    private static final String KEY_COOKIES = "cookies";
    private static final String KEY_HEADERS = "headers";
    private static final String KEY_QUERIES = "queries";

    private StringType id = new StringType();
    private LongType time = new LongType();
    private StringType method = new StringType();
    private StringType requestUrl = new StringType();

    private Queries queries = new Queries();
    private Cookies cookies = new Cookies();
    private Headers headers = new Headers();

    private transient EntryForm entryForm = null;

    private transient LegendPanel generalLegendPanel = null;
    private transient TextField idField = null;
    private transient TextField methodField = null;
    private transient TextField requestUrlField = null;
    private transient LegendPanel queryValuesLegendPanel = null;
    private transient LegendPanel cookiesLegendPanel = null;
    private transient LegendPanel headersLegendPanel = null;

    public Request() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupGeneral();
        setupQuery();
        setupCookies();
        setupHeaders();

        entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithContainer(KEY_GENERAL, this.generalLegendPanel),
                new EntryWithContainer(KEY_QUERIES, this.queryValuesLegendPanel),
                new EntryWithContainer(KEY_COOKIES, this.cookiesLegendPanel),
                new EntryWithContainer(KEY_HEADERS, this.headersLegendPanel)
            );

        return this.entryForm;
    }

    private void setupGeneral() {
        idField = new TextField<>(KEY_ID, id);
        idField.setReadOnly(true);

        methodField = new TextField<>(KEY_METHOD, method);
        methodField.setReadOnly(true);

        requestUrlField = new TextField<>(KEY_REQUEST_URL, requestUrl);
        requestUrlField.setReadOnly(true);



        EntryForm form = new EntryForm(null)
                .addEntries(
                        new EntryWithLabelAndWidget(null, "ID", idField),
                        new EntryWithLabelAndWidget(null, "Method", methodField),
                        new EntryWithLabelAndWidget(null, "Request URL", requestUrlField)
                );

        this.generalLegendPanel = new LegendPanel(KEY_GENERAL, "Request Information");
        this.generalLegendPanel.add(form);
        this.generalLegendPanel.showCollapseButton(true);
        this.generalLegendPanel.setCollapsed(false);
    }

    private void setupQuery() {
        QueryTable queryTable = new QueryTable();
        queryTable.addAll(queries.getWidgets());

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithContainer(null, queryTable)
            );

        this.queryValuesLegendPanel = new LegendPanel(KEY_QUERIES, RESPONSE_I18N.queryValues(this.cookies.getWidgets().size()));
        this.queryValuesLegendPanel.add(form);
        this.queryValuesLegendPanel.showCollapseButton(true);
        this.queryValuesLegendPanel.setCollapsed(true);
    }

    private void setupCookies() {
        CookieTable cookieTable = new CookieTable();
        cookieTable.addAll(cookies.getWidgets());

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithContainer(null, cookieTable)
            );

        this.cookiesLegendPanel = new LegendPanel(KEY_COOKIES, RESPONSE_I18N.cookies(this.cookies.getWidgets().size()));
        this.cookiesLegendPanel.add(form);
        this.cookiesLegendPanel.showCollapseButton(true);
        this.cookiesLegendPanel.setCollapsed(true);
    }

    private void setupHeaders() {
        HeaderTable headerTable = new HeaderTable();
        headerTable.addAll(headers.getWidgets());

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithContainer(null, headerTable));

        this.headersLegendPanel = new LegendPanel(KEY_HEADERS, RESPONSE_I18N.headers(this.headers.getWidgets().size()));
        this.headersLegendPanel.add(form);
        this.headersLegendPanel.showCollapseButton(true);
        this.headersLegendPanel.setCollapsed(true);
    }

    public void setId(StringType id) {
        this.id = id;
    }

    public StringType getId() {
        return this.id;
    }

    public void setTime(LongType time) {
        this.time = time;
    }

    public LongType getTime() {
        return this.time;
    }

    public void setMethod(StringType method) {
        this.method = method;
    }

    public StringType getMethod() {
        return this.method;
    }

    public void setRequestUrl(StringType requestUrl) {
        this.requestUrl = requestUrl;
    }

    public StringType getRequestUrl() {
        return this.requestUrl;
    }

    public Queries getQueries() { return this.queries; }

    public void setQueries(Queries queries) {
        this.queries = queries;
    }

    public Cookies getCookies() {
        return this.cookies;
    }

    public void setCookies(Cookies cookies) {
        this.cookies = cookies;
    }

    public Headers getHeaders() {
        return this.headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

}
