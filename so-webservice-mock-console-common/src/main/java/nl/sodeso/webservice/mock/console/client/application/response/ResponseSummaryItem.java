package nl.sodeso.webservice.mock.console.client.application.response;

import nl.sodeso.gwt.ui.client.form.table.summary.AbstractSummaryItem;

/**
 * @author Ronald Mathies
 */
public class ResponseSummaryItem extends AbstractSummaryItem {

    private String responseName;

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public String getResponseName() {
        return this.responseName;
    }

    @Override
    public String getLabel() {
        return this.responseName;
    }

}
