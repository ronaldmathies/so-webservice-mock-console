package nl.sodeso.webservice.mock.console.client.application.upload.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class UploadRpcGateway implements RpcGateway<UploadRpcAsync>, UploadRpcAsync {

    /**
     * {@inheritDoc}
     */
    public UploadRpcAsync getRpcAsync() {
        return GWT.create(UploadRpc.class);
    }
}
