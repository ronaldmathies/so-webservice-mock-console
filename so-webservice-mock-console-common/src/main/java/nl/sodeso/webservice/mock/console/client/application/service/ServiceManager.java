package nl.sodeso.webservice.mock.console.client.application.service;

import nl.sodeso.gwt.event.client.EventBusController;
import nl.sodeso.webservice.mock.console.client.application.service.events.ServiceChangedEvent;
import nl.sodeso.webservice.mock.console.client.application.service.events.ServiceChangedEventHandler;

/**
 * @author Ronald Mathies
 */
public class ServiceManager {

    private static ServiceManager INSTANCE = null;

    private EventBusController eventbus = null;

    private ServiceOption service = null;

    private ServiceManager() {
        this.eventbus = new EventBusController();
    }

    public static ServiceManager instance() {
        if (INSTANCE == null) {
            INSTANCE = new ServiceManager();
        }

        return INSTANCE;
    }

    public void addServiceChangedEventHandler(ServiceChangedEventHandler handler) {
        this.eventbus.addHandler(ServiceChangedEvent.TYPE, handler);
    }

    public void setService(ServiceOption service) {
        this.service = service;
        this.eventbus.fireEvent(new ServiceChangedEvent(service));
    }

    public ServiceOption getService() {
        return this.service;
    }

    public boolean hasService() {
        return this.service != null;
    }

}
