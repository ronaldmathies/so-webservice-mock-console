package nl.sodeso.webservice.mock.console.client.application.response.functions;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.Sample;

/**
 * @author Ronald Mathies
 */
public class SampleOption extends DefaultOption {

    private Sample sample;

    public SampleOption() {}

    public SampleOption(Sample sample) {
        super(sample.getName(), sample.getName());
        this.sample = sample;
    }

    public Sample getSample() {
        return this.sample;
    }

}
