package nl.sodeso.webservice.mock.console.client.application.response;

import com.google.gwt.event.dom.client.ClickEvent;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointComboboxField;

import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class ResponsesFilterPanel extends PopupWindowPanel {

    private static final String KEY_CHOOSE_CONDITION = "filter-responses";
    private static final String KEY_ENDPOINT = "endpoint";

    private OptionType<DefaultOption> endpointOption = new OptionType<>();

    private ResponsesFilterHandler responsesFilterHandler = null;
    private EndpointComboboxField endpointComboboxField = null;

    public ResponsesFilterPanel(final ResponsesFilterHandler responsesFilterHandler) {
        super(KEY_CHOOSE_CONDITION, RESPONSE_I18N.filter(), Style.INFO);
        this.responsesFilterHandler = responsesFilterHandler;

        this.setWidth("650px");

        this.endpointComboboxField = new EndpointComboboxField(endpointOption, true);

        EntryForm entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_ENDPOINT, RESPONSE_I18N.endpointDocumentation()),
                new EntryWithLabelAndWidget(KEY_ENDPOINT, RESPONSE_I18N.endpoint(), endpointComboboxField));

        addToBody(entryForm);

        addToFooter(Align.RIGHT,
            new OkButton.WithLabel(this::filter),
            new CancelButton.WithLabel((event) -> close()));
    }

    private void filter(ClickEvent event) {
        ValidationUtil.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                ResponsesFilterPanel.this.close();

                responsesFilterHandler.filter(endpointOption);
            }
        }, endpointComboboxField);
    }

}
