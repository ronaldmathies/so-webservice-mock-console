package nl.sodeso.webservice.mock.console.client.application.endpoint.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class EndpointRpcGateway implements RpcGateway<EndpointRpcAsync>, EndpointRpcAsync {

    /**
     * {@inheritDoc}
     */
    public EndpointRpcAsync getRpcAsync() {
        return GWT.create(EndpointRpc.class);
    }
}
