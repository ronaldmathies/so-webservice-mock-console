package nl.sodeso.webservice.mock.console.client.application.generic.query;

import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithWidget;
import nl.sodeso.gwt.ui.client.form.input.LabelField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Query implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid;

    private StringType name = new StringType();
    private StringType value = new StringType();

    private transient EntryWithSingleWidget entry = null;

    public Query() {
    }

    @Override
    public EntryWithWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        entry = new EntryWithSingleWidget(null, new LabelField(null, "no-fields"));

        return entry;
    }

    @Override
    public void validateContainer(ValidationCompletedHandler handler) {

    }

    @Override
    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setName(StringType name) {
        this.name = name;
    }

    public StringType getName() {
        return this.name;
    }

    public StringType getValue() {
        return this.value;
    }

    public void setValue(StringType value) {
        this.value = value;
    }

}
