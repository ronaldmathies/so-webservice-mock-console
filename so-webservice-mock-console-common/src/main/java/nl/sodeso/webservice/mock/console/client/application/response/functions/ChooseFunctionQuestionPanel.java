package nl.sodeso.webservice.mock.console.client.application.response.functions;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.ContainerPanel;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.response.functions.rpc.FunctionRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;

import java.util.ArrayList;

import static nl.sodeso.webservice.mock.console.client.application.Resources.FUNCTION_I18N;

/**
 * @author Ronald Mathies
 */
class ChooseFunctionQuestionPanel extends PopupWindowPanel {

    private static FunctionRpcGateway functionRpcGateway = GWT.create(FunctionRpcGateway.class);

    private static final String KEY_CHOOSE_CONDITION = "choose-function";
    private static final String KEY_FUNCTION_GROUP = "function-group";
    private static final String KEY_FUNCTION = "function";

    private OptionType<FunctionGroupOption> functionGroupOption = new OptionType<>();
    private OptionType<FunctionOption> functionOption = new OptionType<>();
    private FunctionPanel currentFunction = null;

    private FunctionChosenHandler functionChosenHandler = null;
    private FunctionGroupComboboxField functionGroupComboboxField = null;
    private FunctionComboboxField functionComboboxField = null;
    private ContainerPanel functionContainer = null;

    ChooseFunctionQuestionPanel(final FunctionChosenHandler functionChosenHandler) {
        super(KEY_CHOOSE_CONDITION, FUNCTION_I18N.title(), Style.INFO);
        this.functionChosenHandler = functionChosenHandler;

        this.setWidth("950px");

        this.functionGroupComboboxField = new FunctionGroupComboboxField(functionGroupOption);
        this.functionGroupComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_FUNCTION, ValidationMessage.Level.ERROR, functionGroupOption));

        this.functionGroupOption.addValueChangedEventHandler(new ValueChangedEventHandler<OptionType<FunctionGroupOption>>(this.functionGroupComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<OptionType<FunctionGroupOption>> event) {
                functionRpcGateway.functions(ServiceManager.instance().getService(), event.getValueType().getValue(), new DefaultAsyncCallback<ArrayList<FunctionOption>>() {
                    @Override
                    public void success(ArrayList<FunctionOption> result) {
                        functionComboboxField.replaceItems(result, true);
                    }
                });
            }
        });

        this.functionComboboxField = new FunctionComboboxField(functionOption);
        this.functionComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_FUNCTION, ValidationMessage.Level.ERROR, functionOption));

        this.functionOption.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(this.functionComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                currentFunction = new FunctionPanel(functionOption.getValue().getFunction());
                functionContainer.setWidget(currentFunction.toEditForm());
            }
        });

        functionContainer = new ContainerPanel();

        EntryForm entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_FUNCTION_GROUP, FUNCTION_I18N.functionGroup(), functionGroupComboboxField),
                new EntryWithLabelAndWidget(KEY_FUNCTION, FUNCTION_I18N.function(), functionComboboxField),
                new EntryWithContainer(null, functionContainer));

        addToBody(entryForm);

        addToFooter(Align.RIGHT,
            new OkButton.WithLabel(this::ok),
            new CancelButton.WithLabel((event) -> close()));
    }

    private void ok(ClickEvent event) {
        ValidationUtil.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                ChooseFunctionQuestionPanel.this.close();
                functionChosenHandler.ok(currentFunction.toFunction());
            }
        }, functionContainer);
    }

}
