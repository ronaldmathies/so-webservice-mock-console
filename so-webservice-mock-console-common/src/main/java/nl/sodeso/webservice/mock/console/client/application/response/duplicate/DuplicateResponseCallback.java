package nl.sodeso.webservice.mock.console.client.application.response.duplicate;

import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @author Ronald Mathies
 */
public interface DuplicateResponseCallback {

    void onSuccess(StringType name);

}
