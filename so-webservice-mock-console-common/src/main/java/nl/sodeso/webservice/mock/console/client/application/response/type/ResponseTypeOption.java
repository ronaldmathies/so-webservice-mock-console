package nl.sodeso.webservice.mock.console.client.application.response.type;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class ResponseTypeOption extends DefaultOption {

    private static final String KEY_MESSAGE = "message";
    private static final String KEY_PROXY = "proxy";
    private static final String KEY_REDIRECT = "redirect";
    private static final String KEY_DOWNLOAD = "download";

    public static ResponseTypeOption MESSAGE = new ResponseTypeOption(KEY_MESSAGE, "Message");
    public static ResponseTypeOption PROXY = new ResponseTypeOption(KEY_PROXY, "Proxy");
    public static ResponseTypeOption REDIRECT = new ResponseTypeOption(KEY_REDIRECT, "Redirect");
    public static ResponseTypeOption DOWNLOAD = new ResponseTypeOption(KEY_DOWNLOAD, "Download");

    public ResponseTypeOption() {}

    public ResponseTypeOption(String code, String description) {
        super(code, description);
    }

    public static ResponseTypeOption[] getOptions() {
        return new ResponseTypeOption[] {
                MESSAGE, PROXY, REDIRECT, DOWNLOAD
        };
    }

    public static ResponseTypeOption getOption(String code) {
        for (ResponseTypeOption responseTypeOption : getOptions()) {
            if (responseTypeOption.getKey().equals(code)) {
                return responseTypeOption;
            }
        }

        return null;
    }

    public boolean isMessage() {
        return this.getCode().equals(KEY_MESSAGE);
    }

    public boolean isProxy() {
        return this.getCode().equals(KEY_PROXY);
    }

    public boolean isRedirect() {
        return this.getCode().equals(KEY_REDIRECT);
    }

    public boolean isDownload() {
        return this.getCode().equals(KEY_DOWNLOAD);
    }
}
