package nl.sodeso.webservice.mock.console.client.application.service.events;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;


/**
 * @author Ronald Mathies
 */
public class ServiceChangedEvent extends Event<ServiceChangedEventHandler> {

    public static final Type<ServiceChangedEventHandler> TYPE = new Type<>();

    private ServiceOption serviceOption;

    public ServiceChangedEvent(ServiceOption serviceOption) {
        this.serviceOption = serviceOption;
    }

    @Override
    public Type<ServiceChangedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ServiceChangedEventHandler handler) {
        handler.onEvent(this);
    }

    public ServiceOption getService() {
        return this.serviceOption;
    }
}
