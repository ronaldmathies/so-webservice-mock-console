package nl.sodeso.webservice.mock.console.client.application.service.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.webservice.mock.console.client.application.service.Service;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface ServiceRpcAsync {

    void asOptions(AsyncCallback<ArrayList<ServiceOption>> result);

}
