package nl.sodeso.webservice.mock.console.client.application.service.events;

import com.google.gwt.event.shared.EventHandler;
import nl.sodeso.gwt.ui.client.controllers.menu.events.MenuVisibilityChangedEvent;

/**
 * @author Ronald Mathies
 */
public interface ServiceChangedEventHandler extends EventHandler {

    void onEvent(ServiceChangedEvent event);

}
