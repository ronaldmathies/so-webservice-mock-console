package nl.sodeso.webservice.mock.console.client.application.response.conditions;

import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public abstract class Condition implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;

    public Condition() {
    }

    public abstract ConditionOption getConditionOption();

    public abstract EntryWithSingleWidget toEntry();

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, toEntry());
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

}