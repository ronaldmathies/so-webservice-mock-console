package nl.sodeso.webservice.mock.console.client.application.generic.query;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionConfig;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;
import nl.sodeso.webservice.mock.console.client.application.generic.header.Header;

/**
 * @author Ronald Mathies
 */
public class Queries extends WidgetCollectionField<Query> {

    public Queries() {
        super(WidgetCollectionConfig.DEFAULT);
    }

    public WidgetSupplier<Query> createWidgetSupplier() {
        return callback -> callback.created(new Query());
    }
}
