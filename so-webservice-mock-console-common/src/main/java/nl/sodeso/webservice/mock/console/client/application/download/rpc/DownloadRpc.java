package nl.sodeso.webservice.mock.console.client.application.download.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.webservice.mock.console.client.application.download.endpoint.DownloadEndpointWizardContext;
import nl.sodeso.webservice.mock.console.client.application.download.endpoint.DownloadEndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("webservicemockconsole/endpoint.webservicemock-export")
public interface DownloadRpc extends XsrfProtectedService {

    ArrayList<DownloadEndpointSummaryItem> prepareEndpointExport(ServiceOption serviceOption, DownloadEndpointWizardContext context);

}
