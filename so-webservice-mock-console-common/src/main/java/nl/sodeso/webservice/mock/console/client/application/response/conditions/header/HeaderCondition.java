package nl.sodeso.webservice.mock.console.client.application.response.conditions.header;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionOption;
import nl.sodeso.webservice.mock.console.client.application.generic.HeaderComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.HeaderOption;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Condition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import static nl.sodeso.webservice.mock.console.client.application.Resources.HEADER_CONDITION_I18N;

/**
 * @author Ronald Mathies
 */
public class HeaderCondition extends Condition {


    private static final String KEY_HEADER_CONDITION = "header-condition";
    private static final String KEY_EXPRESSION_TYPE = "expression-type";
    private static final String KEY_EXPRESSION = "expression";
    private static final String KEY_NAME = "name";
    private static final String KEY_MATCHES = "matches";

    private OptionType<ExpressionOption> expressionOption = new OptionType<>(ExpressionOption.XPATH);

    private OptionType<HeaderOption> headerOption = new OptionType<>();
    private StringType otherHeaderOption = new StringType();

//    private StringType name = new StringType();
    private StringType expression = new StringType();
    private StringType matches = new StringType();

    private transient ExpressionComboboxField expressionComboboxField = null;
    private transient HeaderComboboxField headerComboboxField = null;
    private transient TextField<StringType> expressionField = null;
    private transient TextField<StringType> matchesField = null;


    private transient EntryWithSingleWidget entry = null;

    public HeaderCondition() {
    }

    public ConditionOption getConditionOption() {
        return ConditionOption.HEADER;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        headerComboboxField = new HeaderComboboxField(headerOption, otherHeaderOption);
        headerOption.addValueChangedEventHandler(new ValueChangedEventHandler<OptionType>(headerComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<OptionType> event) {
                matches.setValue(headerOption.getValue().getExample());
            }
        });

        expressionComboboxField = new ExpressionComboboxField(expressionOption, true);
        expressionComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_EXPRESSION_TYPE, ValidationMessage.Level.ERROR, expressionOption));

        expressionField = new TextField<>(KEY_EXPRESSION, this.expression);
        expressionField.addValidationRule(
                new MandatoryValidationRule(KEY_EXPRESSION, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return expressionField.getValue();
                    }

                    @Override
                    public boolean isApplicable() {
                        return !ExpressionOption.NONE.equals(expressionOption.getValue());
                    }
                }
        );

        matchesField = new TextField<>(KEY_MATCHES, this.matches);

        EntryForm form = new EntryForm(null)
                .addEntries(
                        new EntryWithDocumentation(KEY_HEADER_CONDITION, HEADER_CONDITION_I18N.headerConditionDocumentation()),
                        new EntryWithLabelAndWidget(KEY_NAME, HEADER_CONDITION_I18N.name(), headerComboboxField),
                        new EntryWithLabelAndWidget(KEY_EXPRESSION_TYPE, HEADER_CONDITION_I18N.expressionType(), expressionComboboxField),
                        new EntryWithLabelAndWidget(KEY_EXPRESSION, HEADER_CONDITION_I18N.expression(), expressionField),
                        new EntryWithLabelAndWidget(KEY_MATCHES, HEADER_CONDITION_I18N.matches(), new InsertFunctionButton(matchesField))
                );

        expressionOption.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(expressionComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                form.setEntriesVisible(!ExpressionOption.NONE.equals(expressionOption.getValue()), KEY_EXPRESSION);
            }
        });

        LegendPanel legendPanel = new LegendPanel(null, HEADER_CONDITION_I18N.headerCondition());
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_HEADER_CONDITION, legendPanel);
        return entry;
    }

    public OptionType<ExpressionOption> getExpressionOption() {
        return expressionOption;
    }

    public void setExpressionOption(OptionType<ExpressionOption> expressionOption) {
        this.expressionOption = expressionOption;
    }

    public OptionType<HeaderOption> getHeaderOption() {
        return headerOption;
    }

    public void setHeaderOption(OptionType<HeaderOption> headerOption) {
        this.headerOption = headerOption;
    }

    public StringType getOtherHeaderOption() {
        return otherHeaderOption;
    }

    public void setOtherHeaderOption(StringType otherHeaderOption) {
        this.otherHeaderOption = otherHeaderOption;
    }

    public StringType getExpression() {
        return expression;
    }

    public void setExpression(StringType expression) {
        this.expression = expression;
    }

    public StringType getMatches() {
        return matches;
    }

    public void setMatches(StringType matches) {
        this.matches = matches;
    }
}