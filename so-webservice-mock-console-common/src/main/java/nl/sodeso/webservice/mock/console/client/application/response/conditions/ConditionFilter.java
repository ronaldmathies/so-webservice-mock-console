package nl.sodeso.webservice.mock.console.client.application.response.conditions;

import nl.sodeso.gwt.ui.client.form.combobox.filters.OptionFilter;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.method.MethodCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.method.MethodOption;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * @author Ronald Mathies
 */
public class ConditionFilter implements OptionFilter<ConditionOption> {

    private static HashMap<ConditionOption, Options> options = new HashMap<>();

    static {
        options.put(ConditionOption.PATH, new Options(false, MethodOption.getOptions()));
        options.put(ConditionOption.QUERY, new Options(false, MethodOption.getOptions()));
        options.put(ConditionOption.HEADER, new Options(true, MethodOption.getOptions()));
        options.put(ConditionOption.COOKIE, new Options(true, MethodOption.getOptions()));
        options.put(ConditionOption.METHOD, new Options(false, MethodOption.getOptions()));
        options.put(ConditionOption.BODY,
            new Options(true,
                new MethodOption[] {
                    MethodOption.POST,
                    MethodOption.PUT,
                    MethodOption.TRACE
                }
            )
        );
        options.put(ConditionOption.PARAMETER, new Options(false, MethodOption.getOptions()));
    }

    private Conditions conditions = null;

    public ConditionFilter(Conditions conditions) {
        this.conditions = conditions;
    }

    @Override
    public boolean isOptionAllowed(ConditionOption conditionOption) {
        Optional<Condition> conditionOptional = conditions.getWidgets().stream().filter(new Predicate<Condition>() {
            @Override
            public boolean test(Condition condition) {
                return condition instanceof MethodCondition;
            }
        }).findFirst();

        if (conditionOptional.isPresent() && !conditionOption.isMethodCondition()) {
            MethodCondition methodCondition = (MethodCondition)conditionOptional.get();

            if (methodCondition.getMethodOption() != null && methodCondition.getMethodOption().getValue() != null){
                if (!options.get(conditionOption).isCombinableWith(methodCondition.getMethodOption().getValue())) {
                    return false;
                }
            }
        }

        for (Condition existingCondition : conditions.getWidgets()) {
            if (existingCondition.getConditionOption().equals(conditionOption)) {
                return options.get(conditionOption).isMultiplicityAllowed();
            }
        }

        return true;
    }

    private static class Options {

        private boolean multiplicityAllowed = false;
        private MethodOption[] methodOptions = new MethodOption[] {};

        public Options(
                boolean multiplicityAllowed,
                MethodOption[] methodOptions) {
            this.multiplicityAllowed = multiplicityAllowed;
            this.methodOptions = methodOptions;
        }

        public boolean isMultiplicityAllowed() {
            return this.multiplicityAllowed;
        }

        public boolean isCombinableWith(MethodOption methodOption) {
            for (MethodOption mo : this.methodOptions) {
                if (mo.equals(methodOption)) {
                    return true;
                }
            }

            return false;
        }
    }
}
