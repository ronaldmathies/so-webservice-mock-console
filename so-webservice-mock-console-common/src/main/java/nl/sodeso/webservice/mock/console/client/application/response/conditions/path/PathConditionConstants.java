package nl.sodeso.webservice.mock.console.client.application.response.conditions.path;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface PathConditionConstants extends Messages {

    @DefaultMessage("The path condition is used to limit this response for Requests by testing if the path in the URL contains the exact structure as the path defined in this condition.")
    String pathConditionDocumentation();

    @DefaultMessage("Path Condition")
    String pathCondition();

    @DefaultMessage("Path")
    String path();

    @DefaultMessage("/company/exmployee/10")
    String pathPlaceholder();

    @DefaultMessage("Is Regulair Expression?")
    String isPathRegexp();

}