package nl.sodeso.webservice.mock.console.client.application.endpoint.duplicate;

import nl.sodeso.gwt.ui.client.dialog.InfoQuestionPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.endpoint.NameUniqueValidationRule;
import nl.sodeso.webservice.mock.console.client.application.endpoint.PathUniqueValidationRule;

import static nl.sodeso.webservice.mock.console.client.application.Resources.ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class DuplicateEndpointProcess {

    private static final String KEY_DUPLICATE = "duplicate";
    private static final String KEY_NAME = "name";
    private static final String KEY_PATH = "path";

    private StringType name = new StringType();
    private StringType path = new StringType();
    private TextField<StringType> nameTextField = null;
    private TextField<StringType> pathTextField = null;
    private EntryForm entryForm = null;

    private InfoQuestionPanel questionPanel = null;

    private DuplicateEndpointCallback callback;

    public DuplicateEndpointProcess(DuplicateEndpointCallback callback) {
        this.callback = callback;
    }

    public void start() {
        duplicate();
    }

    private void duplicate() {
        this.questionPanel = new InfoQuestionPanel(ENDPOINT_I18N.duplicateTitle(), ENDPOINT_I18N.duplicateQuestion(), () -> perform());
        this.questionPanel.setWidth("520px");
        this.nameTextField = new TextField<>(KEY_NAME, this.name)
                .addValidationRules(
                        new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
                            @Override
                            public String getValue() {
                                return nameTextField.getValue();
                            }
                        },
                        new NameUniqueValidationRule() {
                            @Override
                            public StringType getName() {
                                return name;
                            }
                        }
                );

        this.pathTextField = new TextField<>(KEY_PATH, this.path)
                .addValidationRules(
                        new MandatoryValidationRule(KEY_PATH, ValidationMessage.Level.ERROR) {
                            @Override
                            public String getValue() {
                                return pathTextField.getValue();
                            }
                        },
                        new PathUniqueValidationRule() {
                            @Override
                            public StringType getPath() {
                                return path;
                            }

                        }
                );
        this.nameTextField.setFocus();

        this.entryForm = new EntryForm(KEY_DUPLICATE)
            .addEntries(
                    new EntryWithLabelAndWidget(KEY_NAME, ENDPOINT_I18N.name(), nameTextField),
                    new EntryWithDocumentation(KEY_PATH, ENDPOINT_I18N.pathDocumentation()),
                    new EntryWithLabelAndWidget(KEY_PATH, ENDPOINT_I18N.path(), pathTextField)
            );
        this.questionPanel.addToBody(this.entryForm);
        this.questionPanel.center(true, true);
    }

    private void perform() {
        ValidationUtil.validate(result -> {
            if (!ValidationUtil.findWorstLevel(result).isError()) {
                questionPanel.close();
                callback.onSuccess(this.name, this.path);
            }
        }, entryForm);

    }
}
