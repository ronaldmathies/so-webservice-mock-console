package nl.sodeso.webservice.mock.console.client.application.request;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.form.button.*;
import nl.sodeso.gwt.ui.client.form.table.summary.SummaryTableField;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.request.rpc.RequestRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;
import nl.sodeso.webservice.mock.console.client.application.ui.button.RequestsButton;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class RequestsPopup extends PopupWindowPanel  {

    private static final String KEY_REQUESTS = "Requests";
    private static final String KEY_TABLE= "table";
    private static final String KEY_DETAILS = "details";

    private String endpointName;
    private String responseName;

    private HorizontalPanel horizontalPanel = null;
    private WindowPanel detailWindowPanel = null;

    private SummaryTableField<RequestSummaryItem> table = null;

    private Request currentObject = new Request();

    private RemoveAllButton.WithLabel deleteAllButton;
    private RefreshButton.WithIcon refreshButton;

    private RemoveButton.WithLabel deleteButton;

    private RequestRpcGateway gateway = GWT.create(RequestRpcGateway.class);

    public RequestsPopup(String endpointName) {
        this(endpointName, null);
    }

    public RequestsPopup(String endpointName, String responseName) {
        super(KEY_REQUESTS, "Requests", Style.INFO);

        this.setWidth("80%");
        this.setHeight("80%");

        this.endpointName = endpointName;
        this.responseName = responseName;

        initWidgets();

        addToBody(horizontalPanel);
    }

    private void initWidgets() {
        this.horizontalPanel = new HorizontalPanel();
        this.horizontalPanel.setFullHeight(true);

        this.table = new SummaryTableField<>();
        this.table.setFullHeight(true);
        this.table.addSelectionChangeHandler(event -> {
            if (table.getSelected() != null) {
                loadDetails();
            }
        });

        this.refreshButton = new RefreshButton.WithIcon((event) -> refresh());
        this.deleteAllButton = new RemoveAllButton.WithLabel((event) -> deleteAll());
        final WindowPanel tableWindowPanel = new WindowPanel(KEY_TABLE, null, WindowPanel.Style.INFO)
                .setFullHeight(true)
                .setBodyPadding(false)
                .addToBody(table)
                .addToToolbar(Align.LEFT, refreshButton)
                .addToToolbar(Align.RIGHT, deleteAllButton);

        this.horizontalPanel.addWidget(tableWindowPanel, 20, com.google.gwt.dom.client.Style.Unit.PCT);

        this.deleteButton = new RemoveButton.WithLabel((event) -> delete());
        this.detailWindowPanel = new WindowPanel(KEY_DETAILS, null, Style.INFO)
                .setFullHeight(true)
                .addToFooter(Align.LEFT, deleteButton);

        this.horizontalPanel.addWidget(detailWindowPanel, 80, com.google.gwt.dom.client.Style.Unit.PCT);

        add(horizontalPanel);

        addToFooter(Align.RIGHT,
            new CloseButton.WithLabel((event) -> close()));

        refresh();
    }

    private void refresh() {
        DefaultAsyncCallback<ArrayList<RequestSummaryItem>> callback = new DefaultAsyncCallback<ArrayList<RequestSummaryItem>>() {
            @Override
            public void success(ArrayList<RequestSummaryItem> result) {
                table.replaceAll(result);

                if (currentObject == null) {
                    table.selectFirst();
                }
            }
        };

        if (responseName != null) {
            gateway.findSummaries(ServiceManager.instance().getService(), endpointName, responseName, callback);
        } else {
            gateway.findSummaries(ServiceManager.instance().getService(), endpointName, callback);
        }
    }

    private void delete() {
        DefaultAsyncCallback<Void> callback = new DefaultAsyncCallback<Void>() {
            @Override
            public void success(Void result) {
                table.remove(table.getFirstSelected());

                if (table.getRowCount() == 0) {
                    detailWindowPanel.clearBody();
                    currentObject = null;
                }
            }
        };

        if (responseName != null) {
            gateway.delete(ServiceManager.instance().getService(), endpointName, responseName, currentObject.getId().getValue(), callback);
        } else {
            gateway.delete(ServiceManager.instance().getService(), endpointName, currentObject.getId().getValue(), callback);
        }
    }

    private void deleteAll() {
        DefaultAsyncCallback<Void> callback = new DefaultAsyncCallback<Void>() {
            @Override
            public void success(Void result) {
                table.removeAll();

                currentObject = null;
                detailWindowPanel.clearBody();
            }
        };

        if (responseName != null) {
            gateway.deleteAll(ServiceManager.instance().getService(), endpointName, responseName, callback);
        } else {
            gateway.deleteAll(ServiceManager.instance().getService(), endpointName, callback);
        }
    }

    private void loadDetails() {
        DefaultAsyncCallback<Request> callback = new DefaultAsyncCallback<Request>() {
            @Override
            public void success(Request result) {
                currentObject = result;

                detailWindowPanel.clearBody();
                detailWindowPanel.addToBody(currentObject.toEditForm());
            }
        };

        if (responseName != null) {
            gateway.findSingle(ServiceManager.instance().getService(), endpointName, responseName, table.getFirstSelected().getId(), callback);
        } else {
            gateway.findSingle(ServiceManager.instance().getService(), endpointName, table.getFirstSelected().getId(), callback);
        }
    }

}