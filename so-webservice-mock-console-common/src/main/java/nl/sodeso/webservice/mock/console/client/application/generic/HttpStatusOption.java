package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class HttpStatusOption extends DefaultOption {

    private static final String KEY_100 = "100";
    private static final String KEY_101 = "101";
    private static final String KEY_102 = "102";

    private static final String KEY_200 = "200";
    private static final String KEY_201 = "201";
    private static final String KEY_202 = "202";
    private static final String KEY_203 = "203";
    private static final String KEY_204 = "204";
    private static final String KEY_205 = "205";
    private static final String KEY_206 = "206";
    private static final String KEY_207 = "207";
    private static final String KEY_208 = "208";
    private static final String KEY_226 = "226";

    private static final String KEY_300 = "300";
    private static final String KEY_301 = "301";
    private static final String KEY_302 = "302";
    private static final String KEY_303 = "303";
    private static final String KEY_304 = "304";
    private static final String KEY_305 = "305";
    private static final String KEY_306 = "306";
    private static final String KEY_307 = "307";
    private static final String KEY_308 = "308";

    private static final String KEY_400 = "400";
    private static final String KEY_401 = "401";
    private static final String KEY_402 = "402";
    private static final String KEY_403 = "403";
    private static final String KEY_404 = "404";
    private static final String KEY_405 = "405";
    private static final String KEY_406 = "406";
    private static final String KEY_407 = "407";
    private static final String KEY_408 = "408";
    private static final String KEY_409 = "409";
    private static final String KEY_410 = "410";
    private static final String KEY_411 = "411";
    private static final String KEY_412 = "412";
    private static final String KEY_413 = "413";
    private static final String KEY_414 = "414";
    private static final String KEY_415 = "415";
    private static final String KEY_416 = "416";
    private static final String KEY_417 = "417";
    private static final String KEY_418 = "418";
    private static final String KEY_421 = "421";
    private static final String KEY_422 = "422";
    private static final String KEY_423 = "423";
    private static final String KEY_424 = "424";
    private static final String KEY_426 = "426";
    private static final String KEY_428 = "428";
    private static final String KEY_429 = "429";
    private static final String KEY_431 = "431";
    private static final String KEY_451 = "451";

    private static final String KEY_500 = "500";
    private static final String KEY_501 = "501";
    private static final String KEY_502 = "502";
    private static final String KEY_503 = "503";
    private static final String KEY_504 = "504";
    private static final String KEY_505 = "505";
    private static final String KEY_506 = "506";
    private static final String KEY_507 = "507";
    private static final String KEY_508 = "508";
    private static final String KEY_510 = "510";
    private static final String KEY_511 = "511";

    public static HttpStatusOption _100 = new HttpStatusOption(KEY_100, "100 ( Continue )");
    public static HttpStatusOption _101 = new HttpStatusOption(KEY_101, "101 ( Switching Protocols )");
    public static HttpStatusOption _102 = new HttpStatusOption(KEY_102, "102 ( Processing (WebDAV; RFC 2518) )");

    public static HttpStatusOption _200 = new HttpStatusOption(KEY_200, "200 ( OK )");
    public static HttpStatusOption _201 = new HttpStatusOption(KEY_201, "201 ( Created )");
    public static HttpStatusOption _202 = new HttpStatusOption(KEY_202, "202 ( Accepted )");
    public static HttpStatusOption _203 = new HttpStatusOption(KEY_203, "203 ( Non-Authoritative Information (since HTTP/1.1) )");
    public static HttpStatusOption _204 = new HttpStatusOption(KEY_204, "204 ( No Content )");
    public static HttpStatusOption _205 = new HttpStatusOption(KEY_205, "205 ( Reset Content )");
    public static HttpStatusOption _206 = new HttpStatusOption(KEY_206, "206 ( Partial Content (RFC 7233) )");
    public static HttpStatusOption _207 = new HttpStatusOption(KEY_207, "207 ( Multi-Status (WebDAV; RFC 4918) )");
    public static HttpStatusOption _208 = new HttpStatusOption(KEY_208, "208 ( Already Reported (WebDAV; RFC 5842) )");
    public static HttpStatusOption _209 = new HttpStatusOption(KEY_226, "226 ( IM Used (RFC 3229) )");

    public static HttpStatusOption _300 = new HttpStatusOption(KEY_300, "300 ( Multiple Choices )");
    public static HttpStatusOption _301 = new HttpStatusOption(KEY_301, "301 ( Moved Permanently )");
    public static HttpStatusOption _302 = new HttpStatusOption(KEY_302, "302 ( Found )");
    public static HttpStatusOption _303 = new HttpStatusOption(KEY_303, "303 ( See Other (since HTTP/1.1) )");
    public static HttpStatusOption _304 = new HttpStatusOption(KEY_304, "304 ( Not Modified (RFC 7232) )");
    public static HttpStatusOption _305 = new HttpStatusOption(KEY_305, "305 ( Use Proxy (since HTTP/1.1) )");
    public static HttpStatusOption _306 = new HttpStatusOption(KEY_306, "306 ( Switch Proxy )");
    public static HttpStatusOption _307 = new HttpStatusOption(KEY_307, "307 ( Temporary Redirect (since HTTP/1.1) )");
    public static HttpStatusOption _308 = new HttpStatusOption(KEY_308, "308 ( Permanent Redirect (RFC 7538) )");

    public static HttpStatusOption _400 = new HttpStatusOption(KEY_400, "400 ( Bad Request )");
    public static HttpStatusOption _401 = new HttpStatusOption(KEY_401, "401 ( Unauthorized (RFC 7235) )");
    public static HttpStatusOption _402 = new HttpStatusOption(KEY_402, "402 ( Payment Required )");
    public static HttpStatusOption _403 = new HttpStatusOption(KEY_403, "403 ( Forbidden )");
    public static HttpStatusOption _404 = new HttpStatusOption(KEY_404, "404 ( Not Found )");
    public static HttpStatusOption _405 = new HttpStatusOption(KEY_405, "405 ( Method Not Allowed )");
    public static HttpStatusOption _406 = new HttpStatusOption(KEY_406, "406 ( Not Acceptable )");
    public static HttpStatusOption _407 = new HttpStatusOption(KEY_407, "407 ( Proxy Authentication Required (RFC 7235) )");
    public static HttpStatusOption _408 = new HttpStatusOption(KEY_408, "408 ( Request Timeout )");
    public static HttpStatusOption _409 = new HttpStatusOption(KEY_409, "409 ( Conflict )");
    public static HttpStatusOption _410 = new HttpStatusOption(KEY_410, "410 ( Gone )");
    public static HttpStatusOption _411 = new HttpStatusOption(KEY_411, "411 ( Length Required )");
    public static HttpStatusOption _412 = new HttpStatusOption(KEY_412, "412 ( Precondition Failed (RFC 7232) )");
    public static HttpStatusOption _413 = new HttpStatusOption(KEY_413, "413 ( Payload Too Large (RFC 7231) )");
    public static HttpStatusOption _414 = new HttpStatusOption(KEY_414, "414 ( URI Too Long (RFC 7231) )");
    public static HttpStatusOption _415 = new HttpStatusOption(KEY_415, "415 ( Unsupported Media Type )");
    public static HttpStatusOption _416 = new HttpStatusOption(KEY_416, "416 ( Range Not Satisfiable (RFC 7233) )");
    public static HttpStatusOption _417 = new HttpStatusOption(KEY_417, "417 ( Expectation Failed )");
    public static HttpStatusOption _418 = new HttpStatusOption(KEY_418, "418 ( I'm a teapot (RFC 2324) )");
    public static HttpStatusOption _421 = new HttpStatusOption(KEY_421, "421 ( Misdirected Request (RFC 7540) )");
    public static HttpStatusOption _422 = new HttpStatusOption(KEY_422, "422 ( Unprocessable Entity (WebDAV; RFC 4918) )");
    public static HttpStatusOption _423 = new HttpStatusOption(KEY_423, "423 ( Locked (WebDAV; RFC 4918) )");
    public static HttpStatusOption _424 = new HttpStatusOption(KEY_424, "424 ( Failed Dependency (WebDAV; RFC 4918) )");
    public static HttpStatusOption _426 = new HttpStatusOption(KEY_426, "426 ( Upgrade Required )");
    public static HttpStatusOption _428 = new HttpStatusOption(KEY_428, "428 ( Precondition Required (RFC 6585) )");
    public static HttpStatusOption _429 = new HttpStatusOption(KEY_429, "429 ( Too Many Requests (RFC 6585) )");
    public static HttpStatusOption _431 = new HttpStatusOption(KEY_431, "431 ( Request Header Fields Too Large (RFC 6585) )");
    public static HttpStatusOption _451 = new HttpStatusOption(KEY_451, "451 ( Unavailable For Legal Reasons )");

    public static HttpStatusOption _500 = new HttpStatusOption(KEY_500, "500 ( Internal Server Error )");
    public static HttpStatusOption _501 = new HttpStatusOption(KEY_501, "501 ( Not Implemented )");
    public static HttpStatusOption _502 = new HttpStatusOption(KEY_502, "502 ( Bad Gateway )");
    public static HttpStatusOption _503 = new HttpStatusOption(KEY_503, "503 ( Service Unavailable )");
    public static HttpStatusOption _504 = new HttpStatusOption(KEY_504, "504 ( Gateway Timeout )");
    public static HttpStatusOption _505 = new HttpStatusOption(KEY_505, "505 ( HTTP Version Not Supported )");
    public static HttpStatusOption _506 = new HttpStatusOption(KEY_506, "506 ( Variant Also Negotiates (RFC 2295) )");
    public static HttpStatusOption _507 = new HttpStatusOption(KEY_507, "507 ( Insufficient Storage (WebDAV; RFC 4918) )");
    public static HttpStatusOption _508 = new HttpStatusOption(KEY_508, "508 ( Loop Detected (WebDAV; RFC 5842) )");
    public static HttpStatusOption _510 = new HttpStatusOption(KEY_510, "510 ( Not Extended (RFC 2774) )");
    public static HttpStatusOption _511 = new HttpStatusOption(KEY_511, "511 ( Network Authentication Required (RFC 6585) )");

    public HttpStatusOption() {}

    public HttpStatusOption(String code, String description) {
        super(code, description);
    }

    public static HttpStatusOption[] getOptions() {
        return new HttpStatusOption[] {
            _100, _101, _102,
            _200, _201, _202, _203, _204, _205, _206, _207, _208, _209,
            _300, _301, _302, _303, _304, _305, _306, _307, _308,
            _400, _401, _402, _403, _404, _405, _406, _407, _408, _409, _410, _411, _412, _413, _414, _415, _416, _417, _418, _421, _422, _423, _424, _426, _428, _429, _431, _451,
            _500, _501, _502, _503, _504, _505, _506, _507, _508, _510, _511
        };
    }

    public static HttpStatusOption fromValue(String contentType) {
        for (HttpStatusOption hso : getOptions()) {
            if (hso.getKey().equals(contentType)) {
                return hso;
            }
        }

        return null;
    }

}
