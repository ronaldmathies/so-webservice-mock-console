package nl.sodeso.webservice.mock.console.client.application.response.actions.variable;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface VariableActionConstants extends Messages {


    @DefaultMessage("Defined variables can be used in functions, there are two types of variables, request and global. Request variables are exist only while an incoming request is being processed. Session variables exist while the browser has a cookie with the name WS-MOCK-SESSION (expires 60 minutes after creation), Global variables exist throughout the lifetime of the running mock server, once the mock server has been restarted all global variables are reset. To use a request variable inside a function for example you need to specify the scope of the variable, a request variable can be accessed with $R{variablename}, a session variable with $S{variablename} and a global variable can be accessed using $G{variablename}. Defining multiple variables with the same name will overwrite the previous set value.")
    String variableDocumentation();

    @DefaultMessage("Name")
    String name();

    @DefaultMessage("Value")
    String value();
}