package nl.sodeso.webservice.mock.console.client.application.response.functions.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class Sample implements Serializable {

    private String name;

    private List<SampleParameter> sampleParameters = new ArrayList<>();

    public Sample() {}

    public Sample(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addSampleParameter(SampleParameter sampleParameter) {
        this.sampleParameters.add(sampleParameter);
    }

    public List<SampleParameter> getSampleParameters() {
        return sampleParameters;
    }

    public void setSampleParameters(List<SampleParameter> sampleParameters) {
        this.sampleParameters = sampleParameters;
    }
}
