package nl.sodeso.webservice.mock.console.client.application.response.functions.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.webservice.mock.console.client.application.response.functions.FunctionGroupOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.FunctionOption;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("webservicemockconsole/endpoint.webservicemock-function")
public interface FunctionRpc extends XsrfProtectedService {

    ArrayList<FunctionGroupOption> groups(ServiceOption serviceOption);
    ArrayList<FunctionOption> functions(ServiceOption serviceOption, FunctionGroupOption group);

}
