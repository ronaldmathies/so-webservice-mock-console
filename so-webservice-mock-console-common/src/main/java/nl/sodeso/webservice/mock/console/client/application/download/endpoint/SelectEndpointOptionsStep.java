package nl.sodeso.webservice.mock.console.client.application.download.endpoint;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;

import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class SelectEndpointOptionsStep extends AbstractWizardStep<DownloadEndpointWizardContext> {

    private static final String KEY_INCLUDE_REQUESTS = "include-responses";
    private static final String KEY_INCLUDE_RESPONSES = "include-requests";

    private BooleanType includeRequests = new BooleanType(false);
    private BooleanType includResponses = new BooleanType(false);

    private transient LegendPanel legendPanel = null;
    private transient RadioButtonGroupField includeRequestsGroupField = null;
    private transient RadioButtonGroupField includeResponsesGroupField = null;

    private transient AbstractWizardStep<DownloadEndpointWizardContext> downloadEndpointsStep = null;

    public SelectEndpointOptionsStep(AbstractWizardStep<DownloadEndpointWizardContext> callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep<DownloadEndpointWizardContext> getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep<DownloadEndpointWizardContext> getNextStep() {
        if (this.downloadEndpointsStep == null) {
            this.downloadEndpointsStep = new DownloadEndpointsStep(this);
        }

        return this.downloadEndpointsStep;
    }

    @Override
    public Widget getBodyWidget() {
        if (this.legendPanel != null) {
            return this.legendPanel;
        }

        this.includeRequestsGroupField = new RadioButtonGroupField(KEY_INCLUDE_REQUESTS, this.includeRequests);
        this.includeRequestsGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.includeRequestsGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.includeRequestsGroupField.setAlignment(Align.HORIZONTAL);

        this.includeResponsesGroupField = new RadioButtonGroupField(KEY_INCLUDE_RESPONSES, this.includResponses);
        this.includeResponsesGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.includeResponsesGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.includeResponsesGroupField.setAlignment(Align.HORIZONTAL);

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntries(
                new EntryWithDocumentation(null, "Include the responses that are registered with the selected endpoints?"),
                new EntryWithLabelAndWidget(KEY_INCLUDE_REQUESTS, "", this.includeResponsesGroupField),
                new EntryWithDocumentation(null, "Include the requests that are logged?"),
                new EntryWithLabelAndWidget(KEY_INCLUDE_RESPONSES, "", this.includeRequestsGroupField)
        );

        this.legendPanel = new LegendPanel("step-2", "Step 2: Export Options");
        this.legendPanel.add(entryForm);

        return this.legendPanel;
    }

    @Override
    public void onBeforePrevious(Trigger trigger) {
        submit();
        super.onBeforePrevious(trigger);
    }

    @Override
    public void onBeforeNext(Trigger trigger) {
        submit();
        super.onBeforeNext(trigger);
    }

    private void submit() {
        getWizardContext().setIncludeRequests(this.includeRequests.getValue());
        getWizardContext().setIncludeResponses(this.includResponses.getValue());
    }
}
