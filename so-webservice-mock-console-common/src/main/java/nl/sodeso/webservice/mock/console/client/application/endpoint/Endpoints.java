package nl.sodeso.webservice.mock.console.client.application.endpoint;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style;
import nl.sodeso.gwt.ui.client.controllers.center.AbstractCenterPanel;
import nl.sodeso.gwt.ui.client.dialog.UnsavedChangesQuestionPanel;
import nl.sodeso.gwt.ui.client.form.button.*;
import nl.sodeso.gwt.ui.client.form.table.summary.SummaryTableField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.menu.ContextMenuItem;
import nl.sodeso.gwt.ui.client.menu.ContextMenuUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.endpoint.duplicate.DuplicateEndpointCallback;
import nl.sodeso.webservice.mock.console.client.application.endpoint.duplicate.DuplicateEndpointProcess;
import nl.sodeso.webservice.mock.console.client.application.endpoint.rpc.EndpointRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.download.endpoint.DownloadEndpointWindowWizard;
import nl.sodeso.webservice.mock.console.client.application.request.RequestsPopup;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;
import nl.sodeso.webservice.mock.console.client.application.tooling.delete.DeleteProcess;
import nl.sodeso.webservice.mock.console.client.application.ui.button.RequestsButton;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadEndpointWindowWizard;

import java.util.ArrayList;
import java.util.Map;

import static nl.sodeso.webservice.mock.console.client.application.ui.menu.HistoryHelper.ARG_UUID;

/**
 * @author Ronald Mathies
 */
public class Endpoints extends AbstractCenterPanel {

    private static final String KEY_TABLE= "table";
    private static final String KEY_DETAILS = "details";

    private HorizontalPanel horizontalPanel = null;
    private WindowPanel detailWindowPanel = null;

    private SummaryTableField<EndpointSummaryItem> table = null;

    private Endpoint currentObject = new Endpoint();

    private Map<String, String> arguments = null;

    private RemoveButton.WithLabel removeButton;
    private RefreshButton.WithIcon refreshButton;
    private DuplicateButton.WithLabel duplicateButton;
    private RequestsButton.WithLabel requestsButton;
    private AddButton.WithIcon addButton;
    private SaveButton.WithLabel saveButton;
    private CancelButton.WithLabel cancelButton;

    private DownloadCloudButton.WithIcon downloadButton;
    private UploadCloudButton.WithIcon uploadButton;

    private EndpointRpcGateway gateway = GWT.create(EndpointRpcGateway.class);

    public Endpoints() {
        super();

        setFullHeight(true);
    }

    public void setArguments(final Map<String, String> arguments) {
        this.arguments = arguments;
    }

    public void beforeAdd(final Trigger trigger) {
        if (horizontalPanel == null) {
            horizontalPanel = new HorizontalPanel();
            horizontalPanel.setFullHeight(true);

            table = new SummaryTableField<>();
            table.setFullHeight(true);
            table.addSelectionChangeHandler(event -> {
                if (table.hasSelection()) {
                    loadDetails();
                }
            });

            ContextMenuUtil.add(table,
                    new ContextMenuItem(DuplicateButton.KEY, Resources.BUTTON_I18N.Duplicate(), () -> prepareDuplicate()),
                    new ContextMenuItem(RemoveButton.KEY, Resources.BUTTON_I18N.Remove(), () -> delete()));


            refreshButton = new RefreshButton.WithIcon((event) -> refresh(null));

            downloadButton = new DownloadCloudButton.WithIcon((event) -> {
                DownloadEndpointWindowWizard.instance().resetWizard();
                DownloadEndpointWindowWizard.instance().setVisible(true);
            });
            uploadButton = new UploadCloudButton.WithIcon((event) -> {
                UploadEndpointWindowWizard.instance().resetWizard();
                UploadEndpointWindowWizard.instance().setVisible(true);
            });
            addButton = new AddButton.WithIcon((event) -> add());

            final WindowPanel tableWindowPanel = new WindowPanel(KEY_TABLE, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .setBodyPadding(false)
                    .addToBody(table)
                    .addToToolbar(Align.LEFT, refreshButton)
                    .addToToolbar(Align.RIGHT, addButton, downloadButton, uploadButton);

            horizontalPanel.addWidget(tableWindowPanel, 20, Style.Unit.PCT);

            duplicateButton = new DuplicateButton.WithLabel((event) -> prepareDuplicate());
            requestsButton = new RequestsButton.WithLabel((event) -> displayRequests());
            removeButton = new RemoveButton.WithLabel((event) -> delete());
            saveButton = new SaveButton.WithLabel((event) -> {
                save(null);
            });
            cancelButton = new CancelButton.WithLabel((event) -> {
                if (!currentObject.isExisting()) {
                    table.selectFirst();
                } else {
                    detailWindowPanel.revert();
                }
            });

            detailWindowPanel = new WindowPanel(KEY_DETAILS, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .addToBody(currentObject.toEditForm())
                    .addToToolbar(Align.LEFT, duplicateButton, requestsButton)
                    .addToFooter(Align.LEFT, removeButton)
                    .addToFooter(Align.RIGHT, saveButton, cancelButton);

            horizontalPanel.addWidget(detailWindowPanel, 80, Style.Unit.PCT);

            evaluateButtonStates();

            add(horizontalPanel);
        }

        Trigger preselect = () -> {
            if (arguments != null && arguments.containsKey(ARG_UUID)) {
                final String endpointName = arguments.get(ARG_UUID);
                for (EndpointSummaryItem summary : table.allItems()) {
                    if (summary.getName().equals(endpointName)) {
                        table.select(summary);
                        break;
                    }
                }
            } else {
                if (!table.hasSelection()) {
                    table.selectFirst();
                }
            }

            trigger.fire();
        };

        if (table.allItems().isEmpty()) {
            refresh(() -> {
                preselect.fire();
            });
        } else {
            preselect.fire();
        }

        ServiceManager.instance().addServiceChangedEventHandler(event -> {
            if (isAttached()) {
                refresh(null);
            }
        });
    }

    private void evaluateButtonStates() {
        saveButton.setEnabled(true);
        cancelButton.setEnabled(currentObject != null && currentObject.isExisting());
        removeButton.setEnabled(currentObject != null && currentObject.isExisting());
        duplicateButton.setEnabled(currentObject != null && currentObject.isExisting());
        requestsButton.setEnabled(currentObject != null && currentObject.isExisting());
    }

    private void displayRequests() {
        RequestsPopup requestsPopup = new RequestsPopup(currentObject.getName().getValue());
        requestsPopup.center(true, true);
    }

    private void prepareDuplicate() {
        saveBeforeAction(this::duplicate);
    }

    private void duplicate() {
        new DuplicateEndpointProcess((name, path) ->
            gateway.duplicate(ServiceManager.instance().getService(), currentObject.getName().getValue(), name.getValue(), path.getValue(),
                new DefaultAsyncCallback<EndpointSummaryItem>() {
                    @Override
                    public void success(EndpointSummaryItem summary) {
                        table.add(summary);
                        table.select(summary);
                    }
                }
            )
        ).start();

    }

    public void refresh(final Trigger trigger) {
        if (ServiceManager.instance().hasService()) {
            gateway.findSummaries(ServiceManager.instance().getService(), new DefaultAsyncCallback<ArrayList<EndpointSummaryItem>>() {
                @Override
                public void success(ArrayList<EndpointSummaryItem> result) {
                    table.replaceAll(result);

                    if (trigger != null) {
                        trigger.fire();
                    }
                }
            });
        }
    }

    private void delete() {
        new DeleteProcess(() -> {
            gateway.delete(ServiceManager.instance().getService(), currentObject.getName().getOriginalValue(), new DefaultAsyncCallback<Void>() {
                @Override
                public void success(Void result) {
                    table.remove(table.getFirstSelected());
                }
            });
        }).start();
    }

    public void saveBeforeAction(final Trigger trigger) {
        if (currentObject.hasChanges()) {
            UnsavedChangesQuestionPanel questionPanel = new UnsavedChangesQuestionPanel(() -> save(trigger));
            questionPanel.center(true, true);
        } else {
            trigger.fire();
        }
    }

    private void save(final Trigger afterSaveTrigger) {
        if (currentObject.hasChanges()) {
            ValidationUtil.validate(result -> {
                if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                    gateway.save(ServiceManager.instance().getService(), currentObject, new DefaultAsyncCallback<EndpointSummaryItem>() {
                        @Override
                        public void success(EndpointSummaryItem summary) {
                            if (!currentObject.isExisting()) {
                                table.add(summary);
                            } else {
                                table.replace(table.getFirstSelected(), summary);
                            }

                            table.select(summary);

                            if (afterSaveTrigger != null) {
                                afterSaveTrigger.fire();
                            }
                        }
                    });
                }
            }, detailWindowPanel);

        }
    }

    private void add() {
        table.deselect(table.getFirstSelected());
        detailWindowPanel.clearBody();

        Scheduler.get().scheduleDeferred(() -> {
            currentObject = new Endpoint();
            detailWindowPanel.addToBody(currentObject.toEditForm());

            evaluateButtonStates();
        });
    }

    private void loadDetails() {
        gateway.getDetails(ServiceManager.instance().getService(), table.getFirstSelected().getName(), new DefaultAsyncCallback<Endpoint>() {
            @Override
            public void success(Endpoint result) {
                currentObject = result;

                detailWindowPanel.clearBody();
                detailWindowPanel.addToBody(currentObject.toEditForm());

                evaluateButtonStates();
            }
        });
    }

}