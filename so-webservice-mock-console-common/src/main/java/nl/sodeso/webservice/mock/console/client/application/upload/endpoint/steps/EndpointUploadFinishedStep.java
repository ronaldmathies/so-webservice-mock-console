package nl.sodeso.webservice.mock.console.client.application.upload.endpoint.steps;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadEndpointWizardContext;

import static nl.sodeso.webservice.mock.console.client.application.Resources.UPLOAD_ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class EndpointUploadFinishedStep extends AbstractWizardStep<UploadEndpointWizardContext> {

    private static final String KEY_MESSAGE = "message";
    private static final String KEY_STEP = "step-";

    private transient LegendPanel legendPanel = null;

    private transient AbstractWizardStep<UploadEndpointWizardContext> uploadEndpointFinishedStep = null;

    public EndpointUploadFinishedStep(AbstractWizardStep<UploadEndpointWizardContext> callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep<UploadEndpointWizardContext> getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep<UploadEndpointWizardContext> getNextStep() {
        if (this.uploadEndpointFinishedStep == null) {
            this.uploadEndpointFinishedStep = new EndpointUploadFinishedStep(this);
        }

        return this.uploadEndpointFinishedStep;
    }

    @Override
    public Widget getBodyWidget() {
        if (this.legendPanel != null) {
            return this.legendPanel;
        }

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntries(
                new EntryWithDocumentation(KEY_MESSAGE, UPLOAD_ENDPOINT_I18N.uploadFinishedDocumentation())
        );

        this.legendPanel = new LegendPanel(KEY_STEP + getStepIndex(), UPLOAD_ENDPOINT_I18N.endpointImportedTitle(getStepIndex()));
        this.legendPanel.add(entryForm);

        return this.legendPanel;
    }
}
