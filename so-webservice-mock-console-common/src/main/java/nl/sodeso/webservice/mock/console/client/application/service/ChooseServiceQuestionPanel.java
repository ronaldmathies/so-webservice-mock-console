package nl.sodeso.webservice.mock.console.client.application.service;

import com.google.gwt.event.dom.client.ClickEvent;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionChosenHandler;

import static nl.sodeso.webservice.mock.console.client.application.Resources.SERVICE_I18N;

/**
 * @author Ronald Mathies
 */
public class ChooseServiceQuestionPanel extends PopupWindowPanel {

    private static final String KEY_CHOOSE_SERVICE = "choose-service";
    private static final String KEY_SERVICE = "service";

    private OptionType<ServiceOption> serviceOption = new OptionType<>();

    private ServiceChosenHandler serviceChosenHandler = null;
    private ServiceComboboxField serviceComboboxField = null;

    public ChooseServiceQuestionPanel(final ServiceChosenHandler serviceChosenHandler) {
        super(KEY_CHOOSE_SERVICE, SERVICE_I18N.title(), Style.INFO);
        this.serviceChosenHandler = serviceChosenHandler;

        this.setWidth("650px");

        this.serviceComboboxField = new ServiceComboboxField(serviceOption);
        this.serviceComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_SERVICE, ValidationMessage.Level.ERROR, serviceOption));

        EntryForm entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_SERVICE, SERVICE_I18N.serviceDocumentation()),
                new EntryWithLabelAndWidget(KEY_SERVICE, SERVICE_I18N.service(), serviceComboboxField));

        addToBody(entryForm);

        addToFooter(Align.RIGHT,
            new OkButton.WithLabel(this::ok),
            new CancelButton.WithLabel((event) -> close()));
    }

    private void ok(ClickEvent event) {
        ValidationUtil.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                ChooseServiceQuestionPanel.this.close();

                serviceChosenHandler.ok(serviceOption);
            }
        }, serviceComboboxField);
    }

}
