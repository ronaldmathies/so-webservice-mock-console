package nl.sodeso.webservice.mock.console.client.application.request;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface RequestConstants extends Messages {

    @DefaultMessage("Endpoints")
    String endpointMenuItem();

    @DefaultMessage("Endpoint")
    String endpointPanel();

    @DefaultMessage("Logging")
    String loggingPanel();

    @DefaultMessage("The condition is used to resolve a unique value anywhere from the request to store the request in the locale storage. This id can then be used to retreive the message from the Webservice Mock.")
    String conditionPanelDocumentation();

    @DefaultMessage("Condition")
    String conditionPanel();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("The path should represent the path of the service.")
    String pathDocumentation();

    @DefaultMessage("Path")
    String path();

    @DefaultMessage("Enabling logging of incomming Requests results in a local storage that will store all incoming Requests for this endpoint. These Requests can then be viewed from within this console or can be resolved using the Webservice Mock API.")
    String enableLoggingDocumentation();

    @DefaultMessage("Enable logging")
    String enableLogging();

    @DefaultMessage("Enter the number of Requests that should be stored, once this number of Requests have been stored the oldest request will be discarded, ideally this number should not be to big since it is stored in-memory.")
    String requestStoreSizeDocumentation();

    @DefaultMessage("Number of Requests")
    String requestStoreSize();

    @DefaultMessage("Name")
    String name();

    @DefaultMessage("Please select the request component that will be used as the basis for the storage id.")
    String requestComponentDocumentation();

    @DefaultMessage("Request component")
    String requestComponent();

    @DefaultMessage("Enter the name of the cookie, header or parameter which will be used as the basis for the storage id.")
    String nameDocumentation();

    @DefaultMessage("Select the type of expression to use to resolve the value which will be used to store the request with.")
    String expressionTypeDocumentation();

    @DefaultMessage("Expression type")
    String expressionType();

    @DefaultMessage("Enter an expression (JSON Path, Regulair Expression or XPath) that will resolve the value which will be used as the storage id.")
    String expressionDocumentation();

    @DefaultMessage("Expression")
    String expression();

    @DefaultMessage("Enter a value that should match with the result of the expression.")
    String matchesDocumentation();

    @DefaultMessage("Matches")
    String matches();
}