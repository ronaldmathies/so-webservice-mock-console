package nl.sodeso.webservice.mock.console.client.application.response.actions;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public interface ActionChosenHandler {

    void ok(OptionType<ActionOption> option);

}
