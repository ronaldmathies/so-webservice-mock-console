package nl.sodeso.webservice.mock.console.client.application.response.functions.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.webservice.mock.console.client.application.response.functions.FunctionGroupOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.FunctionOption;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface FunctionRpcAsync {

    void groups(ServiceOption serviceOption, AsyncCallback<ArrayList<FunctionGroupOption>> result);
    void functions(ServiceOption serviceOption, FunctionGroupOption group, AsyncCallback<ArrayList<FunctionOption>> result);

}
