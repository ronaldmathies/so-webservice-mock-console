package nl.sodeso.webservice.mock.console.client.application.download.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.webservice.mock.console.client.application.download.endpoint.DownloadEndpointWizardContext;
import nl.sodeso.webservice.mock.console.client.application.download.endpoint.DownloadEndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface DownloadRpcAsync {

    void prepareEndpointExport(ServiceOption serviceOption, DownloadEndpointWizardContext context, AsyncCallback<ArrayList<DownloadEndpointSummaryItem>> result);

}
