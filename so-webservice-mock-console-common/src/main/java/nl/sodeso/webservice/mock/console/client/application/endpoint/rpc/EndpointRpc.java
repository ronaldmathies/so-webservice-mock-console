package nl.sodeso.webservice.mock.console.client.application.endpoint.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.webservice.mock.console.client.application.endpoint.Endpoint;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("webservicemockconsole/endpoint.webservicemock-endpoint")
public interface EndpointRpc extends XsrfProtectedService {

    ArrayList<EndpointSummaryItem> findSummaries(ServiceOption serviceOption);
    Endpoint getDetails(ServiceOption serviceOption, String name);

    ValidationResult isNameUnique(ServiceOption serviceOption, String name);
    ValidationResult isPathUnique(ServiceOption serviceOption, String path);

    EndpointSummaryItem save(ServiceOption serviceOption, Endpoint endpoint) throws RemoteException;
    EndpointSummaryItem duplicate(ServiceOption serviceOption, String name, String newName, String newPath);

    ArrayList<DefaultOption> asOptions(ServiceOption serviceOption);

    void delete(ServiceOption serviceOption, String name);

}
