package nl.sodeso.webservice.mock.console.client.application.ui.menu;

import com.google.gwt.user.client.History;

import static nl.sodeso.gwt.ui.client.controllers.menu.ArgumentsUtil.*;

/**
 * @author Ronald Mathies
 */
public class HistoryHelper {

    public static final String ARG_UUID = "uuid";

    /**
     * Pushes a new token on the history with the specified token (key) and arguments.
     *
     * @param key the history token.
     * @param uuid the uuid
     */
    public static void click(String key, String uuid) {
        History.newItem(
            toToken(key,
                toArgs(
                    add(ARG_UUID, uuid)
                )
            )
        );
    }

}
