package nl.sodeso.webservice.mock.console.client.application.request;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.TextHeader;
import com.google.gwt.user.client.Window;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionModel;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.form.table.DefaultEmptyTableMessage;
import nl.sodeso.webservice.mock.console.client.application.generic.header.Header;

import java.util.Comparator;

/**
 * @author Ronald Mathies
 */
public class HeaderTable extends CellTableField<Header> {

    public HeaderTable() {
        super("key", 1000);
        setSelectionModel(new NoSelectionModel<>());

        TextColumn<Header> nameColumn = new TextColumn<Header>() {
            @Override
            public String getValue(Header header) {
                if (header.getHeaderOption().getValue().isOther()) {
                    return header.getOtherHeaderOption().getValue();
                }

                return header.getHeaderOption().getValue().getDescription();
            }
        };
        addColumn(nameColumn, new TextHeader("Name"));

        TextColumn<Header> valueColumn = new TextColumn<Header>() {
            @Override
            public String getValue(Header header) {
                return header.getValue().getValue();
            }
        };
        addColumn(valueColumn, new TextHeader("Value"));

        ColumnSortEvent.ListHandler<Header> listHandler = new ColumnSortEvent.ListHandler<>(getListDataProvider().getList());
        listHandler.setComparator(nameColumn, Comparator.comparing(header -> {
            if (header.getHeaderOption().getValue().isOther()) {
                return header.getOtherHeaderOption().getValue();
            }

            return header.getHeaderOption().getValue().getDescription();
        }));
        this.addColumnSortHandler(listHandler);

        // Set the default sorting to ascending.
        valueColumn.setDefaultSortAscending(true);

        // Set the sorting order of the cell list to the first and only column.
        ColumnSortList.ColumnSortInfo columnSortInfo = new ColumnSortList.ColumnSortInfo(nameColumn, true);
        this.getColumnSortList().push(columnSortInfo);

        this.setColumnWidth(0, 20, com.google.gwt.dom.client.Style.Unit.PCT);
        this.setColumnWidth(1, 80, com.google.gwt.dom.client.Style.Unit.PCT);
        this.setEmptyTableWidget(new DefaultEmptyTableMessage("no-headers", "No headers"));


    }

}
