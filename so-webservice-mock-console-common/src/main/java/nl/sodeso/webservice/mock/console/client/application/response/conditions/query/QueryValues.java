package nl.sodeso.webservice.mock.console.client.application.response.conditions.query;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionConfig;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class QueryValues extends WidgetCollectionField<QueryValue> {

    public QueryValues() {
        super(WidgetCollectionConfig.DEFAULT);
    }

    @Override
    public WidgetSupplier<QueryValue> createWidgetSupplier() {
        return callback -> callback.created(new QueryValue());
    }
}
