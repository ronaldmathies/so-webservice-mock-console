package nl.sodeso.webservice.mock.console.client.application.request.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class RequestRpcGateway implements RpcGateway<RequestRpcAsync>, RequestRpcAsync {

    /**
     * {@inheritDoc}
     */
    public RequestRpcAsync getRpcAsync() {
        return GWT.create(RequestRpc.class);
    }
}
