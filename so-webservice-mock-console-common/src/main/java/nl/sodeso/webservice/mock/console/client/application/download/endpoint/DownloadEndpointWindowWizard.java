package nl.sodeso.webservice.mock.console.client.application.download.endpoint;

import nl.sodeso.gwt.ui.client.form.button.CloseButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.wizard.WizardWindow;

/**
 * @author Ronald Mathies
 */
public class DownloadEndpointWindowWizard extends WizardWindow<DownloadEndpointWizardContext> {

    private static DownloadEndpointWindowWizard instance = null;

    public DownloadEndpointWindowWizard() {
        super(new PopupWindowPanel("export", "Export", WindowPanel.Style.INFO), new DownloadEndpointWizardContext());
        getWindow().showCollapseButton(true);
        getWindow().setWidth("500px");

        getWindow().addToFooter(Align.LEFT, new CloseButton.WithLabel((event) -> setVisible(false)));
    }

    public static DownloadEndpointWindowWizard instance() {
        if (instance == null) {
            instance = new DownloadEndpointWindowWizard();
        }

        return instance;
    }

    public void resetWizard() {
        setWizardStep(new SelectEndpointsStep(null));
    }

}
