package nl.sodeso.webservice.mock.console.client.application.request;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.TextHeader;
import com.google.gwt.view.client.NoSelectionModel;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.form.table.DataGridField;
import nl.sodeso.gwt.ui.client.form.table.DefaultEmptyTableMessage;
import nl.sodeso.gwt.ui.client.form.table.summary.AbstractSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.generic.cookie.Cookie;

import java.util.Comparator;
import java.util.function.Function;

/**
 * @author Ronald Mathies
 */
public class CookieTable extends CellTableField<Cookie> {

    public CookieTable() {
        super("key", 1000);
        setSelectionModel(new NoSelectionModel<>());

        TextColumn<Cookie> nameColumn = new TextColumn<Cookie>() {
            @Override
            public String getValue(Cookie cookie) {
                return cookie.getName().getValue();
            }
        };
        addColumn(nameColumn, new TextHeader("Name"));

        TextColumn<Cookie> domainColumn = new TextColumn<Cookie>() {
            @Override
            public String getValue(Cookie cookie) {
                return cookie.getDomain().getValue();
            }
        };
        addColumn(domainColumn, new TextHeader("Domain"));

        TextColumn<Cookie> pathColumn = new TextColumn<Cookie>() {
            @Override
            public String getValue(Cookie cookie) {
                return cookie.getPath().getValue();
            }
        };
        addColumn(pathColumn, new TextHeader("Path"));

        TextColumn<Cookie> valueColumn = new TextColumn<Cookie>() {
            @Override
            public String getValue(Cookie cookie) {
                return cookie.getValue().getValue();
            }
        };
        addColumn(valueColumn, new TextHeader("Value"));

        TextColumn<Cookie> magAgeColumn = new TextColumn<Cookie>() {
            @Override
            public String getValue(Cookie cookie) {
                return cookie.getMaxAge().getValue();
            }
        };
        addColumn(magAgeColumn, new TextHeader("Max-Age"));

        ColumnSortEvent.ListHandler<Cookie> listHandler = new ColumnSortEvent.ListHandler<>(getListDataProvider().getList());
        listHandler.setComparator(nameColumn, Comparator.comparing(cookie -> cookie.getName().getValue()));
        this.addColumnSortHandler(listHandler);

        // Set the default sorting to ascending.
        nameColumn.setDefaultSortAscending(true);

        // Set the sorting order of the cell list to the first and only column.
        ColumnSortList.ColumnSortInfo columnSortInfo = new ColumnSortList.ColumnSortInfo(nameColumn, true);
        this.getColumnSortList().push(columnSortInfo);

//        this.setColumnWidth(0, 100, com.google.gwt.dom.client.Style.Unit.PCT);
        this.setEmptyTableWidget(new DefaultEmptyTableMessage("no-cookies", "No cookies"));


    }

}
