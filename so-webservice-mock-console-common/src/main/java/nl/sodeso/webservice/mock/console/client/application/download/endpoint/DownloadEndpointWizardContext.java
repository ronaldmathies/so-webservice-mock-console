package nl.sodeso.webservice.mock.console.client.application.download.endpoint;

import nl.sodeso.gwt.ui.client.wizard.WizardContext;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class DownloadEndpointWizardContext extends WizardContext {

    private boolean includeResponses;
    private boolean includeRequests;

    private ArrayList<DownloadEndpointSummaryItem> endpointSummaryItems = new ArrayList<>();

    public DownloadEndpointWizardContext() {
    }

    public ArrayList<DownloadEndpointSummaryItem> getEndpointSummaryItems() {
        return endpointSummaryItems;
    }

    public void setEndpointSummaryItems(ArrayList<DownloadEndpointSummaryItem> endpointSummaryItems) {
        this.endpointSummaryItems = endpointSummaryItems;
    }

    public boolean isIncludeResponses() {
        return includeResponses;
    }

    public void setIncludeResponses(boolean includeResponses) {
        this.includeResponses = includeResponses;
    }

    public boolean isIncludeRequests() {
        return includeRequests;
    }

    public void setIncludeRequests(boolean includeRequests) {
        this.includeRequests = includeRequests;
    }
}
