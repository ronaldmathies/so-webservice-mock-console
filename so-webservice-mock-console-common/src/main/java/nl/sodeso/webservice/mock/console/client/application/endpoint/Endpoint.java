package nl.sodeso.webservice.mock.console.client.application.endpoint;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.logging.Logging;

import java.io.Serializable;

import static nl.sodeso.webservice.mock.console.client.application.Resources.ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class Endpoint implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_NAME = "name";
    private static final String KEY_PATH = "path";
    private static final String KEY_GENERAL = "general";
    private static final String KEY_LOGGING = "logging";

    private boolean isExisting = false;
    private StringType name = new StringType("");
    private StringType path = new StringType("");

    private Logging logging = new Logging();

    private transient EntryForm entryForm = null;

    private transient LegendPanel generalLegendPanel = null;
    private transient TextField nameTextField = null;
    private transient TextField pathTextField = null;
    private transient LegendPanel loggingLegendPanel = null;

    public Endpoint() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupGeneralPanel();
        setupLoggingPanel();

        entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithContainer(KEY_GENERAL, this.generalLegendPanel));
        entryForm.addEntry(new EntryWithContainer(KEY_LOGGING, this.loggingLegendPanel));

        this.nameTextField.setFocus();

        return this.entryForm;
    }

    private void setupGeneralPanel() {
        nameTextField = new TextField<>(KEY_NAME, this.name)
            .addValidationRules(
            new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return nameTextField.getValue();
                }
            },
            new NameUniqueValidationRule() {
                @Override
                public StringType getName() {
                    return name;
                }

                @Override
                public boolean isApplicable() {
                    return super.isApplicable() && !isExisting();
                }
            }
            );
        pathTextField = new TextField<>(KEY_PATH, this.path)
            .addValidationRules(
                new MandatoryValidationRule(KEY_PATH, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return pathTextField.getValue();
                    }
                },
                new PathUniqueValidationRule() {
                    @Override
                    public StringType getPath() {
                        return path;
                    }

                    @Override
                    public boolean isApplicable() {
                        return super.isApplicable() && !isExisting();
                    }
                }
            );

        EntryForm form = new EntryForm(null)
                .addEntries(
                        new EntryWithLabelAndWidget(KEY_NAME, ENDPOINT_I18N.name(), nameTextField),
                        new EntryWithDocumentation(KEY_PATH, ENDPOINT_I18N.pathDocumentation()),
                        new EntryWithLabelAndWidget(KEY_PATH, ENDPOINT_I18N.path(), pathTextField)
                );

        this.generalLegendPanel = new LegendPanel(null, ENDPOINT_I18N.endpointPanel());
        this.generalLegendPanel.add(form);
    }

    private void setupLoggingPanel() {
        this.loggingLegendPanel = new LegendPanel(null, ENDPOINT_I18N.loggingPanel());
        this.loggingLegendPanel.add(logging.toEditForm());
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entryForm);
    }

    public Boolean isExisting() {
        return isExisting;
    }

    public void setIsExisting(boolean isExisting) {
        this.isExisting = isExisting;
    }

    public StringType getName() {
        return this.name;
    }

    public void setName(StringType name) {
        this.name = name;
    }

    public StringType getPath() {
        return path;
    }

    public void setPath(StringType path) {
        this.path = path;
    }

    public void setLogging(Logging logging) {
        this.logging = logging;
    }

    public Logging getLogging() {
        return this.logging;
    }
}
