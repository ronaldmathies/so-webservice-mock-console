package nl.sodeso.webservice.mock.console.client.application.response.conditions.query;

import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import static nl.sodeso.webservice.mock.console.client.application.Resources.*;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class QueryValue implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private static final String KEY_NAME = "name";
    private static final String KEY_VALUE = "value";

    private String uuid = null;
    private StringType name = new StringType();
    private StringType value = new StringType();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient TextField<StringType> nameField = null;
    private transient TextField<StringType> valueField = null;

    public QueryValue() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        nameField = new TextField<>(KEY_NAME, this.name)
                .setPlaceholder(QUERY_I18N.namePlaceholder());
        nameField.addValidationRule(new MandatoryValidationRule(KEY_NAME, QUERY_I18N.name(), ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return nameField.getValue();
            }
        });
        valueField = new TextField<>(KEY_VALUE, this.value)
            .setPlaceholder(QUERY_I18N.valuePlaceholder());
        valueField.addValidationRule(new MandatoryValidationRule(KEY_VALUE, QUERY_I18N.value(), ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return valueField.getValue();
            }
        });

        entry = new EntryWithWidgetAndWidget(null, nameField, new InsertFunctionButton(valueField));

        return entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getName() {
        return this.name;
    }

    public void setName(StringType name) {
        this.name = name;
    }

    public StringType getValue() {
        return this.value;
    }

    public void setValue(StringType value) {
        this.value = value;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }


}
