package nl.sodeso.webservice.mock.console.client.application.response.conditions.method;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Condition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionOption;

import static nl.sodeso.webservice.mock.console.client.application.Resources.METHOD_CONDITION_I18N;

/**
 * @author Ronald Mathies
 */
public class MethodCondition extends Condition {

    private static final String KEY_METHOD_CONDITION = "method-condition";
    private static final String KEY_METHOD = "method";

    private OptionType<MethodOption> methodOption = new OptionType<>(MethodOption.POST);

    private transient MethodComboboxField methodComboboxField = null;

    private transient EntryWithSingleWidget entry = null;

    public MethodCondition() {
    }

    public ConditionOption getConditionOption() {
        return ConditionOption.METHOD;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        methodComboboxField = new MethodComboboxField(methodOption);
        methodComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_METHOD, ValidationMessage.Level.ERROR, methodOption));

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_METHOD, METHOD_CONDITION_I18N.methodConditionDocumentation()),
                new EntryWithLabelAndWidget(KEY_METHOD, METHOD_CONDITION_I18N.method(), methodComboboxField)
            );

        LegendPanel legendPanel = new LegendPanel(null, METHOD_CONDITION_I18N.methodCondition());
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_METHOD_CONDITION, legendPanel);
        return entry;
    }

    public OptionType<MethodOption> getMethodOption() {
        return methodOption;
    }

    public void setMethodOption(OptionType<MethodOption> methodOption) {
        this.methodOption = methodOption;
    }
}