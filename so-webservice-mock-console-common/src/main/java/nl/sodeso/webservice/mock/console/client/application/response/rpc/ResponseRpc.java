package nl.sodeso.webservice.mock.console.client.application.response.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.webservice.mock.console.client.application.response.Response;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("webservicemockconsole/endpoint.webservicemock-response")
public interface ResponseRpc extends XsrfProtectedService {

    ArrayList<ResponseSummaryItem> findSummaries(ServiceOption serviceOption, String name);
    Response getDetails(ServiceOption serviceOption, String endpointName, String responseName);

    ValidationResult isNameUnique(ServiceOption serviceOption, String endpointName, String responseName);

    ResponseSummaryItem save(ServiceOption serviceOption,String endpointName, Response response) throws RemoteException;
    ResponseSummaryItem duplicate(ServiceOption serviceOption,String endpointName, String responseName, String newResponseName);

    void delete(ServiceOption serviceOption,String endpointName, String responseName);
}
