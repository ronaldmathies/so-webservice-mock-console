package nl.sodeso.webservice.mock.console.client.application.response;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style;
import nl.sodeso.gwt.ui.client.controllers.center.AbstractCenterPanel;
import nl.sodeso.gwt.ui.client.dialog.UnsavedChangesQuestionPanel;
import nl.sodeso.gwt.ui.client.form.button.*;
import nl.sodeso.gwt.ui.client.form.table.summary.SummaryTableField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.menu.ContextMenuItem;
import nl.sodeso.gwt.ui.client.menu.ContextMenuUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.endpoint.rpc.EndpointRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.download.endpoint.DownloadEndpointWindowWizard;
import nl.sodeso.webservice.mock.console.client.application.request.RequestsPopup;
import nl.sodeso.webservice.mock.console.client.application.response.rpc.ResponseRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;
import nl.sodeso.webservice.mock.console.client.application.response.duplicate.DuplicateResponseProcess;
import nl.sodeso.webservice.mock.console.client.application.tooling.delete.DeleteProcess;
import nl.sodeso.webservice.mock.console.client.application.ui.button.RequestsButton;

import java.util.ArrayList;
import java.util.Map;

import static nl.sodeso.webservice.mock.console.client.application.Resources.ENDPOINT_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class Responses extends AbstractCenterPanel {

    private static final String KEY_ENDPOINT_TABLE = "endpoint-table";
    private static final String KEY_RESPONSE_TABLE = "response-table";
    private static final String KEY_DETAILS = "details";

    private HorizontalPanel horizontalPanel = null;
    private VerticalPanel verticalPanel = null;
    private WindowPanel detailWindowPanel = null;

    private SummaryTableField<EndpointSummaryItem> endpointsTable = null;
    private SummaryTableField<ResponseSummaryItem> responsesTable = null;

    private Response currentObject = new Response();

    private Map<String, String> arguments = null;

    private RemoveButton.WithLabel removeButton;
    private RefreshButton.WithIcon refreshEndpointsButton;
    private RefreshButton.WithIcon refreshResponsesButton;
    private DuplicateButton.WithLabel duplicateButton;
    private RequestsButton.WithLabel requestsButton;
    private AddButton.WithIcon addButton;
    private SaveButton.WithLabel saveButton;
    private CancelButton.WithLabel cancelButton;

    private DownloadCloudButton.WithIcon downloadButton;
    private UploadCloudButton.WithIcon uploadButton;

    private EndpointRpcGateway endpointGateway = GWT.create(EndpointRpcGateway.class);
    private ResponseRpcGateway responseGateway = GWT.create(ResponseRpcGateway.class);

    public Responses() {
        super();

        setFullHeight(true);
    }

    public void setArguments(final Map<String, String> arguments) {
        this.arguments = arguments;
    }

    public void beforeAdd(final Trigger trigger) {
        if (horizontalPanel == null) {
            horizontalPanel = new HorizontalPanel();
            horizontalPanel.setFullHeight(true);

            // Endpoints table.
            endpointsTable = new SummaryTableField<>();
            endpointsTable.setFullHeight(true);
            endpointsTable.addSelectionChangeHandler(event -> {
                if (endpointsTable.hasSelection()) {
                    refreshResponsesTable();
                }
            });

            ContextMenuUtil.add(endpointsTable,
                    new ContextMenuItem(DownloadCloudButton.KEY, "Export", () -> exportEndpoint()));

            refreshEndpointsButton = new RefreshButton.WithIcon((event) -> refreshEndpointsTable(null));

            downloadButton = new DownloadCloudButton.WithIcon((event) -> {
                exportEndpoint();
            });
            uploadButton = new UploadCloudButton.WithIcon((event) -> {});

            final WindowPanel endpointsTableWindowPanel = new WindowPanel(KEY_ENDPOINT_TABLE, ENDPOINT_I18N.endpoints(), WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .setBodyPadding(false)
                    .setBody(endpointsTable)
                    .addToToolbar(Align.LEFT, refreshEndpointsButton)
                    .addToToolbar(Align.RIGHT, downloadButton, uploadButton);


            // Responses table
            responsesTable = new SummaryTableField<>();
            responsesTable.setFullHeight(true);
            responsesTable.addSelectionChangeHandler(event -> {
                if (responsesTable.hasSelection()) {
                    loadDetails();
                }
            });

            ContextMenuUtil.add(responsesTable,
                new ContextMenuItem(DuplicateButton.KEY, Resources.BUTTON_I18N.Duplicate(), () -> prepareDuplicate()),
                new ContextMenuItem(RemoveButton.KEY, Resources.BUTTON_I18N.Remove(), () -> delete()));

            refreshResponsesButton = new RefreshButton.WithIcon((event) -> refreshResponsesTable());
            addButton = new AddButton.WithIcon((event) -> add());

            final WindowPanel responsesTableWindowPanel = new WindowPanel(KEY_RESPONSE_TABLE, RESPONSE_I18N.responses(), WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .setBodyPadding(false)
                    .setBody(responsesTable)
                    .addToToolbar(Align.LEFT, refreshResponsesButton)
                    .addToToolbar(Align.RIGHT, addButton);


            duplicateButton = new DuplicateButton.WithLabel((event) -> prepareDuplicate());
            requestsButton = new RequestsButton.WithLabel((event) -> displayRequests());
            removeButton = new RemoveButton.WithLabel((event) -> delete());
            saveButton = new SaveButton.WithLabel((event) -> {
                save( null );
            });
            cancelButton = new CancelButton.WithLabel((event) -> {
                detailWindowPanel.revert();
            });

            detailWindowPanel = new WindowPanel(KEY_DETAILS, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .setBody(currentObject.toEditForm())
                    .addToToolbar(Align.LEFT, duplicateButton, requestsButton)
                    .addToFooter(Align.LEFT, removeButton)
                    .addToFooter(Align.RIGHT, saveButton, cancelButton);

            verticalPanel = new VerticalPanel();
            verticalPanel.setFullHeight(true);

            verticalPanel.addWidget(endpointsTableWindowPanel, 40, Style.Unit.PCT);
            verticalPanel.addWidget(responsesTableWindowPanel, 60, Style.Unit.PCT);

            horizontalPanel.addWidget(verticalPanel, 20, Style.Unit.PCT);
            horizontalPanel.addWidget(detailWindowPanel, 80, Style.Unit.PCT);

            evaluateButtonStates();

            add(horizontalPanel);
        }

        if (endpointsTable.allItems().isEmpty()) {
            refreshEndpointsTable(() -> {
                trigger.fire();
            });
        } else {
            trigger.fire();
        }

        ServiceManager.instance().addServiceChangedEventHandler(event -> {
            if (isAttached()) {
                refreshEndpointsTable(null);
            }
        });
    }

    private void exportEndpoint() {
        DownloadEndpointWindowWizard.instance().resetWizard();
        DownloadEndpointWindowWizard.instance().setVisible(true);
    }

//    private void exportCurrentSelectedEndpoint() {
//        ExportableEndpointSummaryItem exportableEndpointSummaryItem = new ExportableEndpointSummaryItem(endpointsTable.getFirstSelected());
//        exportableEndpointSummaryItem.setSelected(true);
//        exportableEndpointSummaryItem.setExportFilename("endpoint.json");
//
//        ArrayList<ExportableEndpointSummaryItem> endpointsToExport = new ArrayList<>();
//        endpointsToExport.add(exportableEndpointSummaryItem);
//
//        exportGateway.prepareExport(ServiceManager.instance().getService(), endpointsToExport, new DefaultAsyncCallback<ArrayList<ExportableEndpointSummaryItem>>() {
//            @Override
//            public void success(ArrayList<ExportableEndpointSummaryItem> result) {
//
//            }
//        });
//    }

    private void evaluateButtonStates() {
        saveButton.setEnabled(currentObject != null);
        addButton.setEnabled(endpointsTable.hasSelection());
        refreshResponsesButton.setEnabled(endpointsTable.hasSelection());
        downloadButton.setEnabled(endpointsTable.hasSelection());
        uploadButton.setEnabled(endpointsTable.hasSelection());
        cancelButton.setEnabled(currentObject != null && currentObject.isExisting());
        removeButton.setEnabled(currentObject != null && currentObject.isExisting());
        duplicateButton.setEnabled(currentObject != null && currentObject.isExisting());
        requestsButton.setEnabled(currentObject != null && currentObject.isExisting());
    }

    private void prepareDuplicate() {
        saveBeforeAction(this::duplicate);
    }

    private void displayRequests() {
        RequestsPopup requestsPopup =
                new RequestsPopup(endpointsTable.getFirstSelected().getName(), currentObject.getName().getValue());
        requestsPopup.center(true, true);
    }

    private void duplicate() {
        new DuplicateResponseProcess(newResponseName -> {
            responseGateway.duplicate(ServiceManager.instance().getService(), endpointsTable.getFirstSelected().getName(), currentObject.getName().getValue(), newResponseName.getValueAsString(), new DefaultAsyncCallback<ResponseSummaryItem>() {
                @Override
                public void success(ResponseSummaryItem summary) {
                    responsesTable.add(summary);
                    responsesTable.select(summary);
                }
            });
        }, endpointsTable.getFirstSelected()).start();
    }

    public void refreshEndpointsTable(final Trigger trigger) {
        if (ServiceManager.instance().hasService()) {
            endpointGateway.findSummaries(ServiceManager.instance().getService(), new DefaultAsyncCallback<ArrayList<EndpointSummaryItem>>() {
                @Override
                public void success(ArrayList<EndpointSummaryItem> result) {

                    endpointsTable.replaceAll(result);
                    if (!endpointsTable.selectFirst()) {
                        responsesTable.removeAll();

                        currentObject = new Response();
                        detailWindowPanel.setBody(currentObject.toEditForm());
                        evaluateButtonStates();
                    }

                    if (trigger != null) {
                        trigger.fire();
                    }
                }
            });
        }
    }

    public void refreshResponsesTable() {
        responseGateway.findSummaries(ServiceManager.instance().getService(), endpointsTable.getFirstSelected().getName(), new DefaultAsyncCallback<ArrayList<ResponseSummaryItem>>() {
            @Override
            public void success(ArrayList<ResponseSummaryItem> result) {
                responsesTable.replaceAll(result);
                if (!responsesTable.selectFirst()) {
                    currentObject = new Response();
                    currentObject.setEndpoint(endpointsTable.getFirstSelected());
                    detailWindowPanel.setBody(currentObject.toEditForm());
                    evaluateButtonStates();
                }

            }
        });
    }

    private void delete() {
        new DeleteProcess(() -> {
            responseGateway.delete(ServiceManager.instance().getService(), endpointsTable.getFirstSelected().getName(), currentObject.getName().getOriginalValue(), new DefaultAsyncCallback<Void>() {
                @Override
                public void success(Void result) {
                    responsesTable.remove(responsesTable.getFirstSelected());
                }
            });
        }).start();
    }

    private void saveBeforeAction(final Trigger trigger) {
        if (currentObject.hasChanges()) {
            UnsavedChangesQuestionPanel questionPanel = new UnsavedChangesQuestionPanel(() -> save(trigger));
            questionPanel.center(true, true);
        } else {
            trigger.fire();
        }
    }

    private void save(final Trigger afterSaveTrigger) {
        if (currentObject.hasChanges()) {
            ValidationUtil.validate(result -> {
                if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {

                    currentObject.preSaveTrigger(() -> {
                        responseGateway.save(ServiceManager.instance().getService(), endpointsTable.getFirstSelected().getName(), currentObject, new DefaultAsyncCallback<ResponseSummaryItem>() {
                            @Override
                            public void success(ResponseSummaryItem summary) {
                                if (!currentObject.isExisting()) {
                                    responsesTable.add(summary);
                                } else {
                                    responsesTable.replace(responsesTable.getFirstSelected(), summary);
                                }

                                responsesTable.select(summary);

                                if (afterSaveTrigger != null) {
                                    afterSaveTrigger.fire();
                                }
                            }
                        });
                    });
                }
            }, detailWindowPanel);

        }
    }

    private void add() {
        responsesTable.deselect(responsesTable.getFirstSelected());

        detailWindowPanel.clearBody();

        Scheduler.get().scheduleDeferred(() -> {
            currentObject = new Response();
            detailWindowPanel.addToBody(currentObject.toEditForm());

            evaluateButtonStates();
        });
    }

    private void loadDetails() {
        ResponseSummaryItem selectedItem = responsesTable.getFirstSelected();
        responseGateway.getDetails(ServiceManager.instance().getService(), endpointsTable.getFirstSelected().getName(), selectedItem.getResponseName(), new DefaultAsyncCallback<Response>() {
            @Override
            public void success(Response result) {
                currentObject = result;
                currentObject.setEndpoint(endpointsTable.getFirstSelected());

                detailWindowPanel.setBody(currentObject.toEditForm());

                evaluateButtonStates();
            }
        });
    }

}