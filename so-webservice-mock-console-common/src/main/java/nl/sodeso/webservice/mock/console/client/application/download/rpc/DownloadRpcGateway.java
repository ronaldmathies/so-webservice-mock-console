package nl.sodeso.webservice.mock.console.client.application.download.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class DownloadRpcGateway implements RpcGateway<DownloadRpcAsync>, DownloadRpcAsync {

    /**
     * {@inheritDoc}
     */
    public DownloadRpcAsync getRpcAsync() {
        return GWT.create(DownloadRpc.class);
    }
}
