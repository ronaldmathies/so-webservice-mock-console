package nl.sodeso.webservice.mock.console.client.application.response.functions.model;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class SampleParameter implements Serializable{

    private String name;
    private String value;

    public SampleParameter() {}

    public SampleParameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
