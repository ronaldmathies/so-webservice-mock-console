package nl.sodeso.webservice.mock.console.client.application.response.conditions.path;

import nl.sodeso.gwt.ui.client.form.*;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Condition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionOption;

import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.ENDPOINT_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.PATH_I18N;

/**
 * @author Ronald Mathies
 */
public class PathCondition extends Condition {

    private static final String KEY_PATH_CONDITION = "path-condition";
    private static final String KEY_PATH = "path";
    private static final String KEY_IS_PATH_REGEXP = "regexp";

    private StringType path = new StringType();
    private BooleanType isRegexp = new BooleanType();

    private transient EntryWithSingleWidget entry = null;
    private transient TextField pathField = null;

    private transient RadioButtonGroupField isRegexpGroupField = null;

    public PathCondition() {
    }

    public ConditionOption getConditionOption() {
        return ConditionOption.PATH;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        pathField = new TextField<>(KEY_PATH, this.path)
            .setPlaceholder(PATH_I18N.pathPlaceholder());
        pathField.addValidationRule(
            new MandatoryValidationRule(KEY_PATH, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return pathField.getValue();
                }
            }
        );

        isRegexpGroupField = new RadioButtonGroupField(KEY_IS_PATH_REGEXP, this.isRegexp);
        isRegexpGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        isRegexpGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        isRegexpGroupField.setAlignment(Align.HORIZONTAL);

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_PATH_CONDITION, PATH_I18N.pathConditionDocumentation()),
                new EntryWithLabelAndWidget(KEY_PATH, PATH_I18N.path(), pathField),
                new EntryWithLabelAndWidget(KEY_IS_PATH_REGEXP, PATH_I18N.isPathRegexp(), isRegexpGroupField)
            );

        LegendPanel legendPanel = new LegendPanel(null, PATH_I18N.pathCondition());
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_PATH_CONDITION, legendPanel);
        return entry;
    }

    public StringType getPath() {
        return path;
    }

    public void setPath(StringType path) {
        this.path = path;
    }

    public BooleanType isRegexp() {
        return isRegexp;
    }

    public void setRegexp(BooleanType isRegexp) {
        this.isRegexp = isRegexp;
    }
}