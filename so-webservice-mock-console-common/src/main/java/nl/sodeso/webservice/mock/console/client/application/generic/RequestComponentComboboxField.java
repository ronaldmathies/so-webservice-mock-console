package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class RequestComponentComboboxField extends ComboboxField<RequestComponentOption> {

    public RequestComponentComboboxField(OptionType<RequestComponentOption> optionValue) {
        super(optionValue);
        addItems(RequestComponentOption.getOptions());
    }
}
