package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class ContentTypeOption extends DefaultOption {

    private static final String KEY_OTHER = "other";
    private static final String KEY_APPLICATION_XML = "application/xml";
    private static final String KEY_APPLICATION_SOAP_XML = "application/soap+xml";
    private static final String KEY_APPLICATION_JSON = "application/json";
    private static final String KEY_APPLICATION_JS = "application/javascript";
    private static final String KEY_TEXT_XML = "text/xml";
    private static final String KEY_TEXT_HTML = "text/html";

    public static ContentTypeOption OTHER = new ContentTypeOption(KEY_OTHER, "Other ( Please Specify )");
    public static ContentTypeOption APPLICATION_XML = new ContentTypeOption(KEY_APPLICATION_XML, "application/xml");
    public static ContentTypeOption TEXT_XML = new ContentTypeOption(KEY_TEXT_XML, "text/xml");
    public static ContentTypeOption APPLICATION_SOAP_XML = new ContentTypeOption(KEY_APPLICATION_SOAP_XML, "application/soap+xml");
    public static ContentTypeOption APPLICATION_JSON = new ContentTypeOption(KEY_APPLICATION_JSON, "application/json");
    public static ContentTypeOption APPLICATION_JS = new ContentTypeOption(KEY_APPLICATION_JS, "application/javascript");
    public static ContentTypeOption TEXT_HTML = new ContentTypeOption(KEY_TEXT_HTML, "text/html");

    public ContentTypeOption() {}

    public ContentTypeOption(String code, String description) {
        super(code, description);
    }

    public static ContentTypeOption[] getOptions() {
        return new ContentTypeOption[] {
            APPLICATION_XML,
            TEXT_XML,
            APPLICATION_SOAP_XML,
            APPLICATION_JSON,
            APPLICATION_JS,
            TEXT_HTML
        };
    }

    public static ContentTypeOption fromValue(String contentType) {
        for (ContentTypeOption cto : getOptions()) {
            if (cto.getKey().equals(contentType)) {
                return cto;
            }
        }

        return null;
    }

    public boolean isOther() {
        return this.equals(ContentTypeOption.OTHER);
    }

}
