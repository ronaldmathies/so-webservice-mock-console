package nl.sodeso.webservice.mock.console.client.application.response;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionOption;

/**
 * @author Ronald Mathies
 */
public interface ResponsesFilterHandler {

    void filter(OptionType<DefaultOption> option);

}
