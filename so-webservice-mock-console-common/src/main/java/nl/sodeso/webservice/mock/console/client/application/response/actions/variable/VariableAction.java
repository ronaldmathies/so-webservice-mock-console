package nl.sodeso.webservice.mock.console.client.application.response.actions.variable;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.webservice.mock.console.client.application.response.actions.Action;
import nl.sodeso.webservice.mock.console.client.application.response.actions.ActionOption;

import static nl.sodeso.webservice.mock.console.client.application.Resources.VARIABLE_I18N;

/**
 * @author Ronald Mathies
 */
public class VariableAction extends Action {

    private static final String KEY_VARIABLE_ACTION = "variable-action";
    private static final String KEY_VARIABLE = "variable";

    private Variables variables = new Variables();

    private transient EntryWithSingleWidget entry = null;

    public VariableAction() {}

    public ActionOption getActionOption() {
        return ActionOption.VARIABLE;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_VARIABLE, VARIABLE_I18N.variableDocumentation()),
                new EntryWithContainer(KEY_VARIABLE, variables.toEditForm())
            );

        LegendPanel legendPanel = new LegendPanel(null, "Variables Action");
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_VARIABLE_ACTION, legendPanel);
        return entry;
    }


    public Variables getVariables() {
        return variables;
    }

    public void setVariables(Variables variables) {
        this.variables = variables;
    }
}
