package nl.sodeso.webservice.mock.console.client.application.download.steps;

import nl.sodeso.gwt.ui.client.form.table.summary.AbstractSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseSummaryItem;

/**
 * @author Ronald Mathies
 */
public class ExportableResponseSummaryItem extends AbstractSummaryItem {

    private boolean isSelected = false;

    private ResponseSummaryItem response = null;

    private String exportFilename;

    public ExportableResponseSummaryItem() {}

    public ExportableResponseSummaryItem(ResponseSummaryItem response) {
        this.response = response;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getExportFilename() {
        return exportFilename;
    }

    public void setExportFilename(String exportFilename) {
        this.exportFilename = exportFilename;
    }

    @Override
    public String getLabel() {
        return this.response.getResponseName();
    }

}
