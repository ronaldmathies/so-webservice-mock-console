package nl.sodeso.webservice.mock.console.client.application.response.conditions;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public interface ConditionChosenHandler {

    void ok(OptionType<ConditionOption> option);

}
