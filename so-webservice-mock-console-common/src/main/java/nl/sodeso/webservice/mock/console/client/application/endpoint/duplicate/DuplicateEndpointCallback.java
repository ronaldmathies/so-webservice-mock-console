package nl.sodeso.webservice.mock.console.client.application.endpoint.duplicate;

import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @author Ronald Mathies
 */
public interface DuplicateEndpointCallback {

    void onSuccess(StringType name, StringType path);

}
