package nl.sodeso.webservice.mock.console.client.application.response.functions;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
class FunctionComboboxField extends ComboboxField<FunctionOption> {

    FunctionComboboxField(OptionType<FunctionOption> optionValue) {
        super(optionValue);
    }
}
