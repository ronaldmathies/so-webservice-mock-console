package nl.sodeso.webservice.mock.console.client.application.response.actions.exception;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface ExceptionActionConstants extends Messages {

    @DefaultMessage("Exception Action")
    String exceptionAction();

    @DefaultMessage("Enter the number of seconds which will cause the response to be delayed with.")
    String exceptionDocumentation();


}