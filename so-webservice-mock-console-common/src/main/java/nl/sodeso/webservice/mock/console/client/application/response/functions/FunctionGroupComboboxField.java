package nl.sodeso.webservice.mock.console.client.application.response.functions;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.webservice.mock.console.client.application.response.functions.rpc.FunctionRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
class FunctionGroupComboboxField extends ComboboxField<FunctionGroupOption> {

    private static FunctionRpcGateway functionRpcGateway = GWT.create(FunctionRpcGateway.class);

    FunctionGroupComboboxField(OptionType<FunctionGroupOption> optionValue) {
        super(optionValue);

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                functionRpcGateway.groups(ServiceManager.instance().getService(), new DefaultAsyncCallback<ArrayList<FunctionGroupOption>>() {
                    @Override
                    public void success(ArrayList<FunctionGroupOption> result) {
                        addItems(result);
                    }
                });
            }
        });
    }
}
