package nl.sodeso.webservice.mock.console.client.application.service;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public interface ServiceChosenHandler {

    void ok(OptionType<ServiceOption> option);

}
