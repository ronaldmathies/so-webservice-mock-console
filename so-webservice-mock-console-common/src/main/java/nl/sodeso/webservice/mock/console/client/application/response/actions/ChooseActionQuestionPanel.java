package nl.sodeso.webservice.mock.console.client.application.response.actions;

import com.google.gwt.event.dom.client.ClickEvent;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.filters.OptionFilter;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

import static nl.sodeso.webservice.mock.console.client.application.Resources.ACTION_I18N;

/**
 * @author Ronald Mathies
 */
public class ChooseActionQuestionPanel extends PopupWindowPanel {

    private static final String KEY_CHOOSE_ACTION = "choose-action";
    private static final String KEY_ACTION = "action";

    private OptionType<ActionOption> actionOption = new OptionType<>();

    private ActionChosenHandler actionChosenHandler = null;
    private ActionComboboxField actionComboboxField = null;

    public ChooseActionQuestionPanel(final ActionChosenHandler actionChosenHandler, OptionFilter<ActionOption> filter) {
        super(KEY_CHOOSE_ACTION, ACTION_I18N.title(), Style.INFO);
        this.actionChosenHandler = actionChosenHandler;

        this.setWidth("650px");

        this.actionComboboxField = new ActionComboboxField(actionOption);
        this.actionComboboxField.setOptionFilter(filter);
        this.actionComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_ACTION, ValidationMessage.Level.ERROR, actionOption));

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithDocumentation(null, ACTION_I18N.actionDocumentation()));
        entryForm.addEntry(new EntryWithLabelAndWidget(null, ACTION_I18N.action(), actionComboboxField));
        addToBody(entryForm);

        addToFooter(Align.RIGHT,
            new OkButton.WithLabel(this::ok),
            new CancelButton.WithLabel((event) -> close()));
    }

    private void ok(ClickEvent event) {
        ValidationUtil.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                ChooseActionQuestionPanel.this.close();

                actionChosenHandler.ok(actionOption);
            }
        }, actionComboboxField);
    }

}
