package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class ActionComboboxField extends ComboboxField<ActionOption> {

    public ActionComboboxField(OptionType<ActionOption> optionValue) {
        super(optionValue);
        addItems(ActionOption.getOptions());
    }
}
