package nl.sodeso.webservice.mock.console.client.application.response.conditions.query;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Condition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionOption;

import static nl.sodeso.webservice.mock.console.client.application.Resources.*;

/**
 * @author Ronald Mathies
 */
public class QueryCondition extends Condition {

    private static final String KEY_QUERY_CONDITION = "query-condition";
    private static final String KEY_QUERY_VALUES = "query-values";

    private QueryValues queryValues = new QueryValues();

    private transient EntryWithSingleWidget entry = null;

    public QueryCondition() {
    }

    public ConditionOption getConditionOption() {
        return ConditionOption.QUERY;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_QUERY_CONDITION, QUERY_I18N.queryDocumentation()),
                new EntryWithContainer(KEY_QUERY_VALUES, queryValues.toEditForm())
            );

        LegendPanel legendPanel = new LegendPanel(null, QUERY_I18N.queryCondition());
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_QUERY_CONDITION, legendPanel);
        return entry;
    }

    public QueryValues getQueryValues() {
        return queryValues;
    }

    public void setQueryValues(QueryValues queryValues) {
        this.queryValues = queryValues;
    }
}