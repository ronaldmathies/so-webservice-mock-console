package nl.sodeso.webservice.mock.console.client.application.generic.header;

import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.webservice.mock.console.client.application.generic.HeaderComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.HeaderOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Header implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private static final String KEY_VALUE = "value";

    private String uuid = null;

    private OptionType<HeaderOption> headerOption = new OptionType<>();
    private StringType otherHeaderOption = new StringType();

    private StringType value = new StringType();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient HeaderComboboxField headerComboboxField = null;

    private transient TextField<StringType> valueField = null;

    public Header() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        headerComboboxField = new HeaderComboboxField(headerOption, otherHeaderOption);
        headerOption.addValueChangedEventHandler(new ValueChangedEventHandler<OptionType>(headerComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<OptionType> event) {
                value.setValue(headerOption.getValue().getExample());
            }
        });

        valueField = new TextField<>(KEY_VALUE, this.value)
            .setPlaceholder(KEY_VALUE);
        valueField.addValidationRule(new MandatoryValidationRule(KEY_VALUE, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return valueField.getValue();
            }
        });

        entry = new EntryWithWidgetAndWidget(null, headerComboboxField, new InsertFunctionButton(valueField));

        return entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<HeaderOption> getHeaderOption() {
        return headerOption;
    }

    public void setHeaderOption(OptionType<HeaderOption> headerOption) {
        this.headerOption = headerOption;
    }

    public StringType getOtherHeaderOption() {
        return otherHeaderOption;
    }

    public void setOtherHeaderOption(StringType otherHeaderOption) {
        this.otherHeaderOption = otherHeaderOption;
    }

    public StringType getValue() {
        return this.value;
    }

    public void setValue(StringType value) {
        this.value = value;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }


}
