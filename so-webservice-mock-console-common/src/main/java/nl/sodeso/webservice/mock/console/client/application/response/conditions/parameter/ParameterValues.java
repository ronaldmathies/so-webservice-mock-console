package nl.sodeso.webservice.mock.console.client.application.response.conditions.parameter;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionConfig;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class ParameterValues extends WidgetCollectionField<ParameterValue> {

    public ParameterValues() {
        super(WidgetCollectionConfig.DEFAULT);
    }

    @Override
    public WidgetSupplier<ParameterValue> createWidgetSupplier() {
        return callback -> callback.created(new ParameterValue());
    }
}
