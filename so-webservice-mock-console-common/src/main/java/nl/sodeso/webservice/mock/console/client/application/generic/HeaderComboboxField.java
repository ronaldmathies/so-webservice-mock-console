package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @author Ronald Mathies
 */
public class HeaderComboboxField extends ComboboxField<HeaderOption> {

    public HeaderComboboxField(OptionType<HeaderOption> optionValue, StringType otherHeader) {
        super(optionValue, otherHeader, HeaderOption.OTHER);
        addItems(HeaderOption.getOptions());
    }
}
