package nl.sodeso.webservice.mock.console.client.application.endpoint;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.StringUtils;
import nl.sodeso.webservice.mock.console.client.application.endpoint.rpc.EndpointRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;

/**
 * @author Ronald Mathies
 */
public abstract class PathUniqueValidationRule implements ValidationRule {

    private EndpointRpcGateway gateway = GWT.create(EndpointRpcGateway.class);

    /**
     * Constructs a new validation rule.
     */
    public PathUniqueValidationRule() {
    }

    /**
     * {@inheritDoc}
     */
    public void validate(final ValidationCompletedHandler handler) {
        if (getPath() != null && StringUtils.isNotEmpty(getPath().getValue())) {
            gateway.isPathUnique(ServiceManager.instance().getService(), getPath().getValue(), new DefaultAsyncCallback<ValidationResult>() {

                @Override
                public void success(ValidationResult result) {
                    handler.completed(result);
                }

                @Override
                public void failure(Throwable caught) {
                    super.failure(caught);
                }
            });
        } else {
            handler.completed(new ValidationResult());
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return getPath() != null && StringUtils.isNotEmpty(getPath().getValue());
    }

    public abstract StringType getPath();

}
