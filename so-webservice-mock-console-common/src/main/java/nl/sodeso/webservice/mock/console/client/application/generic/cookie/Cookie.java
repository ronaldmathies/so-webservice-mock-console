package nl.sodeso.webservice.mock.console.client.application.generic.cookie;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.webservice.mock.console.client.application.generic.ActionComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.ActionOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import java.io.Serializable;

import static nl.sodeso.webservice.mock.console.client.application.Resources.COOKIE_I18N;

/**
 * @author Ronald Mathies
 */
public class Cookie implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private static final String KEY_ACTION = "action";
    private static final String KEY_NAME = "name";
    private static final String KEY_VALUE = "value";
    private static final String KEY_DOMAIN = "domain";
    private static final String KEY_PATH = "path";
    private static final String KEY_VERSION = "version";
    private static final String KEY_MAX_AGE = "max-age";

    private String uuid = null;
    private OptionType<ActionOption> actionOption = new OptionType<>(ActionOption.CREATE);
    private StringType name = new StringType();
    private StringType value = new StringType();
    private StringType domain = new StringType();
    private StringType path = new StringType();
    private StringType version = new StringType();
    private StringType maxAge = new StringType();

    private transient EntryWithSingleWidget entry = null;
    private transient EntryForm form = null;

    private transient ActionComboboxField actionComboboxField = null;
    private transient TextField<StringType> nameField = null;
    private transient TextField<StringType> valueField = null;
    private transient TextField<StringType> domainField = null;
    private transient TextField<StringType> pathField = null;
    private transient TextField<StringType> versionField = null;
    private transient TextField<StringType> maxAgeField = null;

    public Cookie() {
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        actionComboboxField = new ActionComboboxField(actionOption);
        actionComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_ACTION, ValidationMessage.Level.ERROR, actionOption));

        nameField = new TextField<>(KEY_NAME, this.name);
        nameField.addValidationRule(new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return nameField.getValue();
            }
        });
        valueField = new TextField<>(KEY_VALUE, this.value);
        valueField.addValidationRule(new MandatoryValidationRule(KEY_VALUE, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return valueField.getValue();
            }
        });
        domainField = new TextField<>(KEY_DOMAIN, this.domain);
        pathField = new TextField<>(KEY_PATH, this.path);
        versionField = new TextField<>(KEY_VERSION, this.version);
        maxAgeField = new TextField<>(KEY_MAX_AGE, this.maxAge);

        form = new EntryForm("permissions")
            .addEntries(
                new EntryWithDocumentation(KEY_ACTION, COOKIE_I18N.actionDocumentation()),
                new EntryWithLabelAndWidget(KEY_ACTION, COOKIE_I18N.action(), actionComboboxField),
                new EntryWithLabelAndWidget(KEY_NAME, COOKIE_I18N.name(), nameField),
                new EntryWithLabelAndWidget(KEY_VALUE, COOKIE_I18N.value(), new InsertFunctionButton(valueField)),
                new EntryWithDocumentation(KEY_DOMAIN, COOKIE_I18N.domainDocumentation()),
                new EntryWithLabelAndWidget(KEY_DOMAIN, COOKIE_I18N.domain(), domainField),
//                new EntryWithDocumentation(KEY_PATH, COOKIE_I18N.pathDocumentation()),
                new EntryWithLabelAndWidget(KEY_PATH, COOKIE_I18N.path(), pathField),
//                new EntryWithDocumentation(KEY_VERSION, COOKIE_I18N.versionDocumentation()),
//                new EntryWithLabelAndWidget(KEY_VERSION, COOKIE_I18N.version(), versionField),
                new EntryWithDocumentation(KEY_MAX_AGE, COOKIE_I18N.maxAgeDocumentation()),
                new EntryWithLabelAndWidget(KEY_MAX_AGE, COOKIE_I18N.maxAge(), maxAgeField).setHint(COOKIE_I18N.maxAgeHint())
            );

        entry = new EntryWithSingleWidget(null, form);

        actionOption.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(actionComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                boolean isCreate = actionOption.getValue().equals(ActionOption.CREATE);
                form.setEntriesVisible(isCreate, KEY_VALUE);
                form.setEntriesVisible(isCreate, KEY_DOMAIN);
                form.setEntriesVisible(isCreate, KEY_PATH);
                form.setEntriesVisible(isCreate, KEY_VERSION);
                form.setEntriesVisible(isCreate, KEY_MAX_AGE);
            }
        });

        return entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<ActionOption> getActionOption() {
        return actionOption;
    }

    public void setActionOption(OptionType<ActionOption> actionOption) {
        this.actionOption = actionOption;
    }

    public StringType getName() {
        return this.name;
    }

    public void setName(StringType name) {
        this.name = name;
    }

    public StringType getValue() {
        return this.value;
    }

    public void setValue(StringType value) {
        this.value = value;
    }

    public StringType getDomain() {
        return domain;
    }

    public void setDomain(StringType domain) {
        this.domain = domain;
    }

    public StringType getPath() {
        return path;
    }

    public void setPath(StringType path) {
        this.path = path;
    }

    public StringType getVersion() {
        return version;
    }

    public void setVersion(StringType version) {
        this.version = version;
    }

    public StringType getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(StringType maxAge) {
        this.maxAge = maxAge;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, form);
    }


}
