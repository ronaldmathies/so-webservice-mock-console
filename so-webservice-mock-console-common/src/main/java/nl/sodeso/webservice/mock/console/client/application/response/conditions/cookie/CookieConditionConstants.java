package nl.sodeso.webservice.mock.console.client.application.response.conditions.cookie;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface CookieConditionConstants extends Messages {

    @DefaultMessage("The cookie condition is used to limit this response for Requests by testing if there is a cookie with the same name as the name specified, and if the value of the cookie matches the expected result. It is possible to use expressions on the value of the cookie to filter out a specific part of the value which will then be compared to the expected matching result.")
    String cookieConditionDocumentation();

    @DefaultMessage("Cookie Condition")
    String cookieCondition();

    @DefaultMessage("Enter the name of the cookie, header or parameter which will be used as the basis for the storage id.")
    String nameDocumentation();

    @DefaultMessage("Name")
    String name();

    @DefaultMessage("Select the type of expression to use to resolve the value which will be used to store the request with.")
    String expressionTypeDocumentation();

    @DefaultMessage("Expression type")
    String expressionType();

    @DefaultMessage("Enter an expression (JSON Path, Regulair Expression or XPath) that will resolve the value which will be used as the storage id.")
    String expressionDocumentation();

    @DefaultMessage("Expression")
    String expression();

    @DefaultMessage("Enter a value that should match with the result of the expression.")
    String matchesDocumentation();

    @DefaultMessage("Matches")
    String matches();

}