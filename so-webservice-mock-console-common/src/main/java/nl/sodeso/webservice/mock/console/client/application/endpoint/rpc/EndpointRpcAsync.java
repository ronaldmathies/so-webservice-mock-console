package nl.sodeso.webservice.mock.console.client.application.endpoint.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.webservice.mock.console.client.application.endpoint.Endpoint;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface EndpointRpcAsync {

    void findSummaries(ServiceOption serviceOption, AsyncCallback<ArrayList<EndpointSummaryItem>> result);
    void getDetails(ServiceOption serviceOption, String name, AsyncCallback<Endpoint> result);

    void isNameUnique(ServiceOption serviceOption, String name, AsyncCallback<ValidationResult> result);
    void isPathUnique(ServiceOption serviceOption, String path, AsyncCallback<ValidationResult> result);

    void save(ServiceOption serviceOption, Endpoint endpoint, AsyncCallback<EndpointSummaryItem> result);
    void duplicate(ServiceOption serviceOption, String name, String newName, String newPath, AsyncCallback<EndpointSummaryItem> result);

    void asOptions(ServiceOption serviceOption, AsyncCallback<ArrayList<DefaultOption>> result);

    void delete(ServiceOption serviceOption, String name, AsyncCallback<Void> result);

}
