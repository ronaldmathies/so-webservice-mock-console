package nl.sodeso.webservice.mock.console.client.application.tooling.delete;

/**
 * @author Ronald Mathies
 */
public interface DeleteCallback {

    public void onSuccess();

}
