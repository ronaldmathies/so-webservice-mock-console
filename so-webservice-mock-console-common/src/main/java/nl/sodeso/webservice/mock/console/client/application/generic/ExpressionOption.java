package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class ExpressionOption extends DefaultOption {

    private static final String KEY_NONE = "none";
    private static final String KEY_JSONPATH = "jsonpath";
    private static final String KEY_REGEXP = "regexp";
    private static final String KEY_XPATH = "xpath";

    public static ExpressionOption NONE = new ExpressionOption(KEY_NONE, "None");
    public static ExpressionOption JSON_PATH = new ExpressionOption(KEY_JSONPATH, "JSON Path");
    public static ExpressionOption REGEXP = new ExpressionOption(KEY_REGEXP, "Regulair Expression");
    public static ExpressionOption XPATH = new ExpressionOption(KEY_XPATH, "XPath");

    public ExpressionOption() {}

    public ExpressionOption(String code, String description) {
        super(code, description);
    }

    public static ExpressionOption[] getOptions(boolean allowNone) {
        if (allowNone) {
            return new ExpressionOption[] {
                    NONE, JSON_PATH, REGEXP, XPATH
            };
        }

        return new ExpressionOption[] {
            JSON_PATH, REGEXP, XPATH
        };
    }

    public static ExpressionOption getOption(String code) {
        for (ExpressionOption expressionOption : getOptions(true)) {
            if (expressionOption.getKey().equals(code)) {
                return expressionOption;
            }
        }

        return null;
    }

    public boolean isNone() {
        return this.equals(NONE);
    }

    public boolean isJsonPath() {
        return this.equals(JSON_PATH);
    }

    public boolean isRegExp() {
        return this.equals(REGEXP);
    }

    public boolean isXPath() {
        return this.equals(XPATH);
    }
}
