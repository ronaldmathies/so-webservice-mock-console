package nl.sodeso.webservice.mock.console.client.application.endpoint;

import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.webservice.mock.console.client.application.ui.menu.HistoryHelper;

import static nl.sodeso.webservice.mock.console.client.application.Resources.ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class EndpointMenuItem extends MenuItem {

    private static final String KEY = "mi-endpoint";

    private static Endpoints endpoints = null;

    public EndpointMenuItem() {
        super(KEY, Icon.Server, ENDPOINT_I18N.endpointMenuItem(), arguments -> {
            if (endpoints == null) {
                endpoints = new Endpoints();
            }
            endpoints.setArguments(arguments);
            CenterController.instance().setWidget(endpoints);
        });
    }

    public static void click(String uuid) {
        HistoryHelper.click(KEY, uuid);
    }

}
