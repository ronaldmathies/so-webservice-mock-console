package nl.sodeso.webservice.mock.console.client.application.generic.header;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionConfig;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class Headers extends WidgetCollectionField<Header> {

    public Headers() {
        super(WidgetCollectionConfig.DEFAULT);
    }

    public WidgetSupplier<Header> createWidgetSupplier() {
        return callback -> callback.created(new Header());
    }
}
