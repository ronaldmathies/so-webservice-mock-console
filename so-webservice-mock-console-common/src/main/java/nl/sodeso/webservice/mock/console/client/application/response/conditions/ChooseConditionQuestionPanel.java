package nl.sodeso.webservice.mock.console.client.application.response.conditions;

import com.google.gwt.event.dom.client.ClickEvent;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.button.YesButton;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.filters.OptionFilter;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

import static nl.sodeso.webservice.mock.console.client.application.Resources.CONDITION_I18N;

/**
 * @author Ronald Mathies
 */
public class ChooseConditionQuestionPanel extends PopupWindowPanel {

    private static final String KEY_CHOOSE_CONDITION = "choose-condition";
    private static final String KEY_CONDITION = "condition";

    private OptionType<ConditionOption> conditionOption = new OptionType<>();

    private ConditionChosenHandler conditionChosenHandler = null;
    private ConditionComboboxField conditionComboboxField = null;

    public ChooseConditionQuestionPanel(final ConditionChosenHandler conditionChosenHandler, OptionFilter<ConditionOption> filter) {
        super(KEY_CHOOSE_CONDITION, CONDITION_I18N.title(), WindowPanel.Style.INFO);
        this.conditionChosenHandler = conditionChosenHandler;

        this.setWidth("650px");

        this.conditionComboboxField = new ConditionComboboxField(conditionOption);
        this.conditionComboboxField.setOptionFilter(filter);
        this.conditionComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_CONDITION, ValidationMessage.Level.ERROR, conditionOption));

        EntryForm entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_CONDITION, CONDITION_I18N.conditionDocumentation()),
                new EntryWithLabelAndWidget(KEY_CONDITION, CONDITION_I18N.condition(), conditionComboboxField));

        addToBody(entryForm);

        addToFooter(Align.RIGHT,
            new OkButton.WithLabel(this::ok),
            new CancelButton.WithLabel((event) -> close()));
    }

    private void ok(ClickEvent event) {
        ValidationUtil.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                ChooseConditionQuestionPanel.this.close();

                conditionChosenHandler.ok(conditionOption);
            }
        }, conditionComboboxField);
    }

}
