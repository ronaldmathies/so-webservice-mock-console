package nl.sodeso.webservice.mock.console.client.application.response.functions.model;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Parameter implements Serializable {

    public String name;
    public String type;

    public Parameter() {}

    public Parameter(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
