package nl.sodeso.webservice.mock.console.client.application.response.functions;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.types.ValueType;

/**
 * @author Ronald Mathies
 */
public class InsertFunctionButton extends FlowPanel {

    // Keys for so-key entries.
    private static final String KEY_ZOOM_SUFFIX = "-add-function";

    public InsertFunctionButton(TextField<? extends ValueType> field) {
        this.add(field);

        this.getElement().getStyle().setPosition(Style.Position.RELATIVE);

        SimpleButton functionButton = new SimpleButton(field.getKey() != null ? field.getKey() + KEY_ZOOM_SUFFIX : null, Icon.Filter, SimpleButton.Style.NONE);
        functionButton.addClickHandler((event) -> {
            int cursorPosition = field.getCursorPos();
            String selectedText = field.getSelectedText();
            new ChooseFunctionQuestionPanel(function -> {
                String currentValue = field.getValue();

                if (selectedText != null) {
                    int fromPos = currentValue.indexOf(selectedText);
                    String newValue = currentValue.substring(0, fromPos) + function + currentValue.substring(fromPos + selectedText.length());
                    field.setValue(newValue, true);
                } else {
                    field.setValue(currentValue.substring(0, cursorPosition) + function + currentValue.substring(cursorPosition), true);
                }
            }).center(false, true);
        });

        functionButton.getElement().getStyle().setPosition(Style.Position.ABSOLUTE);
        functionButton.getElement().getStyle().setTop(0, Style.Unit.PX);
        functionButton.getElement().getStyle().setRight(0, Style.Unit.PX);
        add(functionButton);
    }

}
