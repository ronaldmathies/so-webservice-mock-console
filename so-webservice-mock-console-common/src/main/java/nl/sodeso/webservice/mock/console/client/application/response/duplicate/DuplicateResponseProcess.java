package nl.sodeso.webservice.mock.console.client.application.response.duplicate;

import nl.sodeso.gwt.ui.client.dialog.InfoQuestionPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.response.NameUniqueValidationRule;

import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class DuplicateResponseProcess {

    private static final String KEY_DUPLICATE = "duplicate";
    private static final String KEY_NAME = "name";

    private StringType name = new StringType();
    private TextField<StringType> nameTextField = null;
    private EntryForm entryForm = null;

    private InfoQuestionPanel questionPanel = null;

    private DuplicateResponseCallback callback;
    private EndpointSummaryItem endpoint;

    public DuplicateResponseProcess(DuplicateResponseCallback callback, EndpointSummaryItem endpoint) {
        this.callback = callback;
        this.endpoint = endpoint;
    }

    public void start() {
        duplicate();
    }

    private void duplicate() {
        this.questionPanel = new InfoQuestionPanel(RESPONSE_I18N.duplicateTitle(), RESPONSE_I18N.duplicateQuestion(), () -> perform());
        this.questionPanel.setWidth("520px");
        this.nameTextField = new TextField<>(KEY_NAME, this.name)
            .addValidationRules(
                new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return name.getValue();
                    }
                },
                new NameUniqueValidationRule() {

                    @Override
                    public String getResponseName() {
                        return name.getValue();
                    }

                    @Override
                    public String getEndpointName() {
                        return endpoint.getLabel();
                    }
                }
            );
        this.nameTextField.setFocus();

        this.entryForm = new EntryForm(KEY_DUPLICATE)
            .addEntry(new EntryWithLabelAndWidget(KEY_NAME, RESPONSE_I18N.name(), nameTextField));

        this.questionPanel.addToBody(entryForm);
        this.questionPanel.center(true, true);
    }

    private void perform() {
        ValidationUtil.validate(result -> {
            if (!ValidationUtil.findWorstLevel(result).isError()) {
                questionPanel.close();
                callback.onSuccess(this.name);
            }
        }, entryForm);

    }

}
