package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class ActionOption extends DefaultOption {

    private static final String KEY_CREATE = "create";
    private static final String KEY_DELETE = "delete";

    public static ActionOption CREATE = new ActionOption(KEY_CREATE, "Create");
    public static ActionOption DELETE = new ActionOption(KEY_DELETE, "Delete");

    public ActionOption() {}

    public ActionOption(String code, String description) {
        super(code, description);
    }

    public static ActionOption[] getOptions() {
        return new ActionOption[] {
                CREATE, DELETE
        };
    }

    public static ActionOption getOption(String key) {
        switch (key) {
            case KEY_CREATE:
                return CREATE;
            case KEY_DELETE:
                return DELETE;
        }

        return null;
    }

    public boolean isCreate() {
        return this.getCode().equals(KEY_CREATE);
    }

    public boolean isDelete() {
        return this.getCode().equals(KEY_DELETE);
    }

}
