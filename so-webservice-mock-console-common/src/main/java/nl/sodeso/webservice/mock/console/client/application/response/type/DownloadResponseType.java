package nl.sodeso.webservice.mock.console.client.application.response.type;

import com.google.gwt.user.client.Window;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.TextAreaField;
import nl.sodeso.gwt.ui.client.form.input.TextAreaToolsField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadField;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadForm;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadType;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.generic.ContentTypeComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.ContentTypeOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class DownloadResponseType extends ResponseType {

    private static final String KEY_HTTP_CONTENTTYPE = "content-type";
    private static final String KEY_FILENAME = "filename";
    private static final String KEY_FILE = "file";

    private OptionType<ContentTypeOption> contentTypeOption = new OptionType<>(ContentTypeOption.APPLICATION_XML);
    private StringType otherContentTypeOption = new StringType();
    private StringType filename = new StringType();

    private FileUploadType fileUpload = new FileUploadType();

    private transient FileUploadForm fileUploadForm = null;
    private transient FileUploadField fileUploadField = null;

    private transient EntryForm form = null;
    private transient ContentTypeComboboxField contentTypeComboboxField = null;
    private transient TextField<StringType> filenameField = null;

    public DownloadResponseType() {}

    public EntryForm toForm() {
        if (form != null) {
            return form;
        }

        contentTypeComboboxField = new ContentTypeComboboxField(contentTypeOption, otherContentTypeOption);

        filenameField = new TextField<>(KEY_FILENAME, filename);

        this.fileUploadField = new FileUploadField(KEY_FILE, fileUpload);
        this.fileUploadField.addValidationRule(new MandatoryValidationRule(KEY_FILE, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return fileUploadField.getValue();
            }

        });
        this.fileUploadForm = new FileUploadForm(fileUploadField, "/file.upload");
        form = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_HTTP_CONTENTTYPE, RESPONSE_I18N.contentType(), contentTypeComboboxField),
                new EntryWithLabelAndWidget(KEY_FILENAME, RESPONSE_I18N.filename(), new InsertFunctionButton(filenameField)),
                new EntryWithLabelAndWidget(KEY_FILE, RESPONSE_I18N.file(), this.fileUploadForm)
            );

        return form;
    }

    public void preSaveAction(Trigger trigger) {
        this.fileUploadForm.setFileUploadCompleteTrigger(trigger);

        if (getFileUpload().isChanged()) {
            this.fileUploadForm.submit();
        } else {
            trigger.fire();
        }
    }

    public StringType getFilename() {
        return this.filename;
    }

    public void setFilename(StringType filename) {
        this.filename = filename;
    }


    public OptionType<ContentTypeOption> getContentTypeOption() {
        return contentTypeOption;
    }

    public void setContentTypeOption(OptionType<ContentTypeOption> contentTypeOption) {
        this.contentTypeOption = contentTypeOption;
    }

    public StringType getOtherContentTypeOption() {
        return otherContentTypeOption;
    }

    public void setOtherContentTypeOption(StringType otherContentTypeOption) {
        this.otherContentTypeOption = otherContentTypeOption;
    }

    public void setFileUpload(FileUploadType fileUpload) {
        this.fileUpload = fileUpload;
    }

    public FileUploadType getFileUpload() {
        return this.fileUpload;
    }

}
