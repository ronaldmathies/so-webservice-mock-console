package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class HttpStatusComboboxField extends ComboboxField<HttpStatusOption> {

    public HttpStatusComboboxField(OptionType<HttpStatusOption> optionValue) {
        super(optionValue);
        addItems(HttpStatusOption.getOptions());
    }
}
