package nl.sodeso.webservice.mock.console.client.application.response.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class ResponseRpcGateway implements RpcGateway<ResponseRpcAsync>, ResponseRpcAsync {

    /**
     * {@inheritDoc}
     */
    public ResponseRpcAsync getRpcAsync() {
        return GWT.create(ResponseRpc.class);
    }
}
