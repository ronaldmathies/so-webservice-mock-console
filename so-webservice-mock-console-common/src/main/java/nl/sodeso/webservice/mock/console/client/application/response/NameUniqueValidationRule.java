package nl.sodeso.webservice.mock.console.client.application.response;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.StringUtils;
import nl.sodeso.webservice.mock.console.client.application.response.rpc.ResponseRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;

/**
 * @author Ronald Mathies
 */
public abstract class NameUniqueValidationRule implements ValidationRule {

    private ResponseRpcGateway gateway = GWT.create(ResponseRpcGateway.class);

    /**
     * Constructs a new validation rule.
     */
    public NameUniqueValidationRule() {
    }

    /**
     * {@inheritDoc}
     */
    public void validate(final ValidationCompletedHandler handler) {
        if (getResponseName() != null && StringUtils.isNotEmpty(getResponseName())) {
            gateway.isNameUnique(ServiceManager.instance().getService(), getEndpointName(), getResponseName(), new DefaultAsyncCallback<ValidationResult>() {
                @Override
                public void success(ValidationResult result) {
                    handler.completed(result);
                }

                @Override
                public void failure(Throwable caught) {
                    super.failure(caught);
                }
            });
        } else {
            handler.completed(new ValidationResult());
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return getResponseName() != null && StringUtils.isNotEmpty(getResponseName());
    }

    public abstract String getResponseName();

    public abstract String getEndpointName();

}
