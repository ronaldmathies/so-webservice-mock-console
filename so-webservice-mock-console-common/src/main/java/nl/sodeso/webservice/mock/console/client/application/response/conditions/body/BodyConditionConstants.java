package nl.sodeso.webservice.mock.console.client.application.response.conditions.body;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface BodyConditionConstants extends Messages {

    @DefaultMessage("Body Condition")
    String bodyCondition();

    @DefaultMessage("The body condition is used to limit this response for Requests by testing the body of the request using an expression for a matching result. When the result of the expression matches the expected result a response is created with the information specified in this response.")
    String bodyConditionDocumentation();

    @DefaultMessage("Select the type of expression to use to resolve the value which will be used to store the request with.")
    String expressionTypeDocumentation();

    @DefaultMessage("Expression type")
    String expressionType();

    @DefaultMessage("Enter an expression (JSON Path, Regulair Expression or XPath) that will resolve the value which will be used as the storage id.")
    String expressionDocumentation();

    @DefaultMessage("Expression")
    String expression();

    @DefaultMessage("Enter a value that should match with the result of the expression.")
    String matchesDocumentation();

    @DefaultMessage("Matches")
    String matches();
}