package nl.sodeso.webservice.mock.console.client.application.response.type;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.IntegerRangeValidationRule;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class ProxyResponseType extends ResponseType {

    private static final String KEY_PROXY_URL = "proxy-url";
    private static final String KEY_IGNORE_HEADERS = "ignore-headers";
    private static final String KEY_IGNORE_COOKIES = "ignore-cookies";
    private static final String KEY_CONNECTION_TIMEOUT_IN_SECONDS = "connection-timeout-in-seconds";
    private static final String KEY_READ_TIMEOUT_IN_SECONDS = "read-timeout-in-seconds";

    private StringType domain = new StringType();

    private BooleanType ignoreHeaders = new BooleanType(false);
    private BooleanType ignoreCookies = new BooleanType(false);

    private IntType connectionTimeoutInSeconds = new IntType(10);
    private IntType readTimeoutInSeconds = new IntType(10);

    private transient EntryForm form = null;
    private transient RadioButtonGroupField ignoreHeadersGroupField = null;
    private transient RadioButtonGroupField ignoreCookiesGroupField = null;

    private transient TextField connectionTimeoutInSecondsField = null;
    private transient TextField readTimeoutInSecondsField = null;
    private transient TextField<StringType> domainField = null;

    public ProxyResponseType() {}

    public EntryForm toForm() {
        if (form != null) {
            return form;
        }

        domainField = new TextField<>(KEY_PROXY_URL, domain);

        ignoreHeadersGroupField = new RadioButtonGroupField(KEY_IGNORE_HEADERS, this.ignoreHeaders);
        ignoreHeadersGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        ignoreHeadersGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        ignoreHeadersGroupField.setAlignment(Align.HORIZONTAL);

        ignoreCookiesGroupField = new RadioButtonGroupField(KEY_IGNORE_COOKIES, this.ignoreHeaders);
        ignoreCookiesGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        ignoreCookiesGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        ignoreCookiesGroupField.setAlignment(Align.HORIZONTAL);

        connectionTimeoutInSecondsField = new TextField<>(KEY_CONNECTION_TIMEOUT_IN_SECONDS, this.connectionTimeoutInSeconds)
            .addValidationRules(
                new IntegerRangeValidationRule(KEY_CONNECTION_TIMEOUT_IN_SECONDS, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return connectionTimeoutInSecondsField.getValue();
                    }
                }
                .setMinimum(5)
            );

        readTimeoutInSecondsField = new TextField<>(KEY_READ_TIMEOUT_IN_SECONDS, this.readTimeoutInSeconds)
            .addValidationRules(
                new IntegerRangeValidationRule(KEY_READ_TIMEOUT_IN_SECONDS, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return readTimeoutInSecondsField.getValue();
                    }
                }
                .setMinimum(5)
            );

        form = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_PROXY_URL, RESPONSE_I18N.domain(), domainField),
                new EntryWithDocumentation(KEY_IGNORE_HEADERS, RESPONSE_I18N.ignoreHeadersDocumentation()),
                new EntryWithLabelAndWidget(KEY_IGNORE_HEADERS, RESPONSE_I18N.ignoreHeaders(), ignoreHeadersGroupField),
                new EntryWithDocumentation(KEY_IGNORE_HEADERS, RESPONSE_I18N.ignoreCookiesDocumentation()),
                new EntryWithLabelAndWidget(KEY_IGNORE_COOKIES, RESPONSE_I18N.ignoreCookies(), ignoreCookiesGroupField),
                new EntryWithLabelAndWidget(KEY_CONNECTION_TIMEOUT_IN_SECONDS, RESPONSE_I18N.connectionTimeout(), connectionTimeoutInSecondsField).setHint(RESPONSE_I18N.connectionTimeoutHint()),
                new EntryWithLabelAndWidget(KEY_READ_TIMEOUT_IN_SECONDS, RESPONSE_I18N.readTimeout(), readTimeoutInSecondsField).setHint(RESPONSE_I18N.readTimeoutHint()));

        return form;
    }

    public StringType getDomain() {
        return this.domain;
    }

    public void setDomain(StringType domain) {
        this.domain = domain;
    }

    public BooleanType getIgnoreHeaders() {
        return ignoreHeaders;
    }

    public void setIgnoreHeaders(BooleanType ignoreHeaders) {
        this.ignoreHeaders = ignoreHeaders;
    }

    public BooleanType getIgnoreCookies() {
        return ignoreCookies;
    }

    public void setIgnoreCookies(BooleanType ignoreCookies) {
        this.ignoreCookies = ignoreCookies;
    }

    public IntType getConnectionTimeoutInSeconds() {
        return connectionTimeoutInSeconds;
    }

    public void setConnectionTimeoutInSeconds(IntType connectionTimeoutInSeconds) {
        this.connectionTimeoutInSeconds = connectionTimeoutInSeconds;
    }

    public IntType getReadTimeoutInSeconds() {
        return readTimeoutInSeconds;
    }

    public void setReadTimeoutInSeconds(IntType readTimeoutInSeconds) {
        this.readTimeoutInSeconds = readTimeoutInSeconds;
    }
}
