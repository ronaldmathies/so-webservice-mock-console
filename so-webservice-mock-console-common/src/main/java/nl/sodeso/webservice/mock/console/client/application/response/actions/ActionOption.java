package nl.sodeso.webservice.mock.console.client.application.response.actions;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class ActionOption extends DefaultOption {

    private static final String KEY_VARIABLE = "variable";
    private static final String KEY_TIMEOUT = "timeout";
    private static final String KEY_EXCEPTION = "exception";

    public static ActionOption VARIABLE = new ActionOption(KEY_VARIABLE, "Variable");
    public static ActionOption TIMEOUT = new ActionOption(KEY_TIMEOUT, "Timeout");
    public static ActionOption EXCEPTION = new ActionOption(KEY_EXCEPTION, "Exception");

    public ActionOption() {
    }

    public ActionOption(String code, String description) {
        super(code, description);
    }

    public static ActionOption[] getOptions() {
        return new ActionOption[] {
            VARIABLE, TIMEOUT, EXCEPTION
        };
    }

    public static ActionOption getOption(String key) {
        switch (key) {
            case KEY_VARIABLE:
                return VARIABLE;
            case KEY_TIMEOUT:
                return TIMEOUT;
            case KEY_EXCEPTION:
                return EXCEPTION;
        }

        return null;
    }



}
