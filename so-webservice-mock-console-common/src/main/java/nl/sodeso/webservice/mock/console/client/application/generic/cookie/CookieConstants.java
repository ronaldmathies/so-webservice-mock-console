package nl.sodeso.webservice.mock.console.client.application.generic.cookie;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface CookieConstants extends Messages {

    @DefaultMessage("Choose the action that you would like to perform, creating a cookie will as the name suggests create a new cookie serverside and send it back to the client. Deleting a cookie will remove a cookie that was send along the request, this will also send information back with the response so that the client will also delete the cookie.")
    String actionDocumentation();

    @DefaultMessage("Action")
    String action();

    @DefaultMessage("Name")
    String name();

    @DefaultMessage("Value")
    String value();

    @DefaultMessage("The Domain and Path attributes define the scope of the cookie. They essentially tell the browser what website the cookie belongs to. For example, a domain with 'sodeso.nl' would allow the cookie to be used by everthing in 'sodeso.nl'. Adding an additional path would further limit the scope.")
    String domainDocumentation();

    @DefaultMessage("Domain")
    String domain();

    @DefaultMessage("Path")
    String path();

    @DefaultMessage("Version")
    String versionDocumentation();

    @DefaultMessage("Version")
    String version();

    @DefaultMessage("The maximum age can be used to set the cookie’s expiration as an interval of seconds in the future, relative to the time the browser received the cookie.")
    String maxAgeDocumentation();

    @DefaultMessage("Seconds")
    String maxAgeHint();

    @DefaultMessage("Maximum Age")
    String maxAge();

}