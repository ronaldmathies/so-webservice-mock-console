package nl.sodeso.webservice.mock.console.client.application.response;

import com.google.gwt.user.client.Window;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.webservice.mock.console.client.application.ui.menu.HistoryHelper;

import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;

/**
 * @author Ronald Mathies
 */
public class ResponseMenuItem extends MenuItem {

    private static final String KEY = "mi-response";

    private static Responses responses = null;

    public ResponseMenuItem() {
        super(KEY, Icon.Reply, RESPONSE_I18N.responseMenuItem(), arguments -> {
            if (responses == null) {
                responses = new Responses();
            }
            responses.setArguments(arguments);
            CenterController.instance().setWidget(responses);
        });
    }

    public static void click(String uuid) {
        HistoryHelper.click(KEY, uuid);
    }

}
