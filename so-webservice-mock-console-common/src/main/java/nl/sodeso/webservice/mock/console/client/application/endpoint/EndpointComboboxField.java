package nl.sodeso.webservice.mock.console.client.application.endpoint;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.webservice.mock.console.client.application.endpoint.rpc.EndpointRpcGateway;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class EndpointComboboxField extends ComboboxField<DefaultOption> {

    public EndpointComboboxField(OptionType<DefaultOption> optionValue, final boolean allowAll) {
        super(optionValue);

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                EndpointRpcGateway gateway = GWT.create(EndpointRpcGateway.class);
                gateway.asOptions(ServiceManager.instance().getService(), new DefaultAsyncCallback<ArrayList<DefaultOption>>() {
                    @Override
                    public void success(ArrayList<DefaultOption> result) {
                        if (allowAll) {
                            result.add(0, new DefaultOption("all", "All"));
                        }

                        EndpointComboboxField.this.replaceItemsRetainSelection(result);
                    }
                });
            }
        });
    }
}
