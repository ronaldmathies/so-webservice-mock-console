package nl.sodeso.webservice.mock.console.client.application.upload.endpoint.steps;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.dialog.InfoQuestionHandler;
import nl.sodeso.gwt.ui.client.dialog.InfoQuestionPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
import nl.sodeso.webservice.mock.console.client.application.endpoint.NameUniqueValidationRule;
import nl.sodeso.webservice.mock.console.client.application.endpoint.PathUniqueValidationRule;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadEndpointWizardContext;
import nl.sodeso.webservice.mock.console.client.application.upload.rpc.UploadRpcGateway;

import static nl.sodeso.webservice.mock.console.client.application.Resources.ENDPOINT_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.UPLOAD_ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class ChooseEndpointNameStep extends AbstractWizardStep<UploadEndpointWizardContext> {

    private static final String KEY_NAME = "name";
    private static final String KEY_PATH = "path";
    private static final String KEY_STEP = "step-";

    private StringType name = new StringType("");
    private StringType path = new StringType("");

    private LegendPanel legendPanel = null;
    private EntryForm entryForm = null;
    private TextField nameTextField = null;
    private TextField pathTextField = null;

    private InfoQuestionPanel infoQuestionPanel = null;

    private AbstractWizardStep<UploadEndpointWizardContext> uploadEndpointFinishedStep = null;

    private UploadRpcGateway gateway = GWT.create(UploadRpcGateway.class);

    public ChooseEndpointNameStep(AbstractWizardStep<UploadEndpointWizardContext> callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep<UploadEndpointWizardContext> getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep<UploadEndpointWizardContext> getNextStep() {
        if (this.uploadEndpointFinishedStep == null) {
            this.uploadEndpointFinishedStep = new EndpointUploadFinishedStep(this);
        }

        return this.uploadEndpointFinishedStep;
    }

    @Override
    public Widget getBodyWidget() {
        if (this.legendPanel != null) {
            return this.legendPanel;
        }

        this.name.setValue(getWizardContext().getUploadedEndpointDetails().getName());
        this.path.setValue(getWizardContext().getUploadedEndpointDetails().getPath());

        nameTextField = new TextField<>(KEY_NAME, this.name)
            .addValidationRules(
                new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return nameTextField.getValue();
                    }
                },
                new NameUniqueValidationRule() {
                    @Override
                    public StringType getName() {
                        return name;
                    }
                }
            );

        this.pathTextField = new TextField<>(KEY_PATH, this.path)
            .addValidationRules(
                new MandatoryValidationRule(KEY_PATH, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return pathTextField.getValue();
                    }
                },
                new PathUniqueValidationRule() {
                    @Override
                    public StringType getPath() {
                        return path;
                    }
                }
            );

        this.entryForm = new EntryForm(null);
        this.entryForm.addEntries(
                new EntryWithDocumentation(KEY_NAME, UPLOAD_ENDPOINT_I18N.nameDocumentation()),
                new EntryWithLabelAndWidget(KEY_NAME, ENDPOINT_I18N.name(), this.nameTextField),
                new EntryWithDocumentation(KEY_NAME, UPLOAD_ENDPOINT_I18N.pathDocumentation()),
                new EntryWithLabelAndWidget(KEY_NAME, ENDPOINT_I18N.path(), this.pathTextField)
        );

        this.legendPanel = new LegendPanel(KEY_STEP + getStepIndex(), UPLOAD_ENDPOINT_I18N.endpointNameTitle(getStepIndex()));
        this.legendPanel.add(this.entryForm);

        this.nameTextField.setFocus();

        return this.legendPanel;
    }

    @Override
    public void onBeforePrevious(Trigger trigger) {
        trigger.fire();
    }

    @Override
    public void onBeforeNext(Trigger trigger) {
        ValidationUtil.validate(result -> {
            if (!ValidationUtil.findWorstLevel(result).isError()) {
                submit(trigger);
            }
        }, entryForm);
    }

    private void submit(Trigger trigger) {
        getWizardContext().setLabel(name.getValue());
        getWizardContext().setPath(path.getValue());

        infoQuestionPanel = new InfoQuestionPanel(UPLOAD_ENDPOINT_I18N.finalizeImportTitle(), UPLOAD_ENDPOINT_I18N.finalizeImportQuestion(),
            () -> finishImport(trigger)
        );
        infoQuestionPanel.center(false, true);
    }

    private void finishImport(Trigger trigger) {
        infoQuestionPanel.close();

        gateway.finishEndpointImport(ServiceManager.instance().getService(), getWizardContext(),
            new DefaultAsyncCallback<Boolean>() {
                @Override
                public void success(Boolean result) {
                    trigger.fire();
                }
            }
        );
    }
}
