package nl.sodeso.webservice.mock.console.client.application.ui;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface UiConstants extends Messages {

    @DefaultMessage("Requests")
    String Requests();

}