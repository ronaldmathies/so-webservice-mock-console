package nl.sodeso.webservice.mock.console.client.application.response.actions.timeout;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface TimeoutActionConstants extends Messages {


    @DefaultMessage("Enter the number of seconds which will cause the response to be delayed with.")
    String secondsDocumentation();

    @DefaultMessage("Seconds")
    String secondsHint();

    @DefaultMessage("Seconds")
    String seconds();

}