package nl.sodeso.webservice.mock.console.client.application.response.functions;

/**
 * @author Ronald Mathies
 */
public interface FunctionChosenHandler {

    void ok(String function);

}
