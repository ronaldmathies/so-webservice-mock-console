package nl.sodeso.webservice.mock.console.client.application.response.conditions;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class ConditionOption extends DefaultOption {

    private static final String KEY_BODY = "body";
    private static final String KEY_COOKIE = "cookie";
    private static final String KEY_HEADER = "header";
    private static final String KEY_METHOD = "method";
    private static final String KEY_PARAMETER = "parameter";
    private static final String KEY_PATH = "path";
    private static final String KEY_QUERY = "query";

    public static ConditionOption BODY = new ConditionOption(KEY_BODY, "Body");
    public static ConditionOption COOKIE = new ConditionOption(KEY_COOKIE, "Cookie");
    public static ConditionOption HEADER = new ConditionOption(KEY_HEADER, "Header");
    public static ConditionOption METHOD = new ConditionOption(KEY_METHOD, "Method");
    public static ConditionOption PARAMETER = new ConditionOption(KEY_PARAMETER, "Parameter");
    public static ConditionOption PATH = new ConditionOption(KEY_PATH, "Path");
    public static ConditionOption QUERY = new ConditionOption(KEY_QUERY, "Query");

    public ConditionOption() {}

    public ConditionOption(String code, String description) {
        super(code, description);
    }

    public static ConditionOption[] getOptions() {
        return new ConditionOption[] {
            BODY, COOKIE, HEADER, METHOD, PARAMETER, PATH, QUERY
        };
    }

    public static ConditionOption getOption(String key) {
        switch (key) {
            case KEY_BODY:
                return BODY;
            case KEY_COOKIE:
                return COOKIE;
            case KEY_HEADER:
                return HEADER;
            case KEY_METHOD:
                return METHOD;
            case KEY_PARAMETER:
                return PARAMETER;
            case KEY_PATH:
                return PATH;
            case KEY_QUERY:
                return QUERY;
        }

        return null;
    }

    public boolean isBodyCondition() {
        return this.getCode().equals(KEY_BODY);
    }

    public boolean isCookieCondition() {
        return this.getCode().equals(KEY_COOKIE);
    }

    public boolean isHeaderCondition() {
        return this.getCode().equals(KEY_HEADER);
    }

    public boolean isMethodCondition() {
        return this.getCode().equals(KEY_METHOD);
    }

    public boolean isParameterCondition() {
        return this.getCode().equals(KEY_PARAMETER);
    }

    public boolean isPathCondition() {
        return this.getCode().equals(KEY_PATH);
    }

    public boolean isQueryCondition() {
        return this.getCode().equals(KEY_QUERY);
    }
}
