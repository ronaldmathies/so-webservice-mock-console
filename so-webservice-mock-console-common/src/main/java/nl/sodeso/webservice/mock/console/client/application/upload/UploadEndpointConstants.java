package nl.sodeso.webservice.mock.console.client.application.upload;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface UploadEndpointConstants extends Messages {

    @DefaultMessage("Import")
    String title();

    @DefaultMessage("Step {0}: Import Options")
    String importOptionsTitle(int index);

    @DefaultMessage("Import responses?")
    String importResponses();

    @DefaultMessage("Import requests?")
    String importRequests();

    @DefaultMessage("Step {0}: Endpoint Name")
    String endpointNameTitle(int index);

    @DefaultMessage("Please enter a name for the endpoint:")
    String nameDocumentation();

    @DefaultMessage("Please enter a path for the endpoint, the path should represent the path of the service.")
    String pathDocumentation();

    @DefaultMessage("Finalize Import")
    String finalizeImportTitle();

    @DefaultMessage("The import of the endpoint is about to be performed, continue?")
    String finalizeImportQuestion();

    @DefaultMessage("Step {0}: Endpoint Imported")
    String endpointImportedTitle(int index);

    @DefaultMessage("The endpoint has been imported.")
    String uploadFinishedDocumentation();

    @DefaultMessage("Step {0}: Upload File")
    String fileUploadTitle(int index);

    @DefaultMessage("Select the JSON file containing the endpoint you would like to import.")
    String fileDocumentation();
}