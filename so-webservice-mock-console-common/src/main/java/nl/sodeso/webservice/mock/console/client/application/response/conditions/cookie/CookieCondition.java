package nl.sodeso.webservice.mock.console.client.application.response.conditions.cookie;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionOption;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Condition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import static nl.sodeso.webservice.mock.console.client.application.Resources.COOKIE_CONDITION_I18N;

/**
 * @author Ronald Mathies
 */
public class CookieCondition extends Condition {


    private static final String KEY_COOKIE_CONDITION = "cookie-condition";
    private static final String KEY_EXPRESSION_TYPE = "expression-type";
    private static final String KEY_EXPRESSION = "expression";
    private static final String KEY_NAME = "name";
    private static final String KEY_MATCHES = "matches";

    private OptionType<ExpressionOption> expressionOption = new OptionType<>(ExpressionOption.XPATH);
    private StringType name = new StringType();
    private StringType expression = new StringType();
    private StringType matches = new StringType();

    private transient ExpressionComboboxField expressionComboboxField = null;
    private transient TextField<StringType> nameField = null;
    private transient TextField<StringType> expressionField = null;
    private transient TextField<StringType> matchesField = null;


    private transient EntryWithSingleWidget entry = null;

    public CookieCondition() {
    }

    public ConditionOption getConditionOption() {
        return ConditionOption.COOKIE;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        expressionComboboxField = new ExpressionComboboxField(expressionOption, true);
        expressionComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_EXPRESSION_TYPE, ValidationMessage.Level.ERROR, expressionOption));

        nameField = new TextField<>(KEY_NAME, this.name);
        nameField.addValidationRule(
            new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return nameField.getValue();
                }
            }
        );

        expressionField = new TextField<>(KEY_EXPRESSION, this.expression);
        expressionField.addValidationRule(
            new MandatoryValidationRule(KEY_EXPRESSION, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return expressionField.getValue();
                }

                @Override
                public boolean isApplicable() {
                    return !ExpressionOption.NONE.equals(expressionOption.getValue());
                }
            }
        );

        matchesField = new TextField<>(KEY_MATCHES, this.matches);
        matchesField.addValidationRule(
            new MandatoryValidationRule(KEY_MATCHES, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return matchesField.getValue();
                }
            }
        );

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_COOKIE_CONDITION, COOKIE_CONDITION_I18N.cookieConditionDocumentation()),
                new EntryWithLabelAndWidget(KEY_NAME, COOKIE_CONDITION_I18N.name(), nameField),
                new EntryWithLabelAndWidget(KEY_EXPRESSION_TYPE, COOKIE_CONDITION_I18N.expressionType(), expressionComboboxField),
                new EntryWithLabelAndWidget(KEY_EXPRESSION, COOKIE_CONDITION_I18N.expression(), expressionField),
                new EntryWithLabelAndWidget(KEY_MATCHES, COOKIE_CONDITION_I18N.matches(), new InsertFunctionButton(matchesField))
            );

        expressionOption.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(expressionComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                form.setEntriesVisible(!ExpressionOption.NONE.equals(expressionOption.getValue()), KEY_EXPRESSION);
            }
        });

        // TODO: Could be nicer.
        form.addAttachHandler(event -> {
            form.setEntriesVisible(!ExpressionOption.NONE.equals(expressionOption.getValue()), KEY_EXPRESSION);
        });

        LegendPanel legendPanel = new LegendPanel(null, COOKIE_CONDITION_I18N.cookieCondition());
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_COOKIE_CONDITION, legendPanel);
        return entry;
    }

    public OptionType<ExpressionOption> getExpressionOption() {
        return expressionOption;
    }

    public void setExpressionOption(OptionType<ExpressionOption> expressionOption) {
        this.expressionOption = expressionOption;
    }

    public StringType getName() {
        return name;
    }

    public void setName(StringType name) {
        this.name = name;
    }

    public StringType getExpression() {
        return expression;
    }

    public void setExpression(StringType expression) {
        this.expression = expression;
    }

    public StringType getMatches() {
        return matches;
    }

    public void setMatches(StringType matches) {
        this.matches = matches;
    }
}