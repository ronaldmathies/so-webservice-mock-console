package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @author Ronald Mathies
 */
public class ContentTypeComboboxField extends ComboboxField<ContentTypeOption> {

    public ContentTypeComboboxField(OptionType<ContentTypeOption> optionValue, StringType otherContentType) {
        super(optionValue, otherContentType, ContentTypeOption.OTHER);
        addItems(ContentTypeOption.getOptions());
    }
}
