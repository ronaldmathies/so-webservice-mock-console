package nl.sodeso.webservice.mock.console.client.application.request.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.webservice.mock.console.client.application.request.Request;
import nl.sodeso.webservice.mock.console.client.application.request.RequestSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.response.Response;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface RequestRpcAsync {

    void findSummaries(ServiceOption serviceOption, String endpointName, AsyncCallback<ArrayList<RequestSummaryItem>> result);
    void findSummaries(ServiceOption serviceOption, String endpointName, String responseName, AsyncCallback<ArrayList<RequestSummaryItem>> result);

    void findSingle(ServiceOption serviceOption, String endpointName, String id, AsyncCallback<Request> result);
    void findSingle(ServiceOption serviceOption, String endpointName, String responseName, String id, AsyncCallback<Request> result);

    void delete(ServiceOption serviceOption, String endpointName, String id, AsyncCallback<Void> result);
    void deleteAll(ServiceOption serviceOption, String endpointName, AsyncCallback<Void> result);
    void delete(ServiceOption serviceOption, String endpointName, String responseName, String id, AsyncCallback<Void> result);
    void deleteAll(ServiceOption serviceOption, String endpointName, String responseName, AsyncCallback<Void> result);
}
