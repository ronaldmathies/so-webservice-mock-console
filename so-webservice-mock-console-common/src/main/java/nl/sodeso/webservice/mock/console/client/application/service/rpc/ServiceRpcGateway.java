package nl.sodeso.webservice.mock.console.client.application.service.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class ServiceRpcGateway implements RpcGateway<ServiceRpcAsync>, ServiceRpcAsync {

    /**
     * {@inheritDoc}
     */
    public ServiceRpcAsync getRpcAsync() {
        return GWT.create(ServiceRpc.class);
    }
}
