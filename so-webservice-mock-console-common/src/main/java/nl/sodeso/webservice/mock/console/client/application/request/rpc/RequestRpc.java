package nl.sodeso.webservice.mock.console.client.application.request.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.webservice.mock.console.client.application.request.Request;
import nl.sodeso.webservice.mock.console.client.application.request.RequestSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.response.Response;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("webservicemockconsole/endpoint.webservicemock-request")
public interface RequestRpc extends XsrfProtectedService {

    ArrayList<RequestSummaryItem> findSummaries(ServiceOption serviceOption, String endpointyName);
    ArrayList<RequestSummaryItem> findSummaries(ServiceOption serviceOption, String endpointName, String responseName);

    Request findSingle(ServiceOption serviceOption, String endpointName, String id);
    Request findSingle(ServiceOption serviceOption, String endpointName, String responseName, String id);

    void delete(ServiceOption serviceOption, String endpointName, String id);
    void deleteAll(ServiceOption serviceOption, String endpointName);
    void delete(ServiceOption serviceOption, String endpointName, String responseName, String id);
    void deleteAll(ServiceOption serviceOption, String endpointName, String responseName);

}
