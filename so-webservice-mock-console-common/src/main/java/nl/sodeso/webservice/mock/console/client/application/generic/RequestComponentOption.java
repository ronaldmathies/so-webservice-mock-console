package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class RequestComponentOption extends DefaultOption {

    private static final String KEY_UUID = "uuid";
    private static final String KEY_BODY = "body";
    private static final String KEY_COOKIE = "cookie";
    private static final String KEY_HEADER = "header";
    private static final String KEY_QUERY = "query";

    public static RequestComponentOption UUID = new RequestComponentOption(KEY_UUID, "Uuid");
    public static RequestComponentOption BODY = new RequestComponentOption(KEY_BODY, "Body");
    public static RequestComponentOption COOKIE = new RequestComponentOption(KEY_COOKIE, "Cookie");
    public static RequestComponentOption HEADER = new RequestComponentOption(KEY_HEADER, "Header");
    public static RequestComponentOption QUERY = new RequestComponentOption(KEY_QUERY, "Query");

    public RequestComponentOption() {}

    public RequestComponentOption(String code, String description) {
        super(code, description);
    }

    public static RequestComponentOption[] getOptions() {
        return new RequestComponentOption[] {
                UUID, BODY, COOKIE, HEADER, QUERY
        };
    }

    public boolean isUuid() {
        return this.equals(UUID);
    }

    public boolean isBody() {
        return this.equals(BODY);
    }

    public boolean isCookie() {
        return this.equals(COOKIE);
    }

    public boolean isHeader() {
        return this.equals(HEADER);
    }

    public boolean isQuery() {
        return this.equals(QUERY);
    }
}
