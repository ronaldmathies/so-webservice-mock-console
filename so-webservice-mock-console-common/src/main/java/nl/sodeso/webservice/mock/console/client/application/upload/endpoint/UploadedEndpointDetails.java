package nl.sodeso.webservice.mock.console.client.application.upload.endpoint;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class UploadedEndpointDetails implements Serializable {

    private String name = null;
    private String path = null;
    private String uuid = null;
    private boolean containsResponses = false;
    private boolean containsRequests = false;

    public UploadedEndpointDetails() {}

    public UploadedEndpointDetails(String name, String path, String uuid) {
        this.name = name;
        this.path = path;
        this.uuid = uuid;
    }

    public String getName() {
        return this.name;
    }

    public String getPath() {
        return this.path;
    }

    public String getUuid() {
        return this.uuid;
    }

    public boolean isContainsResponses() {
        return containsResponses;
    }

    public void setContainsResponses(boolean containsResponses) {
        this.containsResponses = containsResponses;
    }

    public boolean isContainsRequests() {
        return containsRequests;
    }

    public void setContainsRequests(boolean containsRequests) {
        this.containsRequests = containsRequests;
    }
}
