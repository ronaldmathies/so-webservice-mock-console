package nl.sodeso.webservice.mock.console.client.application.response.type;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class ResponseTypeComboboxField extends ComboboxField<ResponseTypeOption> {

    public ResponseTypeComboboxField(OptionType<ResponseTypeOption> optionValue) {
        super(optionValue);
        addItems(ResponseTypeOption.getOptions());
    }
}
