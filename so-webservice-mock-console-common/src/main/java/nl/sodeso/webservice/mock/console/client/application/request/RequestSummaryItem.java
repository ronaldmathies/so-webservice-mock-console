package nl.sodeso.webservice.mock.console.client.application.request;

import nl.sodeso.gwt.ui.client.form.table.summary.AbstractSummaryItem;

/**
 * @author Ronald Mathies
 */
public class RequestSummaryItem extends AbstractSummaryItem {

    private String id;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    @Override
    public String getLabel() {
        return this.id;
    }
}
