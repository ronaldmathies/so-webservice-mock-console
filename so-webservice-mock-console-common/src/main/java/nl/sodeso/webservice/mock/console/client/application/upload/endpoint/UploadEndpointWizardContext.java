package nl.sodeso.webservice.mock.console.client.application.upload.endpoint;

import nl.sodeso.gwt.ui.client.wizard.WizardContext;

/**
 * @author Ronald Mathies
 */
public class UploadEndpointWizardContext extends WizardContext {

    private String label;
    private String path;
    private UploadedEndpointDetails uploadedEndpointDetails = null;

    private boolean includeResponses = true;
    private boolean includeRequests = true;

    public UploadEndpointWizardContext() {
    }

    public UploadedEndpointDetails getUploadedEndpointDetails() {
        return uploadedEndpointDetails;
    }

    public void setUploadedEndpointDetails(UploadedEndpointDetails uploadedEndpointDetails) {
        this.uploadedEndpointDetails = uploadedEndpointDetails;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isIncludeResponses() {
        return includeResponses;
    }

    public void setIncludeResponses(boolean includeResponses) {
        this.includeResponses = includeResponses;
    }

    public boolean isIncludeRequests() {
        return includeRequests;
    }

    public void setIncludeRequests(boolean includeRequests) {
        this.includeRequests = includeRequests;
    }
}
