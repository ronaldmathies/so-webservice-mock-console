package nl.sodeso.webservice.mock.console.client.application.generic.cookie;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionConfig;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class Cookies extends WidgetCollectionField<Cookie> {

    public Cookies() {
        super(WidgetCollectionConfig.DEFAULT);
    }

    @Override
    public WidgetSupplier<Cookie> createWidgetSupplier() {
        return callback -> callback.created(new Cookie());
    }
}
