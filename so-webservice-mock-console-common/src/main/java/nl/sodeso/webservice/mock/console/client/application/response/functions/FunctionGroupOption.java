package nl.sodeso.webservice.mock.console.client.application.response.functions;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class FunctionGroupOption extends DefaultOption {

    public FunctionGroupOption() {}

    public FunctionGroupOption(String code) {
        super(code, code);
    }

}
