package nl.sodeso.webservice.mock.console.client.application.response.actions.variable;

import com.google.gwt.dom.client.Style;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.response.functions.InsertFunctionButton;

import java.io.Serializable;

import static nl.sodeso.webservice.mock.console.client.application.Resources.VARIABLE_I18N;

/**
 * @author Ronald Mathies
 */
public class Variable implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private static final String KEY_SCOPE = "scope";
    private static final String KEY_NAME = "name";
    private static final String KEY_VALUE = "value";

    private String uuid = null;
    private OptionType<VariableScopeOption> variableScopeOption = new OptionType<>(VariableScopeOption.REQUEST);
    private StringType name = new StringType();
    private StringType value = new StringType();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient VariableScopeComboboxField variableScopeComboboxField = null;
    private transient TextField<StringType> nameField = null;
    private transient TextField<StringType> valueField = null;

    public Variable() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        this.variableScopeComboboxField = new VariableScopeComboboxField(variableScopeOption);
        this.variableScopeComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_SCOPE, ValidationMessage.Level.ERROR, variableScopeOption));

        this.nameField = new TextField<>(KEY_NAME, this.name)
                .setPlaceholder(VARIABLE_I18N.name());
        this.nameField.addValidationRule(new MandatoryValidationRule(KEY_NAME, VARIABLE_I18N.name(), ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return nameField.getValue();
            }
        });
        this.valueField = new TextField<>(KEY_VALUE, this.value)
            .setPlaceholder(VARIABLE_I18N.value());
        this.valueField.addValidationRule(new MandatoryValidationRule(KEY_VALUE, VARIABLE_I18N.value(), ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return valueField.getValue();
            }
        });

        this.entry = new EntryWithWidgetAndWidget(null,
                this.nameField,
                new HorizontalPanel()
                        .addWidget(this.variableScopeComboboxField, 120, Style.Unit.PX)
                        .addWidget(new InsertFunctionButton(valueField)));

        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<VariableScopeOption> getVariableScopeOption() {
        return variableScopeOption;
    }

    public void setVariableScopeOption(OptionType<VariableScopeOption> variableScopeOption) {
        this.variableScopeOption = variableScopeOption;
    }

    public StringType getName() {
        return this.name;
    }

    public void setName(StringType name) {
        this.name = name;
    }

    public StringType getValue() {
        return this.value;
    }

    public void setValue(StringType value) {
        this.value = value;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }


}
