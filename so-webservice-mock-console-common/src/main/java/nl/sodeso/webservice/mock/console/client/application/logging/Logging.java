package nl.sodeso.webservice.mock.console.client.application.logging;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.filters.OptionFilter;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionOption;
import nl.sodeso.webservice.mock.console.client.application.generic.RequestComponentComboboxField;
import nl.sodeso.webservice.mock.console.client.application.generic.RequestComponentOption;

import java.io.Serializable;
import java.util.Arrays;

import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class Logging implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_LOGGING = "logging";
    private static final String KEY_ENABLE_LOGGING = "enable-logging";
    private static final String KEY_REQUEST_STORE_SIZE = "request-store-size";
    private static final String KEY_REQUEST_COMPONENT = "request-component";
    private static final String KEY_NAME = "name";
    private static final String KEY_EXPRESSION_TYPE = "expression-type";
    private static final String KEY_EXPRESSION_VALUE = "expression-value";
    private static final String KEY_GENERAL_LEGEND = "general";
    private static final String KEY_LOGGING_LEGEND = "logging";
    private static final String KEY_CONDITION_LEGEND = "condition";

    private BooleanType enableLogging = new BooleanType(false);
    private IntType numberOfRequestsToStore = new IntType(32);
    private OptionType<RequestComponentOption> requestComponentOption = new OptionType<>(RequestComponentOption.HEADER);
    private OptionType<ExpressionOption> expressionOption = new OptionType<>(ExpressionOption.XPATH);
    private StringType name = new StringType();
    private StringType expression = new StringType();

    private transient EntryForm entryForm = null;
    private transient EntryForm conditionEntryForm = null;

    private transient RadioButtonGroupField enableLoggingGroupField = null;
    private transient TextField numberOfRequestsToStoreField = null;
    private transient RequestComponentComboboxField requestComponentComboboxField = null;
    private transient ExpressionComboboxField expressionComboboxField = null;
    private transient TextField nameField = null;
    private transient TextField expressionField = null;

    public Logging() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithContainer(KEY_LOGGING_LEGEND, setupLoggingPanel()));
        entryForm.addEntry(new EntryWithContainer(KEY_CONDITION_LEGEND, setupConditionPanel()));

        return this.entryForm;
    }

    private EntryForm setupLoggingPanel() {
        enableLoggingGroupField = new RadioButtonGroupField(KEY_ENABLE_LOGGING, this.enableLogging);
        enableLoggingGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        enableLoggingGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        enableLoggingGroupField.setAlignment(Align.HORIZONTAL);

        numberOfRequestsToStoreField = new TextField<>(KEY_REQUEST_STORE_SIZE, this.numberOfRequestsToStore)
            .addFilter(new NumericInputFilter(false))
                .addValidationRule(
                    new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
                        @Override
                        public String getValue() {
                            return numberOfRequestsToStoreField.getValue();
                        }

                        @Override
                        public boolean isApplicable() {
                            return enableLoggingGroupField.getSelected().getKey().equals(Boolean.TRUE.toString());
                        }
                    }
                );

        EntryForm form = new EntryForm(KEY_LOGGING)
            .addEntries(
                new EntryWithDocumentation(KEY_ENABLE_LOGGING, ENDPOINT_I18N.enableLoggingDocumentation()),
                new EntryWithLabelAndWidget(KEY_ENABLE_LOGGING, ENDPOINT_I18N.enableLogging(), enableLoggingGroupField),
                new EntryWithDocumentation(KEY_REQUEST_STORE_SIZE, ENDPOINT_I18N.requestStoreSizeDocumentation()),
                new EntryWithLabelAndWidget(KEY_REQUEST_STORE_SIZE, ENDPOINT_I18N.requestStoreSize(), numberOfRequestsToStoreField)
        );

        enableLogging.addValueChangedEventHandler(new ValueChangedEventHandler<BooleanType>(enableLoggingGroupField) {
            @Override
            public void onValueCanged(ValueChangedEvent<BooleanType> event) {
                Boolean showLoggingFields = event.getValueType().getValue();
                form.setEntriesVisible(showLoggingFields, KEY_REQUEST_STORE_SIZE);
                entryForm.setEntriesVisible(showLoggingFields, KEY_CONDITION_LEGEND);
            }
        });

        // TODO: Could be nicer.
        form.addAttachHandler(event -> {
            form.setEntriesVisible(enableLogging.getValue(), KEY_REQUEST_STORE_SIZE);
            entryForm.setEntriesVisible(enableLogging.getValue(), KEY_CONDITION_LEGEND);
        });

        return form;
    }

    private EntryForm setupConditionPanel() {
        requestComponentComboboxField = new RequestComponentComboboxField(requestComponentOption);
        requestComponentComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_REQUEST_COMPONENT, ValidationMessage.Level.ERROR, requestComponentOption));

        expressionComboboxField = new ExpressionComboboxField(expressionOption, true);
        expressionComboboxField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_EXPRESSION_TYPE, ValidationMessage.Level.ERROR, expressionOption));
        expressionComboboxField.setOptionFilter(option -> !(option.isNone() && requestComponentOption.getValue().isBody()));

        nameField = new TextField<>(KEY_NAME, this.name);
        nameField.addValidationRule(

            new MandatoryValidationRule(KEY_NAME, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return nameField.getValue();
                }

                @Override
                public boolean isApplicable() {
                    return conditionEntryForm.isEntryVisible(KEY_NAME);
                }
            }
        );

        expressionField = new TextField<>(KEY_EXPRESSION_VALUE, this.expression);
        expressionField.addValidationRule(
            new MandatoryValidationRule(KEY_EXPRESSION_VALUE, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return expressionField.getValue();
                }

                @Override
                public boolean isApplicable() {
                    return conditionEntryForm.isEntryVisible(KEY_EXPRESSION_VALUE);
                }
            }
        );

        conditionEntryForm = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_CONDITION_LEGEND, ENDPOINT_I18N.conditionPanelDocumentation()),
                new EntryWithDocumentation(KEY_REQUEST_COMPONENT, ENDPOINT_I18N.requestComponentDocumentation()),
                new EntryWithLabelAndWidget(KEY_REQUEST_COMPONENT, ENDPOINT_I18N.requestComponent(), requestComponentComboboxField),

                new EntryWithDocumentation(KEY_NAME, ENDPOINT_I18N.nameDocumentation()),
                new EntryWithLabelAndWidget(KEY_NAME, ENDPOINT_I18N.name(), nameField),

                new EntryWithDocumentation(KEY_EXPRESSION_TYPE, ENDPOINT_I18N.expressionTypeDocumentation()),
                new EntryWithLabelAndWidget(KEY_EXPRESSION_TYPE, ENDPOINT_I18N.expressionType(), expressionComboboxField),
                new EntryWithDocumentation(KEY_EXPRESSION_VALUE, ENDPOINT_I18N.expressionDocumentation()),
                new EntryWithLabelAndWidget(KEY_EXPRESSION_VALUE, ENDPOINT_I18N.expression(), expressionField)
            );

        requestComponentOption.addValueChangedEventHandler(new ValueChangedEventHandler<OptionType<RequestComponentOption>>(requestComponentComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<OptionType<RequestComponentOption>> event) {
                expressionComboboxField.replaceItems(Arrays.asList(ExpressionOption.getOptions(true)), true);

                showhideFields();
            }
        });

        expressionOption.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(expressionComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                showhideFields();
            }
        });

        conditionEntryForm.addAttachHandler(event -> {
            if (enableLogging.getValue()) {
                showhideFields();
            }
        });

        return conditionEntryForm;
    }

    private void showhideFields() {
        RequestComponentOption requestComponent = requestComponentOption.getValue();

        conditionEntryForm.setEntriesVisible(
               !requestComponent.isUuid(), KEY_EXPRESSION_TYPE);

        if (requestComponent.isBody() && expressionOption.getValue().isNone()) {
            expressionOption.setValue(ExpressionOption.XPATH);
        } else if (!requestComponent.isUuid() && expressionOption.getValue() == null) {
            expressionOption.setValue(ExpressionOption.NONE);
        }

        conditionEntryForm.setEntriesVisible(
        requestComponent.isHeader() ||
               requestComponent.isCookie() ||
               requestComponent.isQuery(), KEY_NAME);

        ExpressionOption expression = expressionOption.getValue();
        conditionEntryForm.setEntriesVisible(
               !expression.isNone() && !requestComponent.isUuid(), KEY_EXPRESSION_VALUE);

    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entryForm);
    }

    public BooleanType getEnableLogging() {
        return enableLogging;
    }

    public void setEnableLogging(BooleanType enableLogging) {
        this.enableLogging = enableLogging;
    }

    public IntType getNumberOfRequestsToStore() {
        return numberOfRequestsToStore;
    }

    public void setNumberOfRequestsToStore(IntType numberOfRequestsToStore) {
        this.numberOfRequestsToStore = numberOfRequestsToStore;
    }

    public OptionType<RequestComponentOption> getRequestComponentOption() {
        return requestComponentOption;
    }

    public void setRequestComponentOption(OptionType<RequestComponentOption> requestComponentOption) {
        this.requestComponentOption = requestComponentOption;
    }

    public OptionType<ExpressionOption> getExpressionOption() {
        return expressionOption;
    }

    public void setExpressionOption(OptionType<ExpressionOption> expressionOption) {
        this.expressionOption = expressionOption;
    }

    public StringType getName() {
        return name;
    }

    public void setName(StringType name) {
        this.name = name;
    }

    public StringType getExpression() {
        return expression;
    }

    public void setExpression(StringType expression) {
        this.expression = expression;
    }
}
