package nl.sodeso.webservice.mock.console.client.application.response.conditions;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class ConditionComboboxField extends ComboboxField<ConditionOption> {

    private static final String KEY_CONDITION = "conditiion";

    public ConditionComboboxField(OptionType<ConditionOption> optionValue) {
        super(KEY_CONDITION, optionValue);
        addItems(ConditionOption.getOptions());
    }
}
