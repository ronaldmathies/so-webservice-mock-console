package nl.sodeso.webservice.mock.console.client.application.request;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.TextHeader;
import com.google.gwt.view.client.NoSelectionModel;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.form.table.DefaultEmptyTableMessage;
import nl.sodeso.webservice.mock.console.client.application.generic.header.Header;
import nl.sodeso.webservice.mock.console.client.application.generic.query.Query;

import java.util.Comparator;

/**
 * @author Ronald Mathies
 */
public class QueryTable extends CellTableField<Query> {

    public QueryTable() {
        super("key", 1000);
        setSelectionModel(new NoSelectionModel<>());

        TextColumn<Query> nameColumn = new TextColumn<Query>() {
            @Override
            public String getValue(Query query) {
                return query.getName().getValue();
            }
        };
        addColumn(nameColumn, new TextHeader("Name"));

        TextColumn<Query> valueColumn = new TextColumn<Query>() {
            @Override
            public String getValue(Query query) {
                return query.getValue().getValue();
            }
        };
        addColumn(valueColumn, new TextHeader("Value"));

        ColumnSortEvent.ListHandler<Query> listHandler = new ColumnSortEvent.ListHandler<>(getListDataProvider().getList());
        listHandler.setComparator(nameColumn, Comparator.comparing(query -> {
            return query.getName().getValue();
        }));
        this.addColumnSortHandler(listHandler);

        // Set the default sorting to ascending.
        valueColumn.setDefaultSortAscending(true);

        // Set the sorting order of the cell list to the first and only column.
        ColumnSortList.ColumnSortInfo columnSortInfo = new ColumnSortList.ColumnSortInfo(nameColumn, true);
        this.getColumnSortList().push(columnSortInfo);

        this.setColumnWidth(0, 20, com.google.gwt.dom.client.Style.Unit.PCT);
        this.setColumnWidth(1, 80, com.google.gwt.dom.client.Style.Unit.PCT);
        this.setEmptyTableWidget(new DefaultEmptyTableMessage("no-headers", "No headers"));
    }

}
