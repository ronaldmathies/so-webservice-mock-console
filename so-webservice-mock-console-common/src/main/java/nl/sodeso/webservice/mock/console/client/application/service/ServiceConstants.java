package nl.sodeso.webservice.mock.console.client.application.service;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface ServiceConstants extends Messages {

    @DefaultMessage("Services")
    String title();

    @DefaultMessage("Select a service from the list to display all the information.")
    String serviceDocumentation();

    @DefaultMessage("Service")
    String service();
}