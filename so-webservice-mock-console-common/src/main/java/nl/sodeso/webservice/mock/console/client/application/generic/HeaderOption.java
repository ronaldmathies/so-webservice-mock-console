package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class HeaderOption extends DefaultOption {

    private static final String KEY_OTHER = "other";
    private static final String KEY_CACHE_CONTROL = "Cache-Control";
    private static final String KEY_EXPIRES = "Expires";
    private static final String KEY_LAST_MODIFIED = "Last-Modified";
    private static final String KEY_PRAGMA = "Pragma";
    private static final String KEY_ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    private static final String KEY_CONTENT_DISPOSITION = "Content-Disposition";
    private static final String KEY_USER_AGENT = "User-Agent";

    public static HeaderOption OTHER = new HeaderOption(KEY_OTHER, "Other ( Please Specify )", "");
    public static HeaderOption CACHE_CONTROL = new HeaderOption(KEY_CACHE_CONTROL, "Cache-Control", "max-age=3600");
    public static HeaderOption CONTENT_DISPOSITION = new HeaderOption(KEY_CONTENT_DISPOSITION, "Content-Disposition", "attachment; filename=\"filename.txt\"");
    public static HeaderOption EXPIRES = new HeaderOption(KEY_EXPIRES, "Expires", "Thu, 01 Dec 1994 16:00:00 GMT");
    public static HeaderOption LAST_MODIFIED = new HeaderOption(KEY_LAST_MODIFIED, "Last-Modified", "Tue, 15 Nov 1994 12:45:26 GMT");
    public static HeaderOption PRAGMA = new HeaderOption(KEY_PRAGMA, "Pragma", "no-cache");
    public static HeaderOption ACCESS_CONTROL_ALLOW_ORIGIN = new HeaderOption(KEY_ACCESS_CONTROL_ALLOW_ORIGIN, "Access-Control-Allow-Origin", "*");
    public static HeaderOption USER_AGENT = new HeaderOption(KEY_USER_AGENT, "User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14");

    private String example;

    public HeaderOption() {}

    public HeaderOption(String code, String description, String example) {
        super(code, description);

        this.example = example;
    }

    public static HeaderOption[] getOptions() {
        return new HeaderOption[] {
                CACHE_CONTROL, EXPIRES, LAST_MODIFIED, PRAGMA, ACCESS_CONTROL_ALLOW_ORIGIN, CONTENT_DISPOSITION, USER_AGENT
        };
    }

    public static HeaderOption fromValue(String header) {
        for (HeaderOption cto : getOptions()) {
            if (cto.getKey().equals(header)) {
                return cto;
            }
        }

        return null;
    }

    public String getExample() {
        return example;
    }

    public boolean isOther() {
        return this.equals(HeaderOption.OTHER);
    }
}
