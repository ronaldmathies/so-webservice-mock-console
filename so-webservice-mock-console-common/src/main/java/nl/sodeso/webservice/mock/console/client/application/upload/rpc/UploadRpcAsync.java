package nl.sodeso.webservice.mock.console.client.application.upload.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadedEndpointDetails;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadEndpointWizardContext;

/**
 * @author Ronald Mathies
 */
public interface UploadRpcAsync {

    void prepareEndpointImport(ServiceOption serviceOption, String uuid, AsyncCallback<UploadedEndpointDetails> result);
    void finishEndpointImport(ServiceOption serviceOption, UploadEndpointWizardContext context, AsyncCallback<Boolean> result);

}
