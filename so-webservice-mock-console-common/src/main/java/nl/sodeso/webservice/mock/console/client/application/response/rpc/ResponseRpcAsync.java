package nl.sodeso.webservice.mock.console.client.application.response.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.webservice.mock.console.client.application.response.Response;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface ResponseRpcAsync {

    void findSummaries(ServiceOption serviceOption, String endpointName, AsyncCallback<ArrayList<ResponseSummaryItem>> result);
    void getDetails(ServiceOption serviceOption, String endpointName, String responseName, AsyncCallback<Response> result);

    void isNameUnique(ServiceOption serviceOption, String endpointName, String responseName, AsyncCallback<ValidationResult> result);

    void save(ServiceOption serviceOption, String endpointName, Response response, AsyncCallback<ResponseSummaryItem> result);
    void duplicate(ServiceOption serviceOption, String endpointName, String responseName, String newResponseName, AsyncCallback<ResponseSummaryItem> result);

    void delete(ServiceOption serviceOption, String endpointName, String responseName, AsyncCallback<Void> result);
}
