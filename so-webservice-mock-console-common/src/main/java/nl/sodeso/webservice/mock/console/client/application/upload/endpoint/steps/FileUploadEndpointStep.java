package nl.sodeso.webservice.mock.console.client.application.upload.endpoint.steps;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadField;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadForm;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadType;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadEndpointWizardContext;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadedEndpointDetails;
import nl.sodeso.webservice.mock.console.client.application.upload.rpc.UploadRpcGateway;

import static nl.sodeso.webservice.mock.console.client.application.Resources.RESPONSE_I18N;
import static nl.sodeso.webservice.mock.console.client.application.Resources.UPLOAD_ENDPOINT_I18N;

/**
 * @author Ronald Mathies
 */
public class FileUploadEndpointStep extends AbstractWizardStep<UploadEndpointWizardContext> {

    private static final String KEY_FILE = "file";
    private static final String KEY_STEP = "step-";

    private static final String ACTION_PATH = "/file.upload";

    private FileUploadType fileUpload = new FileUploadType();

    private transient FileUploadForm fileUploadForm = null;
    private transient FileUploadField fileUploadField = null;

    private transient LegendPanel legendPanel = null;

    private transient AbstractWizardStep<UploadEndpointWizardContext> nextStep = null;

    private UploadRpcGateway gateway = GWT.create(UploadRpcGateway.class);

    public FileUploadEndpointStep(AbstractWizardStep<UploadEndpointWizardContext> callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep<UploadEndpointWizardContext> getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep<UploadEndpointWizardContext> getNextStep() {
        if (getWizardContext().getUploadedEndpointDetails() != null && (
                getWizardContext().getUploadedEndpointDetails().isContainsRequests() ||
                getWizardContext().getUploadedEndpointDetails().isContainsResponses())) {
            if (this.nextStep == null || !(this.nextStep instanceof SelectUploadEndpointOptionsStep)) {
                this.nextStep = new SelectUploadEndpointOptionsStep(this);
            }
        } else {
            if (this.nextStep == null || !(this.nextStep instanceof ChooseEndpointNameStep)) {
                this.nextStep = new ChooseEndpointNameStep(this);
            }
        }

        return this.nextStep;

    }

    @Override
    public Widget getBodyWidget() {
        if (this.legendPanel != null) {
            return this.legendPanel;
        }

        this.fileUploadField = new FileUploadField(KEY_FILE, fileUpload);
        this.fileUploadField.addValidationRule(new MandatoryValidationRule(KEY_FILE, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return fileUploadField.getValue();
            }

        });
        this.fileUploadForm = new FileUploadForm(fileUploadField, ACTION_PATH);

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntries(
                new EntryWithDocumentation(KEY_FILE, UPLOAD_ENDPOINT_I18N.fileDocumentation()),
                new EntryWithLabelAndWidget(KEY_FILE, RESPONSE_I18N.file(), this.fileUploadForm)
        );

        this.legendPanel = new LegendPanel(KEY_STEP + getStepIndex(), UPLOAD_ENDPOINT_I18N.fileUploadTitle(getStepIndex()));
        this.legendPanel.add(entryForm);

        return this.legendPanel;
    }

    @Override
    public void onBeforeNext(Trigger trigger) {
        if (this.fileUpload.isChanged()) {
            submit(trigger);
        }
    }

    private void submit(Trigger trigger) {
        this.fileUploadForm.setFileUploadCompleteTrigger(() -> gateway.prepareEndpointImport(ServiceManager.instance().getService(), fileUpload.getValue().getUuid(), new DefaultAsyncCallback<UploadedEndpointDetails>() {
            @Override
            public void success(UploadedEndpointDetails result) {
                getWizardContext().setUploadedEndpointDetails(result);
                trigger.fire();
            }
        }));

        if (this.fileUpload.isChanged()) {
            this.fileUploadForm.submit();
        }
    }
}
