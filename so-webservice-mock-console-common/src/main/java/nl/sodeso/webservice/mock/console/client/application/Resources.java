package nl.sodeso.webservice.mock.console.client.application;

import com.google.gwt.core.client.GWT;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointConstants;
import nl.sodeso.webservice.mock.console.client.application.generic.cookie.CookieConstants;
import nl.sodeso.webservice.mock.console.client.application.generic.header.HeaderConstants;
import nl.sodeso.webservice.mock.console.client.application.request.RequestConstants;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseConstants;
import nl.sodeso.webservice.mock.console.client.application.response.actions.ActionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.actions.exception.ExceptionActionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.actions.timeout.TimeoutActionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.actions.variable.VariableActionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.body.BodyConditionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.cookie.CookieConditionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.header.HeaderConditionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.method.MethodConditionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.parameter.ParameterConditionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.path.PathConditionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.query.QueryConditionConstants;
import nl.sodeso.webservice.mock.console.client.application.response.functions.FunctionConstants;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceConstants;
import nl.sodeso.webservice.mock.console.client.application.ui.UiConstants;
import nl.sodeso.webservice.mock.console.client.application.upload.UploadEndpointConstants;

/**
 * The resources class can be used to register all instances created of Constants and Message interfaces for
 * i18n purposes.
 *
 * @author Ronald Mathies
 */
public final class Resources {

    private Resources() {}

    public final static ServiceConstants SERVICE_I18N = GWT.create(ServiceConstants.class);



    public final static UiConstants UI_I18N = GWT.create(UiConstants.class);
    public final static EndpointConstants ENDPOINT_I18N = GWT.create(EndpointConstants.class);
    public final static ResponseConstants RESPONSE_I18N = GWT.create(ResponseConstants.class);
    public final static RequestConstants REQUEST_I18N = GWT.create(RequestConstants.class);

    public final static ConditionConstants CONDITION_I18N = GWT.create(ConditionConstants.class);
    public final static FunctionConstants FUNCTION_I18N = GWT.create(FunctionConstants.class);
    public final static BodyConditionConstants BODY_CONDITION_I18N = GWT.create(BodyConditionConstants.class);
    public final static HeaderConditionConstants HEADER_CONDITION_I18N = GWT.create(HeaderConditionConstants.class);
    public final static CookieConditionConstants COOKIE_CONDITION_I18N = GWT.create(CookieConditionConstants.class);
    public final static MethodConditionConstants METHOD_CONDITION_I18N = GWT.create(MethodConditionConstants.class);
    public final static ParameterConditionConstants PARAMETER_I18N = GWT.create(ParameterConditionConstants.class);
    public final static PathConditionConstants PATH_I18N = GWT.create(PathConditionConstants.class);
    public final static QueryConditionConstants QUERY_I18N = GWT.create(QueryConditionConstants.class);

    public final static ActionConstants ACTION_I18N = GWT.create(ActionConstants.class);
    public final static TimeoutActionConstants TIMEOUT_I18N = GWT.create(TimeoutActionConstants.class);
    public final static ExceptionActionConstants EXCEPTION_I18N = GWT.create(ExceptionActionConstants.class);
    public final static VariableActionConstants VARIABLE_I18N = GWT.create(VariableActionConstants.class);

    public final static CookieConstants COOKIE_I18N = GWT.create(CookieConstants.class);
    public final static HeaderConstants HEADER_I18N = GWT.create(HeaderConstants.class);

    public final static UploadEndpointConstants UPLOAD_ENDPOINT_I18N = GWT.create(UploadEndpointConstants.class);
}