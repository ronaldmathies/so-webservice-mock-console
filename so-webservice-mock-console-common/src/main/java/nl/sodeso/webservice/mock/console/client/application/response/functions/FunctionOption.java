package nl.sodeso.webservice.mock.console.client.application.response.functions;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.Function;

/**
 * @author Ronald Mathies
 */
public class FunctionOption extends DefaultOption {

    private Function function = null;

    public FunctionOption() {}

    public FunctionOption(Function function) {
        super(function.getName(), function.getDescription());

        this.function = function;
    }

    public Function getFunction() {
        return this.function;
    }

}
