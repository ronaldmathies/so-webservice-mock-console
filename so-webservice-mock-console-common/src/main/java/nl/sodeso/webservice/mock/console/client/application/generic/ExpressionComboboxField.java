package nl.sodeso.webservice.mock.console.client.application.generic;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class ExpressionComboboxField extends ComboboxField<ExpressionOption> {

    public ExpressionComboboxField(OptionType<ExpressionOption> optionValue, boolean allowNone) {
        super(optionValue);
        addItems(ExpressionOption.getOptions(allowNone));
    }
}
