package nl.sodeso.webservice.mock.console.client.application.response.conditions.parameter;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface ParameterConditionConstants extends Messages {

    @DefaultMessage("The parameter condition is used to limit this response for Requests by testing if the path in the URL contains values that match with the parameter values defined, parameter values are for example /company/10, whereas 10 would be the company id. The path within the condition would then be defined as /company/&#123;companyId&#125; and the parameter would then be defined as name = companyId, value = 10.")
    String parameterConditionDocumentation();

    @DefaultMessage("Parameter Condition")
    String parameterCondition();

    @DefaultMessage("Path")
    String path();

    @DefaultMessage("/company/'{'companyId'}'")
    String pathPlaceholder();

    @DefaultMessage("Name")
    String name();

    @DefaultMessage("companyId")
    String namePlaceholder();

    @DefaultMessage("Value")
    String value();

    @DefaultMessage("10")
    String valuePlaceholder();
}