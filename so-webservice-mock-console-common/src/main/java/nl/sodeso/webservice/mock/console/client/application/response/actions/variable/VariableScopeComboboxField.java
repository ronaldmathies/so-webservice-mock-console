package nl.sodeso.webservice.mock.console.client.application.response.actions.variable;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class VariableScopeComboboxField extends ComboboxField<VariableScopeOption> {

    private static final String KEY_VARIABLE_SCOPE = "variable-scope";

    public VariableScopeComboboxField(OptionType<VariableScopeOption> optionValue) {
        super(KEY_VARIABLE_SCOPE, optionValue);
        addItems(VariableScopeOption.getOptions());
    }
}
