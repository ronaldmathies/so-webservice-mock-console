package nl.sodeso.webservice.mock.console.client.application.response.conditions.method;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class MethodOption extends DefaultOption {

    private static final String KEY_GET = "GET";
    private static final String KEY_HEAD = "HEAD";
    private static final String KEY_POST = "POST";
    private static final String KEY_PUT = "PUT";
    private static final String KEY_DELETE = "DELETE";
    private static final String KEY_TRACE = "TRACE";
    private static final String KEY_CONNECT = "CONNECT";


    public static MethodOption GET = new MethodOption(KEY_GET, "GET");
    public static MethodOption HEAD = new MethodOption(KEY_HEAD, "HEAD");
    public static MethodOption POST = new MethodOption(KEY_POST, "POST");
    public static MethodOption PUT = new MethodOption(KEY_PUT, "PUT");
    public static MethodOption DELETE = new MethodOption(KEY_DELETE, "DELETE");
    public static MethodOption TRACE = new MethodOption(KEY_TRACE, "TRACE");
    public static MethodOption CONNECT = new MethodOption(KEY_CONNECT, "CONNECT");

    public MethodOption() {}

    public MethodOption(String code, String description) {
        super(code, description);
    }

    public static MethodOption[] getOptions() {
        return new MethodOption[] {
                GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        };
    }

    public static MethodOption getOption(String key) {
        for (MethodOption methodOption : getOptions()) {
            if (methodOption.getKey().equals(key)) {
                return methodOption;
            }
        }

        return null;
    }
}
