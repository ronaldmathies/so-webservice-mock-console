package nl.sodeso.webservice.mock.console.client.application.generic.header;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface HeaderConstants extends Messages {

    @DefaultMessage("Name")
    String name();

    @DefaultMessage("Value")
    String value();

}