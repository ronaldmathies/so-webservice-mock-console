package nl.sodeso.webservice.mock.console.client.application.download.endpoint;

import nl.sodeso.gwt.ui.client.form.table.summary.AbstractSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;

/**
 * @author Ronald Mathies
 */
public class DownloadEndpointSummaryItem extends AbstractSummaryItem {

    private boolean isSelected = false;

    private EndpointSummaryItem endpoint = null;

    private String exportFilename;

    public DownloadEndpointSummaryItem() {}

    public DownloadEndpointSummaryItem(EndpointSummaryItem endpoint) {
        this.endpoint = endpoint;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getExportFilename() {
        return exportFilename;
    }

    public void setExportFilename(String exportFilename) {
        this.exportFilename = exportFilename;
    }

    @Override
    public String getLabel() {
        return this.endpoint.getName();
    }

}
