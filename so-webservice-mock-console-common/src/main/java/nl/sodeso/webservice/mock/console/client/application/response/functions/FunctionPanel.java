package nl.sodeso.webservice.mock.console.client.application.response.functions;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.Function;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.Parameter;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.SampleParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Ronald Mathies
 */
public class FunctionPanel {

    private static final String KEY_FORM = "form";
    private static final String KEY_SAMPLE = "sample";

    private OptionType<SampleOption> sampleOption = new OptionType<>();

    private transient ComboboxField<SampleOption> sampleComboboxField = null;

    public Function function = null;

    private Map<String, StringType> values = new HashMap<>();
    private Map<String, TextField> fields = new HashMap<>();

    public FunctionPanel(Function function) {
        this.function = function;
    }

    public Widget toEditForm() {
        LegendPanel formPanel = new LegendPanel(KEY_FORM, "");

        EntryForm entryForm = new EntryForm(KEY_FORM);
        toForm(entryForm);

        formPanel.add(entryForm);

        LegendPanel samplePanel = new LegendPanel(KEY_SAMPLE, "");
        samplePanel.add(toExaplanation());

        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.addWidget(formPanel, 60, Style.Unit.PCT);
        horizontalPanel.addWidget(samplePanel, 40, Style.Unit.PCT);
        return horizontalPanel;
    }

    @SuppressWarnings("unchecked")
    private void toForm(EntryForm entryForm) {
        for (Parameter parameter : function.getParameters()) {
            values.put(parameter.getName(), new StringType());

            final TextField[] fieldContainer = new TextField[1];
            fieldContainer[0] = new TextField<>(parameter.getName(), values.get(parameter.getName()))
                .addValidationRule(new MandatoryValidationRule(parameter.getName(), ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return fieldContainer[0].getValue();
                    }
                });

            fields.put(parameter.getName(), fieldContainer[0]);
            entryForm.addEntry(new EntryWithLabelAndWidget(parameter.getName(), parameter.getName(), new InsertFunctionButton(fieldContainer[0])));
        }
    }

    private EntryForm toExaplanation() {
        EntryForm entryForm = new EntryForm(KEY_SAMPLE);

        sampleComboboxField = new ComboboxField<>(sampleOption);
        sampleComboboxField.addItems(getSampleOptions());

        sampleOption.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(sampleComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                showSample(sampleOption.getValue());
            }
        });

        entryForm.addEntry(new EntryWithDocumentation(KEY_FORM, function.getDocumentation() + "<br/><br/>Samples:<br/><br/>"));

        if (!function.getSamples().isEmpty()) {
            entryForm.addEntry(new EntryWithSingleWidget(KEY_SAMPLE, sampleComboboxField));
        }

        return entryForm;
    }

    private List<SampleOption> getSampleOptions() {
        return function.getSamples().stream().map(SampleOption::new).collect(Collectors.toCollection(ArrayList::new));
    }

    private void showSample(SampleOption sampleOption) {
        for (SampleParameter sampleParameter : sampleOption.getSample().getSampleParameters()) {
            values.get(sampleParameter.getName()).setValue(sampleParameter.getValue());
        }
    }

    public String toFunction() {
        Window.alert("A");
        String formattedFunction = function.getFormat();
        for (Map.Entry<String, StringType> entry : values.entrySet()) {
            formattedFunction = formattedFunction.replace(entry.getKey(), entry.getValue().getValue());
        }
        Window.alert("B");

        return formattedFunction;
    }


}
