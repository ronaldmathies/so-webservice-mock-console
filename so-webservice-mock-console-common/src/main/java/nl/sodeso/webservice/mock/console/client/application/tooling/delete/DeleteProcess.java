package nl.sodeso.webservice.mock.console.client.application.tooling.delete;

import nl.sodeso.gwt.ui.client.dialog.AlertQuestionPanel;

/**
 * @author Ronald Mathies
 */
public class DeleteProcess {

    private DeleteCallback callback;

    private AlertQuestionPanel questionPanel = null;

    public DeleteProcess(DeleteCallback callback) {
        this.callback = callback;
    }

    public void start() {
        prepareDelete();
    }

    private void prepareDelete() {
        questionPanel = new AlertQuestionPanel("Delete", "Are you sure you want to delete this entry permanently?", () -> delete());
        questionPanel.center(true, true);
    }

    private void delete() {
        questionPanel.close();
        callback.onSuccess();
    }
}
