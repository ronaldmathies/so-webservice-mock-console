package nl.sodeso.webservice.mock.console.client.application.response.conditions;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionConfig;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.body.BodyCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.cookie.CookieCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.header.HeaderCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.method.MethodCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.parameter.ParameterCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.path.PathCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.query.QueryCondition;

import java.util.function.Predicate;

/**
 * @author Ronald Mathies
 */

public class Conditions extends WidgetCollectionField<Condition> {

    public Conditions() {
        super(new WidgetCollectionConfig().setEnableSwitching(true));
    }

    @Override
    public WidgetSupplier<Condition> createWidgetSupplier() {
        return callback -> {
            final ChooseConditionQuestionPanel questionPanel = new ChooseConditionQuestionPanel(option -> {
                ConditionOption conditionOption = option.getValue();
                if (conditionOption.equals(ConditionOption.BODY)) {
                    callback.created(new BodyCondition());
                } else if (conditionOption.equals(ConditionOption.COOKIE)) {
                    callback.created(new CookieCondition());
                } else if (conditionOption.equals(ConditionOption.HEADER)) {
                    callback.created(new HeaderCondition());
                } else if (conditionOption.equals(ConditionOption.METHOD)) {
                    callback.created(new MethodCondition());
                } else if (conditionOption.equals(ConditionOption.PARAMETER)) {
                    callback.created(new ParameterCondition());
                } else if (conditionOption.equals(ConditionOption.PATH)) {
                    callback.created(new PathCondition());
                } else if (conditionOption.equals(ConditionOption.QUERY)) {
                    callback.created(new QueryCondition());

                }
            }, new ConditionFilter(this));
            questionPanel.center(true, true);
        };
    }

}
