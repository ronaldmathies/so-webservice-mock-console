package nl.sodeso.webservice.mock.console.client.application.response.conditions.parameter;

import nl.sodeso.gwt.ui.client.form.*;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Condition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.ConditionOption;

import static nl.sodeso.webservice.mock.console.client.application.Resources.*;

/**
 * @author Ronald Mathies
 */
public class ParameterCondition extends Condition {

    private static final String KEY_PARAMETER_CONDITION = "parameter-condition";
    private static final String KEY_PATH = "path";
    private static final String KEY_PARAMETER_VALUES = "parameter-values";

    private StringType path = new StringType();
    private ParameterValues parameterValues = new ParameterValues();

    private transient EntryWithSingleWidget entry = null;
    private transient TextField pathField = null;

    public ParameterCondition() {
    }

    public ConditionOption getConditionOption() {
        return ConditionOption.PARAMETER;
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        pathField = new TextField<>(KEY_PATH, this.path)
            .setPlaceholder(PARAMETER_I18N.pathPlaceholder());
        pathField.addValidationRule(
            new MandatoryValidationRule(KEY_PATH, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return pathField.getValue();
                }
            }
        );

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_PARAMETER_CONDITION, PARAMETER_I18N.parameterConditionDocumentation()),
                new EntryWithLabelAndWidget(KEY_PATH, PARAMETER_I18N.path(), pathField),
                new EntryWithContainer(KEY_PARAMETER_VALUES, parameterValues.toEditForm())
            );

        LegendPanel legendPanel = new LegendPanel(null, PARAMETER_I18N.parameterCondition());
        legendPanel.add(form);

        entry = new EntryWithSingleWidget(KEY_PARAMETER_CONDITION, legendPanel);
        return entry;
    }

    public StringType getPath() {
        return path;
    }

    public void setPath(StringType path) {
        this.path = path;
    }

    public ParameterValues getParameterValues() {
        return parameterValues;
    }

    public void setParameterValues(ParameterValues parameterValues) {
        this.parameterValues = parameterValues;
    }
}