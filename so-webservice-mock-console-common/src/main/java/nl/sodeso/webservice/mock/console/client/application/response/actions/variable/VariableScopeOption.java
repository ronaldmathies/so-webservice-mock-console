package nl.sodeso.webservice.mock.console.client.application.response.actions.variable;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

/**
 * @author Ronald Mathies
 */
public class VariableScopeOption extends DefaultOption {

    private static final String KEY_REQUEST = "request";
    private static final String KEY_SESSION = "session";
    private static final String KEY_GLOBAL = "global";

    public static VariableScopeOption REQUEST = new VariableScopeOption(KEY_REQUEST, "Request");
    public static VariableScopeOption SESSION = new VariableScopeOption(KEY_SESSION, "Session");
    public static VariableScopeOption GLOBAL = new VariableScopeOption(KEY_GLOBAL, "Global");

    public VariableScopeOption() {}

    public VariableScopeOption(String code, String description) {
        super(code, description);
    }

    public static VariableScopeOption[] getOptions() {
        return new VariableScopeOption[] {
                REQUEST, SESSION, GLOBAL
        };
    }

    public static VariableScopeOption getOption(String key) {
        switch (key) {
            case KEY_REQUEST:
                return REQUEST;
            case KEY_SESSION:
                return SESSION;
            case KEY_GLOBAL:
                return GLOBAL;
        }

        return null;
    }



}
