package nl.sodeso.webservice.mock.console.client.application.response.functions;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface FunctionConstants extends Messages {

    @DefaultMessage("Select a function  type of function to add, ")
    String functionDocumentation();

    @DefaultMessage("Add Function")
    String title();

    @DefaultMessage("Function Group")
    String functionGroup();

    @DefaultMessage("Function")
    String function();


}