package nl.sodeso.webservice.mock.console.client.application.response.actions;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class ActionComboboxField extends ComboboxField<ActionOption> {

    private static final String KEY_ACTION = "action";

    public ActionComboboxField(OptionType<ActionOption> optionValue) {
        super(KEY_ACTION, optionValue);
        addItems(ActionOption.getOptions());
    }
}
