package nl.sodeso.webservice.mock.console.server.endpoint.properties;

import nl.sodeso.commons.properties.annotations.EnvironmentResource;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.environment.EnvironmentContainer;
import nl.sodeso.webservice.mock.console.client.Constants;

/**
 * @author Ronald Mathies
 */
@Resource(
    domain = Constants.MODULE_ID
)
@EnvironmentResource
public class ModulePropertiesContainer extends EnvironmentContainer {

    public static final String DISCOVERY_SERVICE_ID = "SERVICE_ID";

    public static final String DISCOVERY_MULTICAST_ENABLED = "MULTICAST_ENABLED";
    public static final String DISCOVERY_MULTICAST_GROUP = "MULTICAST_GROUP";
    public static final String DISCOVERY_MULTICAST_PORT = "MULTICAST_PORT";

    public static final String DISCOVERY_BROADCAST_ENABLED = "BROADCAST_ENABLED";
    public static final String DISCOVERY_BROADCAST_ADDRESS = "BROADCAST_ADDRESS";
    public static final String DISCOVERY_BROADCAST_PORT = "BROADCAST_PORT";


}
