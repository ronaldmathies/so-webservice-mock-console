package nl.sodeso.webservice.mock.console.server.servlet;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.gwt.ui.client.util.ContentType;
import nl.sodeso.gwt.ui.server.servlet.AbstractFileUploadServlet;
import nl.sodeso.gwt.ui.server.servlet.UploadFile;
import nl.sodeso.commons.storage.FileEntry;
import nl.sodeso.commons.storage.StorageController;
import nl.sodeso.commons.storage.rules.CreatedExpirationRule;
import nl.sodeso.commons.storage.rules.ExpirationRule;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
@WebServlet(
        name = "File Upload Servlet",
        urlPatterns = {"*.upload"}
)
@MultipartConfig(
        fileSizeThreshold=1024*1024*10,
        maxFileSize=1024*1024*50,
        maxRequestSize=1024*1024*100)
public class FileUploadServlet extends AbstractFileUploadServlet {

    private static final Logger log = Logger.getLogger(FileUploadServlet.class.getName());

    @Override
    public void upload(List<UploadFile> uploadedFiles, HttpServletResponse response) throws ServletException {
        try {

            UploadFile uploadFile = null;
            if (!uploadedFiles.isEmpty()) {
                uploadFile = uploadedFiles.get(0);
            }

            log.info(String.format("Incomming file upload '%s'.", uploadFile.getFilename()));

            String uuid = UUID.randomUUID().toString();
            File file = new File(FileUtil.getSystemTempFolderAsFile(), uuid.concat(".upl"));
            if (uploadFile.getTempFile().renameTo(file)) {
                log.info(String.format("Incomming file upload stored as '%s'.", file.getName()));
                StorageController.getInstance().add(new FileEntry(uuid, file, new CreatedExpirationRule(ExpirationRule.THIRTY_MINUTES)));
            }

            response.setContentType(ContentType.TEXT_HTML.getContentType());
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.print("{ \"uuid\": \"" + uuid + "\" }");
        } catch (IOException e) {
            throw new ServletException("Failed to store file.", e);
        }
    }
}
