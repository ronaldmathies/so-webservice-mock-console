package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.gwt.ui.client.types.LongType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.request.Request;
import nl.sodeso.webservice.mock.console.client.application.request.RequestSummaryItem;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class RequestConvertor {

    private static final Logger LOG = Logger.getLogger(RequestConvertor.class.getName());

    public static Request toClient(JsRequest jsRequest) {
        Request request = new Request();

        request.setId(new StringType(jsRequest.getId()));
        request.setTime(new LongType(jsRequest.getTime()));
        request.setMethod(new StringType(jsRequest.getMethod()));
        request.setRequestUrl(new StringType(jsRequest.getRequestUrl()));
        request.getQueries().getWidgets().addAll(QueryConvertor.toClient(jsRequest.getJsQueries()));
        request.getHeaders().getWidgets().addAll(HeaderConvertor.toClient(jsRequest.getJsHeaders()));
        request.getCookies().getWidgets().addAll(CookieConvertor.toClient(jsRequest.getJsCookies()));

        return request;
    }

    public static ArrayList<RequestSummaryItem> toClientSummaries(List<JsRequest> jsRequests) {
        ArrayList<RequestSummaryItem> requests = new ArrayList<>();

        for (JsRequest jsRequest : jsRequests) {
            requests.add(toClientSummary(jsRequest));
        }

        return requests;
    }

    public static RequestSummaryItem toClientSummary(JsRequest jsRequest) {
        RequestSummaryItem summaryItem = new RequestSummaryItem();
        summaryItem.setId(jsRequest.getId());
        return summaryItem;
    }

}
