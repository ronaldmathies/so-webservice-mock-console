package nl.sodeso.webservice.mock.console.server.endpoint.download;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.webservice.mock.console.client.application.download.endpoint.DownloadEndpointWizardContext;
import nl.sodeso.webservice.mock.console.client.application.download.rpc.DownloadRpc;
import nl.sodeso.webservice.mock.console.client.application.download.endpoint.DownloadEndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.commons.storage.FileEntry;
import nl.sodeso.commons.storage.StorageController;
import nl.sodeso.commons.storage.rules.CreatedExpirationRule;
import nl.sodeso.commons.storage.rules.ExpirationRule;
import org.apache.commons.io.IOUtils;

import javax.servlet.annotation.WebServlet;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.webservicemock-export")
public class DownloadEndpoint extends XsrfProtectedServiceServlet implements DownloadRpc {

    public ArrayList<DownloadEndpointSummaryItem> prepareEndpointExport(ServiceOption serviceOption, DownloadEndpointWizardContext downloadEndpointWizardContext) {

        for (DownloadEndpointSummaryItem downloadEndpointSummaryItem : downloadEndpointWizardContext.getEndpointSummaryItems()) {
            if (downloadEndpointSummaryItem.isSelected()) {

                InputStream inputStream = null;
                if (downloadEndpointWizardContext.isIncludeResponses()) {
                    if (downloadEndpointWizardContext.isIncludeRequests()) {
                        inputStream = ExportRestClient.exportEndpointWithResponsesAndRequests(serviceOption.getIpAddress(), serviceOption.getPort(), downloadEndpointSummaryItem.getLabel());
                    } else {
                        inputStream = ExportRestClient.exportEndpointWithResponses(serviceOption.getIpAddress(), serviceOption.getPort(), downloadEndpointSummaryItem.getLabel());
                    }
                } else if (downloadEndpointWizardContext.isIncludeRequests()) {
                    inputStream = ExportRestClient.exportEndpointWithRequests(serviceOption.getIpAddress(), serviceOption.getPort(), downloadEndpointSummaryItem.getLabel());
                } else {
                    inputStream = ExportRestClient.exportEndpoint(serviceOption.getIpAddress(), serviceOption.getPort(), downloadEndpointSummaryItem.getLabel());
                }

                if (inputStream != null) {
                    savePreparedExport(downloadEndpointSummaryItem, inputStream);
                }
            }
        }

        return downloadEndpointWizardContext.getEndpointSummaryItems();
    }

    private void savePreparedExport(DownloadEndpointSummaryItem downloadEndpointSummaryItem, InputStream inputStream) {
        File export = null;
        try {

            String prefix = FileUtil.removeIllegalChars(downloadEndpointSummaryItem.getLabel());
            String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());

            export = File.createTempFile(prefix + "-" + timestamp + "-", ".json", FileUtil.getSystemTempFolderAsFile());
            try (FileOutputStream fos = new FileOutputStream(export)) {

                IOUtils.copy(inputStream, fos);
                downloadEndpointSummaryItem.setExportFilename(export.getName());
            }

        } catch (IOException e) {
            // Skip file.
        } finally {
            if (export.exists()) {
                StorageController.getInstance().add(new FileEntry(UUID.randomUUID().toString(), export, new CreatedExpirationRule(ExpirationRule.THIRTY_MINUTES)));
            }
        }
    }


}
