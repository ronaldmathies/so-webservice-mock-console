package nl.sodeso.webservice.mock.console.client;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.controllers.navigation.NavigationController;
import nl.sodeso.gwt.ui.client.link.LinkFactory;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.rpc.DefaultRunAsyncCallback;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.webservice.mock.console.client.application.StartMenu;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceSwitchButton;


/**
 * The application entry point is the main method of a GWT application, it should handle
 * the boodstrapping of the application.
 *
 * @author Ronald Mathies
 */
public class WebServiceMockConsoleEntryPoint extends ModuleEntryPoint {

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    @Override
    public String getModuleName() {
        return "WS-Mock Console";
    }

    @Override
    public Icon getModuleIcon() {
        return Icon.Users;
    }


    @Override
    public void onBeforeModuleLoad(BeforeModuleLoadFinishedTrigger trigger) {
        trigger.fire();
    }

    @Override
    public void onAfterModuleLoad(AfterModuleLoadFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback(){
            @Override
            public void success(){
                StartMenu.init();

                if (LinkFactory.getLink(getWelcomeToken()) == null) {
                    LinkFactory.addLink(new WelcomeLink());
                }

                NavigationController.instance().addButtons(Align.LEFT,
                        new ServiceSwitchButton());

                trigger.fire();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWelcomeToken() {
        return WelcomeLink.TOKEN;
    }

    /**
     * {@inheritDoc}
     */
    public void activateModule(ActivateFinishedTrigger trigger) {
        trigger.fire();
    }

    /**
     * {@inheritDoc}
     */
    public void suspendModule(SuspendFinishedTrigger trigger) {
        trigger.fire();
    }
}
