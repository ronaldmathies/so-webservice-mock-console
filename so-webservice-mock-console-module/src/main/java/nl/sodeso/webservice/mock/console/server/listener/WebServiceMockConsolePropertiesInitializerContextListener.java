package nl.sodeso.webservice.mock.console.server.listener;

import nl.sodeso.gwt.ui.server.listener.AbstractModulePropertiesInitializerContextListener;
import nl.sodeso.webservice.mock.console.client.Constants;
import nl.sodeso.webservice.mock.console.server.endpoint.properties.WebServiceMockConsoleServerModuleProperties;

/**
 * @author Ronald Mathies
 */
public class WebServiceMockConsolePropertiesInitializerContextListener extends AbstractModulePropertiesInitializerContextListener<WebServiceMockConsoleServerModuleProperties> {

    @Override
    public Class<WebServiceMockConsoleServerModuleProperties> getServerModulePropertiesClass() {
        return WebServiceMockConsoleServerModuleProperties.class;
    }

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }
}
