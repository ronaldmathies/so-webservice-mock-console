package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionOption;
import nl.sodeso.webservice.mock.console.client.application.generic.RequestComponentOption;
import nl.sodeso.webservice.mock.console.client.application.logging.Logging;
import nl.sodeso.webservice.mock.model.endpoint.settings.JsLogging;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.*;
import nl.sodeso.webservice.mock.model.generic.JsPath;

/**
 * @author Ronald Mathies
 */
public class LoggingConvertor {

    public static JsLogging toService(Logging logging) {
        JsLogging jsLogging = new JsLogging();

        jsLogging.setEnableLogging(logging.getEnableLogging().getValue());
        jsLogging.setNumberOfRequestsToStore(logging.getNumberOfRequestsToStore().getValue());

        JsExpression jsExpression = null;
        RequestComponentOption requestComponentOption = logging.getRequestComponentOption().getValue();
        if (requestComponentOption.equals(RequestComponentOption.BODY)) {
            JsBodyExpression jsBodyExpression = new JsBodyExpression();
            toService(logging, jsBodyExpression);
            jsExpression = jsBodyExpression;
        } else if (requestComponentOption.equals(RequestComponentOption.COOKIE)) {
            JsCookieExpression jsCookieExpression = new JsCookieExpression();
            toService(logging, jsCookieExpression);
            jsCookieExpression.setName(logging.getName().getValue());
            jsExpression = jsCookieExpression;
        } else if (requestComponentOption.equals(RequestComponentOption.HEADER)) {
            JsHeaderExpression jsHeaderExpression = new JsHeaderExpression();
            toService(logging, jsHeaderExpression);
            jsHeaderExpression.setName(logging.getName().getValue());
            jsExpression = jsHeaderExpression;
        } else if (requestComponentOption.equals(RequestComponentOption.QUERY)) {
            JsQueryExpression jsQueryExpression = new JsQueryExpression();
            toService(logging, jsQueryExpression);
            jsQueryExpression.setName(logging.getName().getValue());
            jsExpression = jsQueryExpression;
        } else if (requestComponentOption.equals(RequestComponentOption.UUID)) {
            JsUuidExpression jsUuidExpression = new JsUuidExpression();
            jsExpression = jsUuidExpression;
        }

        jsLogging.setJsExpression(jsExpression);

        return jsLogging;
    }

    private static void toService(Logging logging, JsPath jsPath) {
        OptionType<ExpressionOption> expressionOption = logging.getExpressionOption();
        String expression = logging.getExpression().getValue();

        if (expressionOption.getValue().equals(ExpressionOption.NONE)) {
        } else if (expressionOption.getValue().equals(ExpressionOption.JSON_PATH)) {
            jsPath.setJsonpath(expression);
        } else if (expressionOption.getValue().equals(ExpressionOption.REGEXP)) {
            jsPath.setRegexp(expression);
        } else if (expressionOption.getValue().equals(ExpressionOption.XPATH)) {
            jsPath.setXpath(expression);
        }
    }

    public static Logging toClient(JsLogging jsLogging) {
        Logging logging = new Logging();

        logging.setEnableLogging(new BooleanType(jsLogging.isEnableLogging()));
        logging.setNumberOfRequestsToStore(new IntType(jsLogging.getNumberOfRequestsToStore()));

        JsExpression jsExpression = jsLogging.getJsExpression();
        if (jsExpression instanceof JsBodyExpression) {
            logging.setRequestComponentOption(new OptionType<>(RequestComponentOption.BODY));

            toClient((JsBodyExpression) jsExpression, logging);
        } else if (jsExpression instanceof JsCookieExpression) {
            logging.setRequestComponentOption(new OptionType<>(RequestComponentOption.COOKIE));
            JsCookieExpression jsCookieExpression = (JsCookieExpression) jsExpression;
            logging.setName(new StringType(jsCookieExpression.getName()));
            toClient(jsCookieExpression, logging);
        } else if (jsExpression instanceof JsHeaderExpression) {
            logging.setRequestComponentOption(new OptionType<>(RequestComponentOption.HEADER));
            JsHeaderExpression jsHeaderExpression = (JsHeaderExpression) jsExpression;
            logging.setName(new StringType(jsHeaderExpression.getName()));
            toClient(jsHeaderExpression, logging);
        } else if (jsExpression instanceof JsQueryExpression) {
            logging.setRequestComponentOption(new OptionType<>(RequestComponentOption.QUERY));
            JsQueryExpression jsQueryExpression = (JsQueryExpression) jsExpression;
            logging.setName(new StringType(jsQueryExpression.getName()));
            toClient(jsQueryExpression, logging);
        } else if (jsExpression instanceof JsUuidExpression) {
            logging.setRequestComponentOption(new OptionType<>(RequestComponentOption.UUID));
        }

        return logging;
    }

    public static void toClient(JsPath jsPath, Logging logging) {
        if (jsPath.getJsonpath() != null) {
            logging.setExpressionOption(new OptionType<>(ExpressionOption.JSON_PATH));
            logging.setExpression(new StringType(jsPath.getJsonpath()));
        } else if (jsPath.getXpath() != null) {
            logging.setExpressionOption(new OptionType<>(ExpressionOption.XPATH));
            logging.setExpression(new StringType(jsPath.getXpath()));
        } else if (jsPath.getRegexp() != null) {
            logging.setExpressionOption(new OptionType<>(ExpressionOption.REGEXP));
            logging.setExpression(new StringType(jsPath.getRegexp()));
        } else {
            logging.setExpressionOption(new OptionType<>(ExpressionOption.NONE));
            logging.setExpression(new StringType(null));
        }
    }

}