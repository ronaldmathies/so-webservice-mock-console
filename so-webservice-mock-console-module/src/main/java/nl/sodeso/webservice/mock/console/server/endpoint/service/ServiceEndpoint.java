package nl.sodeso.webservice.mock.console.server.endpoint.service;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.commons.network.discovery.DiscoveredService;
import nl.sodeso.commons.network.discovery.DiscoveredServices;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.console.client.application.service.rpc.ServiceRpc;
import nl.sodeso.webservice.mock.console.server.endpoint.convertor.ServiceConvertor;
import nl.sodeso.webservice.mock.model.service.JsService;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.webservicemock-service")
public class ServiceEndpoint extends XsrfProtectedServiceServlet implements ServiceRpc {

    public ArrayList<ServiceOption> asOptions() {
        ArrayList<ServiceOption> services = new ArrayList<>();

        List<DiscoveredService> discoveredServices = DiscoveredServices.getInstance().getServices();
        for (DiscoveredService service : discoveredServices) {

            JsService jsService = ServiceRestClient.getVersion(service.getInetAddress().getHostAddress(), service.getPort());
            if (jsService != null) {
                services.add(ServiceConvertor.toOption(jsService, service));
            }

        }

        return services;
    }

}
