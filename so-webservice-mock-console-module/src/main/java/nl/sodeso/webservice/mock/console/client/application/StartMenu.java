package nl.sodeso.webservice.mock.console.client.application;

import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointMenuItem;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseMenuItem;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceManager;

/**
 * @author Ronald Mathies
 */
public class StartMenu {

    private StartMenu() {}

    public static void init() {
        MenuController.instance().setFullwidth(true);

        EndpointMenuItem endpointMenuItem = new EndpointMenuItem();
        endpointMenuItem.setEnabled(false);

        ResponseMenuItem responseMenuItem = new ResponseMenuItem();
        responseMenuItem.setEnabled(false);

        MenuController.instance().addMenuItems(
                endpointMenuItem,
                responseMenuItem);

        ServiceManager.instance().addServiceChangedEventHandler(event -> {
            endpointMenuItem.setEnabled(event.getService() != null);
            responseMenuItem.setEnabled(event.getService() != null);
        });
    }

}
