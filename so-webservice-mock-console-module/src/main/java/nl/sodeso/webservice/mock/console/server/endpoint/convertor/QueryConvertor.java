package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.generic.query.Query;
import nl.sodeso.webservice.mock.model.generic.JsQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class QueryConvertor {

    public static List<Query> toClient(List<JsQuery> jsQueries) {
        List<Query> queries = new ArrayList<>();

        for (JsQuery jsQuery : jsQueries) {
            Query query = new Query();
            query.setName(new StringType(jsQuery.getName()));
            query.setValue(new StringType(jsQuery.getValue()));
            queries.add(query);
        }

        return queries;
    }

}
