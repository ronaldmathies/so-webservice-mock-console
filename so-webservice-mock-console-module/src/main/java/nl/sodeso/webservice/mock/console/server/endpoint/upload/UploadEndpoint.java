package nl.sodeso.webservice.mock.console.server.endpoint.upload;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.commons.storage.FileEntry;
import nl.sodeso.commons.storage.StorageController;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadEndpointWizardContext;
import nl.sodeso.webservice.mock.console.client.application.upload.endpoint.UploadedEndpointDetails;
import nl.sodeso.webservice.mock.console.client.application.upload.rpc.UploadRpc;
import nl.sodeso.webservice.mock.console.server.endpoint.endpoint.EndpointRestClient;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;

import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.webservicemock-import")
public class UploadEndpoint extends XsrfProtectedServiceServlet implements UploadRpc {

    private static final Logger log = Logger.getLogger(UploadEndpoint.class.getName());

    public UploadedEndpointDetails prepareEndpointImport(ServiceOption serviceOption, String uuid) {

        Optional<FileEntry> fileEntryOptional = StorageController.getInstance().get(uuid);
        if (fileEntryOptional.isPresent()) {
            FileEntry fileEntry = fileEntryOptional.get();

            try {
                ObjectMapper mapper = new ObjectMapper();
                JsEndpoint jsEndpoint = mapper.readValue(fileEntry.getFile(), JsEndpoint.class);

                UploadedEndpointDetails uploadedEndpointDetails = new UploadedEndpointDetails(jsEndpoint.getName(), jsEndpoint.getPath(), uuid);
                boolean containsRequests = jsEndpoint.getJsResponses().stream().anyMatch(jsResponse -> !jsResponse.getJsRequests().isEmpty()) ||
                        !jsEndpoint.getJsRequests().isEmpty();

                uploadedEndpointDetails.setContainsRequests(containsRequests);
                uploadedEndpointDetails.setContainsResponses(!jsEndpoint.getJsResponses().isEmpty());

                return uploadedEndpointDetails;
            } catch (IOException e) {
                log.severe(String.format("Failed to load uploaded import file: ", fileEntry.getFile().getName()));
            }
        }

        return null;

    }

    @Override
    public Boolean finishEndpointImport(ServiceOption serviceOption, UploadEndpointWizardContext context) {
        Optional<FileEntry> fileEntryOptional = StorageController.getInstance().get(context.getUploadedEndpointDetails().getUuid());
        if (fileEntryOptional.isPresent()) {
            FileEntry fileEntry = fileEntryOptional.get();

            try {
                ObjectMapper mapper = new ObjectMapper();
                JsEndpoint jsEndpoint = mapper.readValue(fileEntry.getFile(), JsEndpoint.class);
                jsEndpoint.setName(context.getLabel());
                jsEndpoint.setPath(context.getPath());

                if (!context.isIncludeRequests()) {
                    jsEndpoint.clearJsRequests();
                    jsEndpoint.getJsResponses().forEach(jsResponse -> jsResponse.clearJsRequests());
                }

                if (!context.isIncludeResponses()) {
                    jsEndpoint.clearJsResponses();
                }

                EndpointRestClient.addEndpoint(serviceOption.getIpAddress(), serviceOption.getPort(), jsEndpoint);
                return true;
            } catch (IOException e) {
                log.severe(String.format("Failed to import file: ", fileEntry.getFile().getName()));
            }
        }

        return false;
    }
}
