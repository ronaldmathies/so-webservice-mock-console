package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.commons.general.Base64Util;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.upload.FileUpload;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.generic.ContentTypeOption;
import nl.sodeso.webservice.mock.console.client.application.generic.HttpStatusOption;
import nl.sodeso.webservice.mock.console.client.application.response.Response;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.response.type.*;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.model.endpoint.response.type.*;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class ResponseConvertor {

    private static final Logger LOG = Logger.getLogger(ResponseConvertor.class.getName());

    public static JsResponse toService(Response response) {
        JsResponse mResponse = new JsResponse();

        mResponse.setName(response.getName().getValue());
        mResponse.getJsSettings().setDeleteAfterUse(response.getIsDeleteAfterUse().getValue());
        mResponse.setJsActions(ActionConvertor.toService(response.getActions().getWidgets()));
        mResponse.setJsConditions(ConditionConvertor.toService(response.getConditions().getWidgets()));

        JsResponseType jsResponseType = null;
        ResponseTypeOption responseTypeOption = response.getResponseTypeOption().getValue();
        if (responseTypeOption.isMessage()) {
            jsResponseType = toService((MessageResponseType) response.getResponseType());
        } else if (responseTypeOption.isRedirect()) {
            jsResponseType = toService((RedirectResponseType) response.getResponseType());
        } else if (responseTypeOption.isProxy()) {
            jsResponseType = toService((ProxyResponseType) response.getResponseType());
        } else if (responseTypeOption.isDownload()) {
            jsResponseType = toService((DownloadResponseType) response.getResponseType());
        }

        jsResponseType.setHttpStatus(Integer.parseInt(response.getHttpStatusOption().getValue().getCode()));
        jsResponseType.getJsHeaders().addAll(HeaderConvertor.toService(response.getHeaders().getWidgets()));
        jsResponseType.getJsCookies().addAll(CookieConvertor.toService(response.getCookies().getWidgets()));
        mResponse.setJsResponseType(jsResponseType);

        mResponse.setJsLogging(LoggingConvertor.toService(response.getLogging()));

        return mResponse;
    }

    private static JsMessageResponseType toService(MessageResponseType messageResponseType) {
        JsMessageResponseType jsMessageResponseType = new JsMessageResponseType();
        jsMessageResponseType.setBody(messageResponseType.getBody().getValue());
        String value = messageResponseType.getContentTypeOption().getValue().getCode();
        if (messageResponseType.getContentTypeOption().getValue().isOther()) {
            value = messageResponseType.getOtherContentTypeOption().getValue();
        }
        jsMessageResponseType.setContentType(value);
        return jsMessageResponseType;
    }

    private static JsRedirectResponseType toService(RedirectResponseType redirectResponseType) {
        JsRedirectResponseType jsRedirectResponseType = new JsRedirectResponseType();
        jsRedirectResponseType.setUrl(redirectResponseType.getRedirectUrl().getValue());
        return jsRedirectResponseType;
    }

    private static JsProxyResponseType toService(ProxyResponseType proxyResponseType) {
        JsProxyResponseType jsProxyResponseType = new JsProxyResponseType();
        jsProxyResponseType.setDomain(proxyResponseType.getDomain().getValue());
        jsProxyResponseType.setIgnoreHeaders(proxyResponseType.getIgnoreHeaders().getValue());
        jsProxyResponseType.setIgnoreCookies(proxyResponseType.getIgnoreCookies().getValue());
        jsProxyResponseType.setConnectionTimeoutInSeconds(proxyResponseType.getConnectionTimeoutInSeconds().getValue());
        jsProxyResponseType.setReadTimeoutInSeconds(proxyResponseType.getReadTimeoutInSeconds().getValue());
        return jsProxyResponseType;
    }

    private static JsDownloadResponseType toService(DownloadResponseType downloadResponseType) {
        JsDownloadResponseType jsDownloadResponseType = new JsDownloadResponseType();
        jsDownloadResponseType.setFilename(downloadResponseType.getFilename().getValue());

        String value = downloadResponseType.getContentTypeOption().getValue().getCode();
        if (downloadResponseType.getContentTypeOption().getValue().isOther()) {
            value = downloadResponseType.getOtherContentTypeOption().getValue();
        }
        jsDownloadResponseType.setContentType(value);

        String uuid = downloadResponseType.getFileUpload().getValue().getUuid();
        if (uuid != null && !uuid.isEmpty()) {

            try {
                File file = new File(FileUtil.getSystemTempFolderAsFile(), uuid.concat(".upl"));
                if (file.exists()) {
                    StringWriter base64EncodedFileContents = Base64Util.encode(new FileInputStream(file));
                    jsDownloadResponseType.setData(base64EncodedFileContents.toString());
                    jsDownloadResponseType.setSize(file.length());
                }
            } catch (FileNotFoundException e) {
                // Should not occure.
            }
        }
        return jsDownloadResponseType;
    }

    public static Response toClient(JsResponse jsResponse) {
        Response response = new Response();

        response.setIsExisting(true);
        response.setName(new StringType(jsResponse.getName()));
        response.setIsDeleteAfterUse(new BooleanType(jsResponse.getJsSettings().isDeleteAfterUse()));
        response.getActions().getWidgets().addAll(ActionConvertor.toClient(jsResponse.getJsActions()));
        response.getConditions().getWidgets().addAll(ConditionConvertor.toClient(jsResponse.getJsConditions()));

        if (jsResponse.getJsResponseType() instanceof JsMessageResponseType) {
            response.setResponseTypeOption(new OptionType<>(ResponseTypeOption.MESSAGE));
            response.setResponseType(toClient((JsMessageResponseType)jsResponse.getJsResponseType()));
        } else if (jsResponse.getJsResponseType() instanceof JsRedirectResponseType) {
            response.setResponseTypeOption(new OptionType<>(ResponseTypeOption.REDIRECT));
            response.setResponseType(toClient((JsRedirectResponseType)jsResponse.getJsResponseType()));
        } else if (jsResponse.getJsResponseType() instanceof JsProxyResponseType) {
            response.setResponseTypeOption(new OptionType<>(ResponseTypeOption.PROXY));
            response.setResponseType(toClient((JsProxyResponseType)jsResponse.getJsResponseType()));
        } else if (jsResponse.getJsResponseType() instanceof JsDownloadResponseType) {
            response.setResponseTypeOption(new OptionType<>(ResponseTypeOption.DOWNLOAD));
            response.setResponseType(toClient((JsDownloadResponseType)jsResponse.getJsResponseType()));
        }

        response.setHttpStatusOption(new OptionType<>(HttpStatusOption.fromValue(String.valueOf((jsResponse.getJsResponseType()).getHttpStatus()))));
        response.getHeaders().getWidgets().addAll(HeaderConvertor.toClient(jsResponse.getJsResponseType().getJsHeaders()));
        response.getCookies().getWidgets().addAll(CookieConvertor.toClient(jsResponse.getJsResponseType().getJsCookies()));
        response.setLogging(LoggingConvertor.toClient(jsResponse.getJsLogging()));

        return response;
    }

    private static MessageResponseType toClient(JsMessageResponseType jsMessageResponseType) {
        MessageResponseType messageResponseType = new MessageResponseType();
        messageResponseType.setBody(new StringType(jsMessageResponseType.getBody()));


        ContentTypeOption contentTypeOption = ContentTypeOption.fromValue(jsMessageResponseType.getContentType());
        if (contentTypeOption == null) {
            contentTypeOption = ContentTypeOption.OTHER;
            messageResponseType.setOtherContentTypeOption(new StringType(jsMessageResponseType.getContentType()));
        } else {
            messageResponseType.setOtherContentTypeOption(new StringType());
        }

        messageResponseType.setContentTypeOption(new OptionType<>(contentTypeOption));
        return messageResponseType;
    }

    private static RedirectResponseType toClient(JsRedirectResponseType jsRedirectResponseType) {
        RedirectResponseType redirectResponseType = new RedirectResponseType();
        redirectResponseType.setRedirectUrl(new StringType(jsRedirectResponseType.getUrl()));
        return redirectResponseType;
    }

    private static ProxyResponseType toClient(JsProxyResponseType jsProxyResponseType) {
        ProxyResponseType proxyResponseType = new ProxyResponseType();
        proxyResponseType.setDomain(new StringType(jsProxyResponseType.getDomain()));
        proxyResponseType.setIgnoreCookies(new BooleanType(jsProxyResponseType.isIgnoreCookies()));
        proxyResponseType.setIgnoreHeaders(new BooleanType(jsProxyResponseType.isIgnoreHeaders()));
        proxyResponseType.setConnectionTimeoutInSeconds(new IntType(jsProxyResponseType.getConnectionTimeoutInSeconds()));
        proxyResponseType.setReadTimeoutInSeconds(new IntType(jsProxyResponseType.getReadTimeoutInSeconds()));
        return proxyResponseType;
    }

    private static DownloadResponseType toClient(JsDownloadResponseType jsDownloadResponseType) {
        DownloadResponseType downloadResponseType = new DownloadResponseType();
        downloadResponseType.setFilename(new StringType(jsDownloadResponseType.getFilename()));

        ContentTypeOption contentTypeOption = ContentTypeOption.fromValue(jsDownloadResponseType.getContentType());
        if (contentTypeOption == null) {
            contentTypeOption = ContentTypeOption.OTHER;
            downloadResponseType.setOtherContentTypeOption(new StringType(jsDownloadResponseType.getContentType()));
        } else {
            downloadResponseType.setOtherContentTypeOption(new StringType());
        }
        downloadResponseType.setContentTypeOption(new OptionType<>(contentTypeOption));


        FileUpload fileUpload = new FileUpload(UUID.randomUUID().toString(), jsDownloadResponseType.getFilename(), jsDownloadResponseType.getSize());

        try {
            File file = new File(FileUtil.getSystemTempFolderAsFile(), fileUpload.getUuid().concat(".upl"));
            if (file.exists()) {
                if (!file.delete()) {
                    LOG.log(Level.WARNING, "Unable to delete file: '%s'", file);
                }
            }

            IOUtils.copy(new Base64InputStream(new ByteArrayInputStream(jsDownloadResponseType.getData().getBytes("UTF-8")), false), new FileOutputStream(file));
        } catch (IOException e) {
            // We do nothing.
        } finally {
            // We do nothing.
        }


        downloadResponseType.setFileUpload(new FileUploadType(fileUpload));
        return downloadResponseType;
    }

    public static ArrayList<ResponseSummaryItem> toClientSummaries(List<JsResponse> jsResponses) {
        ArrayList<ResponseSummaryItem> responses = new ArrayList<>();

        for (JsResponse jsResponse : jsResponses) {
            responses.add(toClientSummary(jsResponse));
        }

        return responses;
    }

    public static ResponseSummaryItem toClientSummary(JsResponse jsResponse) {
        ResponseSummaryItem summaryItem = new ResponseSummaryItem();
        summaryItem.setResponseName(jsResponse.getName());
        return summaryItem;
    }

    public static ArrayList<DefaultOption> toOptions(List<JsResponse> jsResponses) {
        ArrayList<DefaultOption> options = new ArrayList<>();
        for (JsResponse jsResponse : jsResponses) {
            options.add(new DefaultOption(jsResponse.getName(), jsResponse.getName()));
        }
        return options;
    }

}
