package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.response.actions.Action;
import nl.sodeso.webservice.mock.console.client.application.response.actions.exception.ExceptionAction;
import nl.sodeso.webservice.mock.console.client.application.response.actions.timeout.TimeoutAction;
import nl.sodeso.webservice.mock.console.client.application.response.actions.variable.Variable;
import nl.sodeso.webservice.mock.console.client.application.response.actions.variable.VariableAction;
import nl.sodeso.webservice.mock.console.client.application.response.actions.variable.VariableScopeOption;
import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;
import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
class ActionConvertor {

    static List<JsAction> toService(List<Action> actions) {
        List<JsAction> jsActions = new ArrayList<>();

        for (Action action : actions) {
            if (action instanceof ExceptionAction) {
                jsActions.add(toService((ExceptionAction)action));
            } else if (action instanceof TimeoutAction) {
                jsActions.add(toService((TimeoutAction)action));
            } else if (action instanceof VariableAction) {
                jsActions.add(toService((VariableAction)action));
            }
        }

        return jsActions;
    }

    private static JsExceptionAction toService(ExceptionAction exceptionAction) {
        return new JsExceptionAction();
    }

    private static JsTimeoutAction toService(TimeoutAction timeoutAction) {
        JsTimeoutAction jsTimeoutAction = new JsTimeoutAction();
        jsTimeoutAction.setSeconds(timeoutAction.getSeconds().getValue());
        return jsTimeoutAction;
    }

    private static JsVariableAction toService(VariableAction variableAction) {
        JsVariableAction jsVariableAction = new JsVariableAction();

        for (Variable variable : variableAction.getVariables().getWidgets()) {
            jsVariableAction.addVariable(new JsVariableValue(variable.getVariableScopeOption().getValue().getCode(), variable.getName().getValue(), variable.getValue().getValue()));
        }

        return jsVariableAction;
    }

    static List<Action> toClient(List<JsAction> jsActions) {
        List<Action> actions = new ArrayList<>();

        for (JsAction jsAction : jsActions) {
            if (jsAction instanceof JsExceptionAction) {
                actions.add(toClient((JsExceptionAction)jsAction));
            } else if (jsAction instanceof JsTimeoutAction) {
                actions.add(toClient((JsTimeoutAction)jsAction));
            } else if (jsAction instanceof JsVariableAction) {
                actions.add(toClient((JsVariableAction)jsAction));
            }
        }

        return actions;
    }

    private static ExceptionAction toClient(JsExceptionAction jsAction) {
        return new ExceptionAction();
    }

    private static TimeoutAction toClient(JsTimeoutAction jsAction) {
        TimeoutAction timeoutAction = new TimeoutAction();
        timeoutAction.setSeconds(new StringType(jsAction.getSeconds()));
        return timeoutAction;
    }

    private static VariableAction toClient(JsVariableAction jsAction) {
        VariableAction variableAction = new VariableAction();

        for (JsVariableValue jsVariableValue : jsAction.getVariables()) {
            Variable variable = new Variable();
            variable.setVariableScopeOption(new OptionType<>(VariableScopeOption.getOption(jsVariableValue.getScope())));
            variable.setName(new StringType(jsVariableValue.getName()));
            variable.setValue(new StringType(jsVariableValue.getValue()));
            variableAction.getVariables().getWidgets().add(variable);
        }

        return variableAction;
    }

}
