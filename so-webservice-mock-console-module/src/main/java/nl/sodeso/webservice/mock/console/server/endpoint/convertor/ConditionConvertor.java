package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.generic.ExpressionOption;
import nl.sodeso.webservice.mock.console.client.application.generic.HeaderOption;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.Condition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.parameter.ParameterValue;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.path.PathCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.query.QueryCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.body.BodyCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.cookie.CookieCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.header.HeaderCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.method.MethodCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.method.MethodOption;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.parameter.ParameterCondition;
import nl.sodeso.webservice.mock.console.client.application.response.conditions.query.QueryValue;
import nl.sodeso.webservice.mock.model.generic.JsParameter;
import nl.sodeso.webservice.mock.model.generic.JsPath;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.*;
import nl.sodeso.webservice.mock.model.generic.JsQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
class ConditionConvertor {

    static List<JsCondition> toService(List<Condition> conditions) {
        List<JsCondition> jsConditions = new ArrayList<>();

        for (Condition condition : conditions) {
            if (condition instanceof BodyCondition) {
                jsConditions.add(toService((BodyCondition)condition));
            } else if (condition instanceof CookieCondition) {
                jsConditions.add(toService((CookieCondition)condition));
            } else if (condition instanceof HeaderCondition) {
                jsConditions.add(toService((HeaderCondition)condition));
            } else if (condition instanceof MethodCondition) {
                jsConditions.add(toService((MethodCondition)condition));
            } else if (condition instanceof ParameterCondition) {
                jsConditions.add(toService((ParameterCondition)condition));
            } else if (condition instanceof QueryCondition) {
                jsConditions.add(toService((QueryCondition)condition));
            } else if (condition instanceof PathCondition) {
                jsConditions.add(toService((PathCondition)condition));
            }
        }

        return jsConditions;
    }

    private static JsBodyCondition toService(BodyCondition bodyCondition) {
        JsBodyCondition jsBodyCondition = new JsBodyCondition();
        toService(bodyCondition.getExpressionOption(), bodyCondition.getExpression(), jsBodyCondition);
        jsBodyCondition.setMatches(bodyCondition.getMatches().getValue());
        return jsBodyCondition;
    }

    private static JsCookieCondition toService(CookieCondition cookieCondition) {
        JsCookieCondition jsCookieCondition = new JsCookieCondition();
        toService(cookieCondition.getExpressionOption(), cookieCondition.getExpression(), jsCookieCondition);
        jsCookieCondition.setName(cookieCondition.getName().getValue());
        jsCookieCondition.setMatches(cookieCondition.getMatches().getValue());
        return jsCookieCondition;
    }

    private static JsHeaderCondition toService(HeaderCondition headerCondition) {
        JsHeaderCondition jsHeaderCondition = new JsHeaderCondition();
        toService(headerCondition.getExpressionOption(), headerCondition.getExpression(), jsHeaderCondition);

        String name = headerCondition.getHeaderOption().getValue().getCode();
        if (headerCondition.getHeaderOption().getValue().isOther()) {
            name = headerCondition.getOtherHeaderOption().getValue();
        }

        jsHeaderCondition.setName(name);
        jsHeaderCondition.setMatches(headerCondition.getMatches().getValue());
        return jsHeaderCondition;
    }

    private static JsMethodCondition toService(MethodCondition methodCondition) {
        JsMethodCondition jsMethodCondition = new JsMethodCondition();
        jsMethodCondition.setType(methodCondition.getMethodOption().getValue().getCode());
        return jsMethodCondition;
    }

    private static JsParameterCondition toService(ParameterCondition parameterCondition) {
        JsParameterCondition jsParameterCondition = new JsParameterCondition();
        jsParameterCondition.setPath(parameterCondition.getPath().getValue());

        for (ParameterValue parameterValue : parameterCondition.getParameterValues().getWidgets()) {
            JsParameter jsParameterValue = new JsParameter(parameterValue.getName().getValue(), parameterValue.getValue().getValue());
            jsParameterCondition.addParameter(jsParameterValue);
        }

        return jsParameterCondition;
    }

    private static JsQueryCondition toService(QueryCondition queryCondition) {
        JsQueryCondition jsQueryCondition = new JsQueryCondition();

        for (QueryValue queryValue : queryCondition.getQueryValues().getWidgets()) {
            jsQueryCondition.addQuery(new JsQuery(queryValue.getName().getValue(), queryValue.getValue().getValue()));
        }

        return jsQueryCondition;
    }

    private static JsPathCondition toService(PathCondition pathCondition) {
        JsPathCondition jsPathCondition = new JsPathCondition();
        jsPathCondition.setPath(pathCondition.getPath().getValue());
        jsPathCondition.setRegexp(pathCondition.isRegexp().getValue());
        return jsPathCondition;
    }

    private static void toService(OptionType<ExpressionOption> expressionOption, StringType expression, JsPath jsPath) {
        if (expressionOption.getValue().equals(ExpressionOption.NONE)) {
            // We do nothing.
        } else if (expressionOption.getValue().equals(ExpressionOption.XPATH)) {
            jsPath.setXpath(expression.getValue());
        } else if (expressionOption.getValue().equals(ExpressionOption.JSON_PATH)) {
            jsPath.setJsonpath(expression.getValue());
        } else if (expressionOption.getValue().equals(ExpressionOption.REGEXP)) {
            jsPath.setRegexp(expression.getValue());
        }
    }

    static List<Condition> toClient(List<JsCondition> jsConditions) {
        List<Condition> conditions = new ArrayList<>();

        for (JsCondition jsCondition : jsConditions) {
            if (jsCondition instanceof JsBodyCondition) {
                conditions.add(toClient((JsBodyCondition)jsCondition));
            } else if (jsCondition instanceof JsCookieCondition) {
                conditions.add(toClient((JsCookieCondition)jsCondition));
            } else if (jsCondition instanceof JsHeaderCondition) {
                conditions.add(toClient((JsHeaderCondition)jsCondition));
            } else if (jsCondition instanceof JsMethodCondition) {
                conditions.add(toClient((JsMethodCondition)jsCondition));
            } else if (jsCondition instanceof JsParameterCondition) {
                conditions.add(toClient((JsParameterCondition)jsCondition));
            } else if (jsCondition instanceof JsQueryCondition) {
                conditions.add(toClient((JsQueryCondition)jsCondition));
            } else if (jsCondition instanceof JsPathCondition) {
                conditions.add(toClient((JsPathCondition)jsCondition));
            }
        }

        return conditions;
    }

    private static BodyCondition toClient(JsBodyCondition jsBodyCondition) {
        BodyCondition bodyCondition = new BodyCondition();
        bodyCondition.setExpressionOption(getExpressionOption(jsBodyCondition));
        bodyCondition.setExpression(getExpression(jsBodyCondition));
        bodyCondition.setMatches(new StringType(jsBodyCondition.getMatches()));
        return bodyCondition;
    }

    private static CookieCondition toClient(JsCookieCondition jsCookieCondition) {
        CookieCondition cookieCondition = new CookieCondition();
        cookieCondition.setName(new StringType(jsCookieCondition.getName()));
        cookieCondition.setExpressionOption(getExpressionOption(jsCookieCondition));
        cookieCondition.setExpression(getExpression(jsCookieCondition));
        cookieCondition.setMatches(new StringType(jsCookieCondition.getMatches()));
        return cookieCondition;
    }

    private static HeaderCondition toClient(JsHeaderCondition jsHeaderCondition) {
        HeaderCondition headerCondition = new HeaderCondition();

        HeaderOption headerOption = HeaderOption.fromValue(jsHeaderCondition.getName());
        if (headerOption == null) {
            headerOption = HeaderOption.OTHER;
            headerCondition.setOtherHeaderOption(new StringType(jsHeaderCondition.getName()));
        } else {
            headerCondition.setOtherHeaderOption(new StringType());
        }

        headerCondition.setHeaderOption(new OptionType<>(headerOption));
        headerCondition.setExpressionOption(getExpressionOption(jsHeaderCondition));
        headerCondition.setExpression(getExpression(jsHeaderCondition));
        headerCondition.setMatches(new StringType(jsHeaderCondition.getMatches()));
        return headerCondition;
    }

    private static MethodCondition toClient(JsMethodCondition jsMethodCondition) {
        MethodCondition methodCondition = new MethodCondition();
        methodCondition.setMethodOption(new OptionType<>(MethodOption.getOption(jsMethodCondition.getType())));
        return methodCondition;
    }

    private static ParameterCondition toClient(JsParameterCondition jsParameterCondition) {
        ParameterCondition parameterCondition = new ParameterCondition();
        parameterCondition.setPath(new StringType(jsParameterCondition.getPath()));

        for (JsParameter jsParameterValue : jsParameterCondition.getParameters()) {
            ParameterValue parameterValue = new ParameterValue();
            parameterValue.setName(new StringType(jsParameterValue.getName()));
            parameterValue.setValue(new StringType(jsParameterValue.getValue()));
            parameterCondition.getParameterValues().getWidgets().add(parameterValue);
        }

        return parameterCondition;
    }

    private static QueryCondition toClient(JsQueryCondition jsQueryCondition) {
        QueryCondition queryCondition = new QueryCondition();

        for (JsQuery jsQueryValue : jsQueryCondition.getQueries()) {
            QueryValue queryValue = new QueryValue();
            queryValue.setName(new StringType(jsQueryValue.getName()));
            queryValue.setValue(new StringType(jsQueryValue.getValue()));
            queryCondition.getQueryValues().getWidgets().add(queryValue);
        }

        return queryCondition;
    }

    private static PathCondition toClient(JsPathCondition jsPathCondition) {
        PathCondition pathCondition = new PathCondition();
        pathCondition.setPath(new StringType(jsPathCondition.getPath()));
        pathCondition.setRegexp(new BooleanType(jsPathCondition.isRegexp()));
        return pathCondition;
    }

    private static StringType getExpression(JsPath jsPath) {
        return new StringType(
            jsPath.getXpath() != null ? jsPath.getXpath() : jsPath.getJsonpath() != null ? jsPath.getJsonpath() :
                    jsPath.getRegexp() != null ? jsPath.getRegexp() : null
        );
    }

    private static OptionType<ExpressionOption> getExpressionOption(JsPath jsPath) {
        if (jsPath.getXpath() != null) {
            return new OptionType<>(ExpressionOption.XPATH);
        } else if (jsPath.getJsonpath() != null) {
            return new OptionType<>(ExpressionOption.JSON_PATH);
        } else if (jsPath.getRegexp() != null) {
            return new OptionType<>(ExpressionOption.REGEXP);
        }

        return new OptionType<>(ExpressionOption.NONE);
    }

}
