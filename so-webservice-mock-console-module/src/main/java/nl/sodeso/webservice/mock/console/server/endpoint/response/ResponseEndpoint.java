package nl.sodeso.webservice.mock.console.server.endpoint.response;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.webservice.mock.console.client.application.response.Response;
import nl.sodeso.webservice.mock.console.client.application.response.ResponseSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.response.rpc.ResponseRpc;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.console.server.endpoint.convertor.ResponseConvertor;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.webservicemock-response")
public class ResponseEndpoint extends XsrfProtectedServiceServlet implements ResponseRpc {

    public ArrayList<ResponseSummaryItem> findSummaries(ServiceOption serviceOption, String endpointName) {
        return ResponseConvertor.toClientSummaries(ResponseRestClient.retreiveAllResponses(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName));
    }

    public Response getDetails(ServiceOption serviceOption, String endpointName, String responseName) {
        JsResponse jsResponse = ResponseRestClient.retreiveSingleResponse(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, responseName);
        if (jsResponse != null) {
            return ResponseConvertor.toClient(jsResponse);
        }

        return null;
    }

    public ValidationResult isNameUnique(ServiceOption serviceOption, String endpointName, String responseName) {
        ValidationResult result = new ValidationResult();

        JsResponse jsResponse = ResponseRestClient.retreiveSingleResponse(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, responseName);
        if (jsResponse != null) {
            result.add(new ValidationMessage("name", ValidationMessage.Level.ERROR, "The name is already in use."));
        }

        return result;
    }

    public ResponseSummaryItem save(ServiceOption serviceOption, String endpointName, Response response) throws RemoteException {
        JsResponse jsResponse = ResponseConvertor.toService(response);

        if (response.isExisting()) {
            ResponseRestClient.updatResponse(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, response.getName().getOriginalValue(), jsResponse);
        } else {
            ResponseRestClient.addResponse(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, jsResponse);
        }

        return ResponseConvertor.toClientSummary(ResponseRestClient.retreiveSingleResponse(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, response.getName().getValue()));
    }

    public ResponseSummaryItem duplicate(ServiceOption serviceOption, String endpointName, String responseName, String newResponseName) {
        JsResponse jsResponse = ResponseRestClient.retreiveSingleResponse(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, responseName);
        jsResponse.setName(newResponseName);
        ResponseRestClient.addResponse(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, jsResponse);
        return ResponseConvertor.toClientSummary(jsResponse);
    }

    public void delete(ServiceOption serviceOption, String endpointName, String responseName) {
        ResponseRestClient.deleteResponse(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, responseName);
    }
}
