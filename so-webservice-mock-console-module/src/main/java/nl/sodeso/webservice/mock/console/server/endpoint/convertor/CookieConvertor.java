package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.generic.ActionOption;
import nl.sodeso.webservice.mock.console.client.application.generic.cookie.Cookie;
import nl.sodeso.webservice.mock.model.generic.JsCookie;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class CookieConvertor {

    public static List<JsCookie> toService(List<Cookie> cookies) {
        List<JsCookie> jsCookies = new ArrayList<>();

        for (Cookie cookie : cookies) {
            JsCookie jsCookie = new JsCookie(cookie.getActionOption().getValue().getKey(), cookie.getName().getValue(), cookie.getValue().getValue());
            jsCookie.setMaxAge(cookie.getMaxAge().getValue());
            jsCookie.setDomain(cookie.getDomain().getValue());
            jsCookie.setPath(cookie.getPath().getValue());
            jsCookie.setVersion(cookie.getVersion().getValue());
            jsCookies.add(jsCookie);
        }

        return jsCookies;
    }

   public static List<Cookie> toClient(List<JsCookie> jsCookies) {
        List<Cookie> cookies = new ArrayList<>();

        for (JsCookie jsCookie : jsCookies) {
            Cookie cookie = new Cookie();
            cookie.setActionOption(new OptionType<>(ActionOption.getOption(jsCookie.getAction())));
            cookie.setName(new StringType(jsCookie.getName()));
            cookie.setValue(new StringType(jsCookie.getValue()));
            cookie.setMaxAge(new StringType(jsCookie.getMaxAge()));
            cookie.setDomain(new StringType(jsCookie.getDomain()));
            cookie.setPath(new StringType(jsCookie.getPath()));
            cookie.setVersion(new StringType(jsCookie.getVersion()));
            cookies.add(cookie);
        }

        return cookies;
    }

}
