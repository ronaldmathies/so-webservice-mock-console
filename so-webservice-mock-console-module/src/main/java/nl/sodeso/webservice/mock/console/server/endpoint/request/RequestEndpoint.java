package nl.sodeso.webservice.mock.console.server.endpoint.request;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.webservice.mock.console.client.application.request.Request;
import nl.sodeso.webservice.mock.console.client.application.request.RequestSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.request.rpc.RequestRpc;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.console.server.endpoint.convertor.RequestConvertor;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.webservicemock-request")
public class RequestEndpoint extends XsrfProtectedServiceServlet implements RequestRpc {

    public ArrayList<RequestSummaryItem> findSummaries(ServiceOption serviceOption, String endpointName) {
        return RequestConvertor.toClientSummaries(RequestRestClient.getRequests(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName));
    }

    public ArrayList<RequestSummaryItem> findSummaries(ServiceOption serviceOption, String endpointName, String responseName) {
        return RequestConvertor.toClientSummaries(RequestRestClient.getRequests(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, responseName));
    }

    public Request findSingle(ServiceOption serviceOption, String endpointName, String id) {
        return RequestConvertor.toClient(RequestRestClient.getRequest(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, id));
    }

    public Request findSingle(ServiceOption serviceOption, String endpointName, String responseName, String id) {
        return RequestConvertor.toClient(RequestRestClient.getRequest(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, responseName, id));
    }

    public void delete(ServiceOption serviceOption, String endpointName, String id) {
        RequestRestClient.deleteRequest(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, id);
    }

    public void deleteAll(ServiceOption serviceOption, String endpointName) {
        RequestRestClient.deleteRequestsAll(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName);
    }

    public void delete(ServiceOption serviceOption, String endpointName, String responseName, String id) {
        RequestRestClient.deleteRequest(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, responseName, id);
    }

    public void deleteAll(ServiceOption serviceOption, String endpointName, String responseName) {
        RequestRestClient.deleteRequestsAll(serviceOption.getIpAddress(), serviceOption.getPort(), endpointName, responseName);
    }

}
