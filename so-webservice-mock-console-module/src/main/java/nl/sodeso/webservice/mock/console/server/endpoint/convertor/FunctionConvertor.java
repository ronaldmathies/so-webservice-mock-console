package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.webservice.mock.console.client.application.response.functions.model.Function;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.Parameter;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.Sample;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.SampleParameter;
import nl.sodeso.webservice.mock.model.function.JsFunction;
import nl.sodeso.webservice.mock.model.function.JsFunctionParameter;
import nl.sodeso.webservice.mock.model.function.JsSample;
import nl.sodeso.webservice.mock.model.function.JsSampleParameter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class FunctionConvertor {

    public static ArrayList<Function> toClient(List<JsFunction> jsFunctions) {
        ArrayList<Function> functions = new ArrayList<>();
        for (JsFunction jsFunction : jsFunctions) {
            functions.add(toClient(jsFunction));
        }
        return functions;
    }

    public static Function toClient(JsFunction jsFunction) {
        Function function = new Function(jsFunction.getName(), jsFunction.getDescription(), jsFunction.getDocumentation(), jsFunction.getFormat());

        for (JsFunctionParameter jsParameter : jsFunction.getJsFunctionParameters()) {
            function.addParameter(new Parameter(jsParameter.getName(), jsParameter.getType()));
        }

        for (JsSample jsSample : jsFunction.getJsSamples()) {
            Sample sample = new Sample(jsSample.getName());

            for (JsSampleParameter jsSampleParameter : jsSample.getJsSampleParameters()) {
                sample.addSampleParameter(new SampleParameter(jsSampleParameter.getName(), jsSampleParameter.getValue()));
            }

            function.addSample(sample);
        }

        return function;
    }

}
