package nl.sodeso.webservice.mock.console.server.websocket.discovery;

import nl.sodeso.gwt.websocket.client.Message;
import nl.sodeso.gwt.websocket.server.WebsocketMessageHandler;

import javax.servlet.annotation.WebListener;
import javax.websocket.Session;

/**
 * @author Ronald Mathies
 */
public class DiscoveryWebsocketHandler implements WebsocketMessageHandler {

    @Override
    public void onMessage(Session session, Message message) {
    }

//
//    /**
//     * {@inheritDoc}
//     */
//    public void started() {
//        WebSocketServerController.instance().send(new DiscoveryStartedMessage());
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public void found(DiscoveredService discoveredService) {
//        WebSocketServerController.instance().send(new DiscoveryFoundMessage());
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public void failed() {
//        WebSocketServerController.instance().send(new DiscoveryFailedMessage());
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public void ended() {
//        WebSocketServerController.instance().send(new DiscoveryEndedMessage());
//    }
}
