package nl.sodeso.webservice.mock.console.server.endpoint.response;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.commons.restful.client.RestClient;
import nl.sodeso.commons.restful.client.RestClientCallback;
import nl.sodeso.commons.restful.client.RestClientException;
import nl.sodeso.commons.restful.client.SimpleRestClientConfiguration;
import nl.sodeso.webservice.mock.model.generic.JsStatus;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponseView;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ResponseRestClient {

    private static final String WS_MOCK_POST_RESPONSE = "/mock/response/%s";
    private static final String WS_MOCK_PUT_RESPONSE = "/mock/response/%s/%s";
    private static final String WS_MOCK_GET_RESPONSE = "/mock/response/%s/%s";
    private static final String WS_MOCK_GET_RESPONSES = "/mock/responses/%s";
    private static final String WS_MOCK_DELETE_RESPONSE = "/mock/response/%s/%s";
    private static final String WS_MOCK_DELETE_RESPONSES = "/mock/responses/%s";

    static void addResponse(String host, int port, String endpointName, JsResponse jsResponse) {
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.post(WS_MOCK_POST_RESPONSE, writeValue(JsResponseView.Detailed.class, jsResponse), new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }
    }

    static void updatResponse(String host, int port, String endpointName, String responseName, JsResponse jsResponse) {
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.put(WS_MOCK_PUT_RESPONSE, writeValue(JsResponseView.Detailed.class, jsResponse), new RestClientCallback() {
                @Override
                public void onSuccess(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName, responseName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }
    }

    static List<JsResponse> retreiveAllResponses(String host, int port, String name) {
        List<JsResponse> mResponses = new ArrayList<>();

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_RESPONSES, new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) throws RestClientException {
                    mResponses.addAll(readValue(response, JsResponseView.Summary.class, new TypeReference<List<JsResponse>>() {}));
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    // This should not happen, the service always returns an OK.
                }
            }, name);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return mResponses;
    }

    static JsResponse retreiveSingleResponse(String host, int port, String endpointName, String responseName) {
        JsResponse[] mResponse = new JsResponse[1];
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_RESPONSE, new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) throws RestClientException {
                    mResponse[0] = readValue(response, JsResponseView.Detailed.class, new TypeReference<JsResponse>() {});
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName, responseName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return mResponse[0];
    }

    static boolean deleteResponse(String host, int port, String endpointName, String responseName) {
        boolean[] result = {false};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.delete(WS_MOCK_DELETE_RESPONSE, new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    result[0] = true;
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    // This should not happen, the service always returns an OK.
                }
            }, endpointName, responseName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return result[0];
    }

    public static boolean deleteAllResponses(String host, int port, String name) {
        boolean[] result = {false};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.delete(WS_MOCK_DELETE_RESPONSES, new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    result[0] = true;
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    // This should not happen, the service always returns an OK.
                }
            }, name);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return result[0];
    }

    private static <X> X readValue(Response response, Class<?> view, TypeReference typeReference) throws RestClientException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readerWithView(view)
                    .forType(typeReference)
                    .readValue(response.readEntity(String.class));
        } catch (IOException e) {
            throw new RestClientException(e, "Failed to read response.");
        }
    }

    private static Entity<String> writeValue(Class<?> view, Object value) throws RestClientException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return Entity.entity(mapper.writerWithView(view).writeValueAsString(value), MediaType.APPLICATION_JSON_TYPE);
        } catch (IOException e) {
            throw new RestClientException(e, "Failed to write data.");
        }
    }

}
