package nl.sodeso.webservice.mock.console.server.endpoint.service;

import nl.sodeso.commons.restful.client.RestClient;
import nl.sodeso.commons.restful.client.RestClientCallback;
import nl.sodeso.commons.restful.client.RestClientException;
import nl.sodeso.commons.restful.client.SimpleRestClientConfiguration;
import nl.sodeso.webservice.mock.model.service.JsService;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class ServiceRestClient {

    private static final Logger log = Logger.getLogger(ServiceRestClient.class.getName());

    private static final String WS_MOCK_GET_VERSION = "/mock/version";

    static JsService getVersion(String host, int port) {
        log.info(String.format("Resolving version information for %s:%d.", host, port));

        JsService[] jsService = {null};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_VERSION, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) {
                    jsService[0] = response.readEntity(new GenericType<JsService>() {});
                }

                @Override
                public void onFailure(Response response) {
                    // This should not happen, the service always returns an OK.
                }
            });

            log.info(String.format("Version information resolved for %s:%d, found version %s with name %s..", host, port, jsService[0].getVersion(), jsService[0].getName()));

        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return jsService[0];
    }

}
