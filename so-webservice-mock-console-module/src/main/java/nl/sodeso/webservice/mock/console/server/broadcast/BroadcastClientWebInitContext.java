package nl.sodeso.webservice.mock.console.server.broadcast;

import nl.sodeso.commons.network.discovery.broadcast.BroadcastClientConfiguration;
import nl.sodeso.commons.network.discovery.broadcast.AbstractBroadcastClientWebInitContext;
import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.webservice.mock.console.client.Constants;
import nl.sodeso.webservice.mock.console.server.endpoint.properties.ModulePropertiesContainer;

/**
 * @author Ronald Mathies
 */
public class BroadcastClientWebInitContext extends AbstractBroadcastClientWebInitContext {

    @Override
    public BroadcastClientConfiguration getConfiguration() {
        return new BroadcastClientConfiguration() {
            @Override
            public boolean isEnabled() {
                return PropertyConfiguration.getInstance().getBooleanProperty(Constants.MODULE_ID, ModulePropertiesContainer.DISCOVERY_BROADCAST_ENABLED, false);
            }

            @Override
            public String getServiceId() {
                return PropertyConfiguration.getInstance().getStringProperty(Constants.MODULE_ID, ModulePropertiesContainer.DISCOVERY_SERVICE_ID, null);
            }

            @Override
            public String getLocalBindAddress() {
                return PropertyConfiguration.getInstance().getStringProperty(Constants.MODULE_ID, ModulePropertiesContainer.DISCOVERY_BROADCAST_ADDRESS, null);
            }

            @Override
            public int getLocalBindPort() {
                return PropertyConfiguration.getInstance().getIntegerProperty(Constants.MODULE_ID, ModulePropertiesContainer.DISCOVERY_BROADCAST_PORT, 0);
            }
        };
    }
}
