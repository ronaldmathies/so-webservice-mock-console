package nl.sodeso.webservice.mock.console.server.endpoint.endpoint;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import nl.sodeso.commons.restful.client.RestClient;
import nl.sodeso.commons.restful.client.RestClientCallback;
import nl.sodeso.commons.restful.client.RestClientException;
import nl.sodeso.commons.restful.client.SimpleRestClientConfiguration;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpointView;
import nl.sodeso.webservice.mock.model.generic.JsStatus;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class EndpointRestClient {

    private static final String WS_MOCK_POST_ENDPOINT = "/mock/endpoint";
    private static final String WS_MOCK_PUT_ENDPOINT = "/mock/endpoint/%s";
    private static final String WS_MOCK_GET_ENDPOINTS = "/mock/endpoints";
    private static final String WS_MOCK_GET_ENDPOINT_BY_NAME = "/mock/endpoint";
    private static final String WS_MOCK_GET_ENDPOINT_BY_PATH = "/mock/endpoint";
    private static final String WS_MOCK_DELETE_ENDPOINT = "/mock/endpoint/%s";

    private static final String QUERY_NAME = "name";
    private static final String QUERY_PATH = "path";

    public static void addEndpoint(String host, int port, JsEndpoint mEndpoint) {
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.post(WS_MOCK_POST_ENDPOINT, writeValue(JsEndpointView.Responses.class, mEndpoint), new RestClientCallback() {
                @Override
                public void onSuccess(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            });
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }
    }

    public static void updateEndpoint(String host, int port, String name, JsEndpoint mEndpoint) {
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.put(WS_MOCK_PUT_ENDPOINT, writeValue(JsEndpointView.Responses.class, mEndpoint), new RestClientCallback() {
                @Override
                public void onSuccess(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, name);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }
    }

    public static List<JsEndpoint> retreiveAllEndpoints(String host, int port) {
        List<JsEndpoint> endpoints = new ArrayList<>();


        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_ENDPOINTS, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException {
                    endpoints.addAll(readValue(response, JsEndpointView.Summary.class, new TypeReference<List<JsEndpoint>>() {}));
                }

                @Override
                public void onFailure(Response response) {
                    // This should not happen, the service always returns an OK.
                }
            });
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return endpoints;
    }

    public static JsEndpoint retreiveEndpointByName(String host, int port, String name) {
        JsEndpoint[] endpoint = new JsEndpoint[1];
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_ENDPOINT_BY_NAME, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException{
                    endpoint[0] = readValue(response, JsEndpointView.Detailed.class, new TypeReference<JsEndpoint>() {});
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, ImmutableMap.of(QUERY_NAME, name));
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return endpoint[0];
    }

    public static JsEndpoint retreiveEndpointByPath(String host, int port, String path) {
        JsEndpoint[] endpoint = new JsEndpoint[1];
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_ENDPOINT_BY_PATH, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException{
                    endpoint[0] = readValue(response, JsEndpointView.Detailed.class, new TypeReference<JsEndpoint>() {});
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, ImmutableMap.of(QUERY_PATH, path));
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return endpoint[0];
    }

    public static boolean deleteEndpoint(String host, int port, String name) {
        boolean[] result = {false};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.delete(WS_MOCK_DELETE_ENDPOINT, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    result[0] = true;
                }

                @Override
                public void onFailure(Response response) {
                    // This should not happen, the service always returns an OK.
                }
            }, name);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return result[0];
    }

    private static <X> X readValue(Response response, Class<?> view, TypeReference typeReference) throws RestClientException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readerWithView(view)
                    .forType(typeReference)
                    .readValue(response.readEntity(String.class));
        } catch (IOException e) {
            throw new RestClientException(e, "Failed to read data.");
        }
    }

    private static Entity<String> writeValue(Class<?> view, Object value) throws RestClientException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return Entity.entity(mapper.writerWithView(view).writeValueAsString(value), MediaType.APPLICATION_JSON_TYPE);
        } catch (IOException e) {
            throw new RestClientException(e, "Failed to write data.");
        }
    }

}
