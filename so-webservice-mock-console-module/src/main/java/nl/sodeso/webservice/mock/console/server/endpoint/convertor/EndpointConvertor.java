package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.endpoint.Endpoint;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class EndpointConvertor {

    public static JsEndpoint toService(Endpoint endpoint) {
        JsEndpoint jsEndpoint = new JsEndpoint(endpoint.getName().getValue(), endpoint.getPath().getValue());
        jsEndpoint.setJsLogging(LoggingConvertor.toService(endpoint.getLogging()));

        return jsEndpoint;
    }

    public static Endpoint toClient(JsEndpoint jsEndpoint) {
        Endpoint endpoint = new Endpoint();

        endpoint.setIsExisting(true);
        endpoint.setName(new StringType(jsEndpoint.getName()));
        endpoint.setPath(new StringType(jsEndpoint.getPath()));
        endpoint.setLogging(LoggingConvertor.toClient(jsEndpoint.getJsLogging()));

        return endpoint;
    }

    public static ArrayList<EndpointSummaryItem> toClientSummaries(List<JsEndpoint> jsEndpoints) {
        ArrayList<EndpointSummaryItem> endpoints = new ArrayList<>();

        for (JsEndpoint jsEndpoint : jsEndpoints) {
            endpoints.add(toClientSummary(jsEndpoint));
        }

        return endpoints;
    }

    public static EndpointSummaryItem toClientSummary(JsEndpoint jsEndpoint) {
        EndpointSummaryItem summaryItem = new EndpointSummaryItem();
        summaryItem.setName(jsEndpoint.getName());
        return summaryItem;
    }

    public static ArrayList<DefaultOption> toOptions(List<JsEndpoint> jsEndpoints) {
        ArrayList<DefaultOption> options = new ArrayList<>();
        for (JsEndpoint jsEndpoint : jsEndpoints) {
            options.add(new DefaultOption(jsEndpoint.getName(), jsEndpoint.getName()));
        }
        return options;
    }
}
