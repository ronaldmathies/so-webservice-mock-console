package nl.sodeso.webservice.mock.console.client.properties;

import nl.sodeso.gwt.ui.client.application.module.properties.ClientModuleProperties;

/**
 * @author Ronald Mathies
 */
public class WebServiceMockConsoleClientAppProperties extends ClientModuleProperties {

    /*
        By default the application has an ApplicationProperties consisting of the following settings:

        name: Name of the application (used as the title)
        version: Version number of the application (see contacts-configuration.properties)
        devkitEnabled: Are the extra development tools available (see contacts-configuration.properties).

        If you would like to have more global application settings (not user specific), that don't
        change during a session, then this is the place to add them. Just add the fields below
        and fill them using the ApplicationPropertiesEndpoint.

        for example:

        private String myField = "";

        public String getMyField() {
            return this.myField;
        }

        public void setMyField(String myField) {
            this.myField = myField;
        }

    */

}
