package nl.sodeso.webservice.mock.console.server.endpoint.functions;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.commons.restful.client.RestClient;
import nl.sodeso.commons.restful.client.RestClientCallback;
import nl.sodeso.commons.restful.client.RestClientException;
import nl.sodeso.commons.restful.client.SimpleRestClientConfiguration;
import nl.sodeso.webservice.mock.model.function.JsFunction;
import nl.sodeso.webservice.mock.model.generic.JsStatus;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class FunctionRestClient {


    private static final String WS_MOCK_GET_GROUPS =  "/mock/groups";
    private static final String WS_MOCK_GET_GROUP_FUNCTIONS = "/mock/%s/functions";

    public static ArrayList<String> groups(String host, int port) {
        ArrayList<String> groups = new ArrayList<>();
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_GROUPS, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException {
                    groups.addAll(readValue(response, String.class, new TypeReference<List<String>>() {}));
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            });
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }
        return groups;
    }

    public static ArrayList<JsFunction> functions(String host, int port, String group) {
        ArrayList<JsFunction> functions = new ArrayList<>();
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_GROUP_FUNCTIONS, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException {
                    functions.addAll(readValue(response, JsFunction.class, new TypeReference<List<JsFunction>>() {}));
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, group);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return functions;
    }

    private static <X> X readValue(Response response, Class<?> view, TypeReference typeReference) throws RestClientException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readerWithView(view)
                    .forType(typeReference)
                    .readValue(response.readEntity(String.class));
        } catch (IOException e) {
            throw new RestClientException(e, "Failed to read data.");
        }
    }

    private static Entity<String> writeValue(Class<?> view, Object value) throws RestClientException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return Entity.entity(mapper.writerWithView(view).writeValueAsString(value), MediaType.APPLICATION_JSON_TYPE);
        } catch (IOException e) {
            throw new RestClientException(e, "Failed to write data.");
        }
    }

}
