package nl.sodeso.webservice.mock.console.server.endpoint.download;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.commons.restful.client.RestClient;
import nl.sodeso.commons.restful.client.RestClientCallback;
import nl.sodeso.commons.restful.client.RestClientException;
import nl.sodeso.commons.restful.client.SimpleRestClientConfiguration;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpointView;
import nl.sodeso.webservice.mock.model.generic.JsStatus;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ExportRestClient {

    private static final String WS_MOCK_GET_EXPORT_ENDPOINT = "/mock/export/%s";
    private static final String WS_MOCK_GET_EXPORT_ENDPOINT_WITH_REQUESTS = "/mock/export/%s/requests";
    private static final String WS_MOCK_GET_EXPORT_ENDPOINT_WITH_RESPONSES = "/mock/export/%s/responses";
    private static final String WS_MOCK_GET_EXPORT_ENDPOINT_WITH_RESPONSES_AND_REQUESTS = "/mock/export/%s/responses/requests";
    private static final String WS_MOCK_GET_EXPORT_RESPONSE = "/mock/export/%s/%s";
    private static final String WS_MOCK_GET_EXPORT_RESPONSE_WITH_REQUESTS = "/mock/export/%s/%s/requests";

    public static InputStream exportEndpoint(String host, int port, String endpointName) {
        return exportEndpoint(WS_MOCK_GET_EXPORT_ENDPOINT, host, port, endpointName);
    }

    public static InputStream exportEndpointWithRequests(String host, int port, String endpointName) {
        return exportEndpoint(WS_MOCK_GET_EXPORT_ENDPOINT_WITH_REQUESTS, host, port, endpointName);
    }

    public static InputStream exportEndpointWithResponses(String host, int port, String endpointName) {
        return exportEndpoint(WS_MOCK_GET_EXPORT_ENDPOINT_WITH_RESPONSES, host, port, endpointName);
    }

    public static InputStream exportEndpointWithResponsesAndRequests(String host, int port, String endpointName) {
        return exportEndpoint(WS_MOCK_GET_EXPORT_ENDPOINT_WITH_RESPONSES_AND_REQUESTS, host, port, endpointName);
    }

    private static InputStream exportEndpoint(String path, String host, int port, String endpointName) {
        InputStream[] inputStreams = new InputStream[1];
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(path, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) {
                    inputStreams[0] = response.readEntity(InputStream.class);
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return inputStreams[0];
    }

    public static InputStream exportResponse(String host, int port, String endpointName, String responseName) {
        return exportResponse(WS_MOCK_GET_EXPORT_RESPONSE, host, port, endpointName, responseName);
    }

    public static InputStream exportResponseWithRequests(String host, int port, String endpointName, String responseName) {
        return exportResponse(WS_MOCK_GET_EXPORT_RESPONSE_WITH_REQUESTS, host, port, endpointName, responseName);
    }

    private static InputStream exportResponse(String path, String host, int port, String endpointName, String responseName) {
        InputStream[] inputStreams = new InputStream[1];
        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(path, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) {
                    inputStreams[0] = response.readEntity(InputStream.class);
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName, responseName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return inputStreams[0];
    }

}
