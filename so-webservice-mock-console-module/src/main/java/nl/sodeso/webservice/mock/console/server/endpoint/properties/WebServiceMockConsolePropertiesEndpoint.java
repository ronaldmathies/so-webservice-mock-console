package nl.sodeso.webservice.mock.console.server.endpoint.properties;

import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractModulePropertiesEndpoint;
import nl.sodeso.gwt.ui.server.endpoint.properties.ModulePropertiesContainer;
import nl.sodeso.webservice.mock.console.client.Constants;
import nl.sodeso.webservice.mock.console.client.properties.WebServiceMockConsoleClientAppProperties;

import javax.servlet.annotation.WebServlet;

/**
 * By default the application has an endpoint for properties consisting of the following settings:
 *
 * <ul>
 *  <li>name: Name of the application (used as the title)</li>
 *  <li>version: Version number of the application.</li>
 *  <li>devkitEnabled: Are the extra development tools available.</li>
 * </ul>
 *
 * If you would like to have more global application settings (not user specific), that don't
 * change during a session, then this is the place to add them. Just add the fields below
 * and fill them using the endpoint.
 *
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.webservicemockconsole-properties"})
public class WebServiceMockConsolePropertiesEndpoint extends AbstractModulePropertiesEndpoint<WebServiceMockConsoleClientAppProperties, WebServiceMockConsoleServerModuleProperties> {

    @Override
    public void fillApplicationProperties(WebServiceMockConsoleServerModuleProperties serverAppProperties, WebServiceMockConsoleClientAppProperties clientAppProperties) {
    }

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    @Override
    public Class<WebServiceMockConsoleClientAppProperties> getClientModulePropertiesClass() {
        return WebServiceMockConsoleClientAppProperties.class;
    }

    @Override
    public WebServiceMockConsoleServerModuleProperties getServerModuleProperties() {
        return ModulePropertiesContainer.instance().getModuleProperties(getModuleId());
    }

}

