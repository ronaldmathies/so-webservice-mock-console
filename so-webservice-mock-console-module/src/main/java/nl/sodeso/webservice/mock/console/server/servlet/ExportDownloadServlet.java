package nl.sodeso.webservice.mock.console.server.servlet;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.gwt.ui.client.util.ContentType;
import nl.sodeso.gwt.ui.server.servlet.AbstractFileDownloadServlet;
import nl.sodeso.gwt.ui.server.servlet.DownloadFile;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Ronald Mathies
 */
@WebServlet(
    name="Export Download Servlet",
    urlPatterns = { "*.webservicemock-export-download" }
)
public class ExportDownloadServlet extends AbstractFileDownloadServlet {

    @Override
    public DownloadFile download(HttpServletRequest request) throws ServletException {
        String file = request.getParameter("file");
        DownloadFile df = new DownloadFile();
        df.setFilename(file);
        df.setContentType(ContentType.APPLICATION_JSON);
        try {
            df.setInputStream(new FileInputStream(new File(FileUtil.getSystemTempFolderAsFile(), file)));
        } catch (IOException e) {

        }

        return df;
    }

}
