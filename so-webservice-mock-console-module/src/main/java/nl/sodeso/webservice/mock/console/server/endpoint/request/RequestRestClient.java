package nl.sodeso.webservice.mock.console.server.endpoint.request;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.commons.restful.client.RestClient;
import nl.sodeso.commons.restful.client.RestClientCallback;
import nl.sodeso.commons.restful.client.RestClientException;
import nl.sodeso.commons.restful.client.SimpleRestClientConfiguration;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequestView;
import nl.sodeso.webservice.mock.model.generic.JsStatus;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class RequestRestClient {

    private static final String WS_MOCK_GET_REQUEST = "/mock/request/%s/%s";
    private static final String WS_MOCK_GET_REQUESTS = "/mock/requests/%s";
    private static final String WS_MOCK_GET_REQUEST_FOR_RESPONSE = "/mock/requests/%s/%s/%s";
    private static final String WS_MOCK_GET_REQUESTS_FOR_RESPONSE = "/mock/requests/%s/%s";

    private static final String WS_MOCK_DELETE_REQUEST = "/mock/request/%s/%s";
    private static final String WS_MOCK_DELETE_REQUESTS_ALL = "/mock/requests/%s";
    private static final String WS_MOCK_DELETE_REQUEST_FOR_RESPONSE = "/mock/requests/%s/%s/%s";
    private static final String WS_MOCK_DELETE_REQUESTS_FOR_RESPONSE_ALL = "/mock/requests/%s/%s";

    public static boolean deleteRequest(String host, int port, String endpointName, String id) {
        boolean[] result = {false};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.delete(WS_MOCK_DELETE_REQUEST, new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    result[0] = true;
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    // This should not happen, the service always returns an OK.
                }
            }, endpointName, id);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return result[0];
    }

    public static boolean deleteRequestsAll(String host, int port, String endpointName) {
        boolean[] result = {false};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.delete(WS_MOCK_DELETE_REQUESTS_ALL, new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    result[0] = true;
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    // This should not happen, the service always returns an OK.
                }
            }, endpointName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return result[0];
    }

    public static boolean deleteRequest(String host, int port, String endpointName, String responseName, String id) {
        boolean[] result = {false};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.delete(WS_MOCK_DELETE_REQUEST_FOR_RESPONSE, new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    result[0] = true;
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    // This should not happen, the service always returns an OK.
                }
            }, endpointName, responseName, id);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return result[0];
    }

    public static boolean deleteRequestsAll(String host, int port, String endpointName, String responseName) {
        boolean[] result = {false};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.delete(WS_MOCK_DELETE_REQUESTS_FOR_RESPONSE_ALL, new RestClientCallback() {
                @Override
                public void onSuccess(javax.ws.rs.core.Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    result[0] = true;
                }

                @Override
                public void onFailure(javax.ws.rs.core.Response response) {
                    // This should not happen, the service always returns an OK.
                }
            }, endpointName, responseName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return result[0];
    }

    public static JsRequest getRequest(String host, int port, String endpointName, String id) {
        JsRequest[] jsRequest = {null};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_REQUEST, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException {
                    jsRequest[0] = readValue(response, JsRequestView.Detailed.class, new TypeReference<JsRequest>() {});
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName, id);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return jsRequest[0];
    }

    public static List<JsRequest> getRequests(String host, int port, String endpointName) {
        List<JsRequest> jsRequests = new ArrayList<>();

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_REQUESTS, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException {
                    jsRequests.addAll(readValue(response, JsRequestView.Summary.class, new TypeReference<List<JsRequest>>() {}));
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return jsRequests;
    }

    public static JsRequest getRequest(String host, int port, String endpointName, String responseName, String id) {
        JsRequest[] jsRequest = {null};

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_REQUEST_FOR_RESPONSE, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException {
                    jsRequest[0] = readValue(response, JsRequestView.Detailed.class, new TypeReference<JsRequest>() {});
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName, responseName, id);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return jsRequest[0];
    }

    public static List<JsRequest> getRequests(String host, int port, String endpointName, String responseName) {
        List<JsRequest> jsRequests = new ArrayList<>();

        try {
            RestClient restClient = new RestClient(new SimpleRestClientConfiguration(host, port));
            restClient.get(WS_MOCK_GET_REQUESTS_FOR_RESPONSE, new RestClientCallback() {
                @Override
                public void onSuccess(Response response) throws RestClientException {
                    jsRequests.addAll(readValue(response, JsRequestView.Summary.class, new TypeReference<List<JsRequest>>() {}));
                }

                @Override
                public void onFailure(Response response) {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                }
            }, endpointName, responseName);
        } catch (RestClientException e) {
            // Something went wrong, handle response.
        }

        return jsRequests;
    }

    private static <X> X readValue(Response response, Class<?> view, TypeReference typeReference) throws RestClientException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readerWithView(view)
                    .forType(typeReference)
                    .readValue(response.readEntity(String.class));
        } catch (IOException e) {
            throw new RestClientException(e, "Failed to read data.");
        }
    }

    private static Entity<String> writeValue(Class<?> view, Object value) throws RestClientException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return Entity.entity(mapper.writerWithView(view).writeValueAsString(value), MediaType.APPLICATION_JSON_TYPE);
        } catch (IOException e) {
            throw new RestClientException(e, "Failed to write data.");
        }
    }

}
