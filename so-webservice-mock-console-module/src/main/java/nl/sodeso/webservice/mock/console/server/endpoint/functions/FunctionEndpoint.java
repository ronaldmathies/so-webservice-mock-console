package nl.sodeso.webservice.mock.console.server.endpoint.functions;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.webservice.mock.console.client.application.response.functions.FunctionGroupOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.FunctionOption;
import nl.sodeso.webservice.mock.console.client.application.response.functions.model.Function;
import nl.sodeso.webservice.mock.console.client.application.response.functions.rpc.FunctionRpc;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.console.server.endpoint.convertor.FunctionConvertor;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.webservicemock-function")
public class FunctionEndpoint extends XsrfProtectedServiceServlet implements FunctionRpc {

    public ArrayList<FunctionGroupOption> groups(ServiceOption serviceOption) {
        ArrayList<String> groups = FunctionRestClient.groups(serviceOption.getIpAddress(), serviceOption.getPort());
        return groups.stream().map(FunctionGroupOption::new).collect(Collectors.toCollection(ArrayList::new));
    }

    public ArrayList<FunctionOption> functions(ServiceOption serviceOption, FunctionGroupOption functionGroupOption) {
        ArrayList<Function> functions = FunctionConvertor.toClient(FunctionRestClient.functions(serviceOption.getIpAddress(), serviceOption.getPort(), functionGroupOption.getCode()));
        return functions.stream().map(FunctionOption::new).collect(Collectors.toCollection(ArrayList::new));
    }

}
