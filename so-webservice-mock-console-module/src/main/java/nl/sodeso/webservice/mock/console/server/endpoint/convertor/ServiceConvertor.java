package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.commons.network.discovery.DiscoveredService;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.model.service.JsService;

/**
 * @author Ronald Mathies
 */
public class ServiceConvertor {

    public static ServiceOption toOption(JsService jsService, DiscoveredService discoveredService) {
        ServiceOption service = new ServiceOption(jsService.getName());
        service.setVersion(jsService.getVersion());
        service.setIpAddress(discoveredService.getInetAddress().getHostAddress());
        service.setPort(discoveredService.getPort());
        return service;
    }

}
