package nl.sodeso.webservice.mock.console.server.endpoint.convertor;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.webservice.mock.console.client.application.generic.HeaderOption;
import nl.sodeso.webservice.mock.console.client.application.generic.header.Header;
import nl.sodeso.webservice.mock.console.client.application.response.type.MessageResponseType;
import nl.sodeso.webservice.mock.model.generic.JsHeader;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class HeaderConvertor {

    public static List<JsHeader> toService(List<Header> headers) {
        List<JsHeader> jsHeaders = new ArrayList<>();

        for (Header header : headers) {
            String name = header.getHeaderOption().getValue().getCode();
            if (header.getHeaderOption().getValue().isOther()) {
                name = header.getOtherHeaderOption().getValue();
            }

            JsHeader jsHeader = new JsHeader(name, header.getValue().getValue());
            jsHeaders.add(jsHeader);
        }

        return jsHeaders;
    }

    public static List<Header> toClient(List<JsHeader> jsHeaders) {
        List<Header> headers = new ArrayList<>();

        for (JsHeader jsHeader : jsHeaders) {
            Header header = new Header();


            HeaderOption headerOption = HeaderOption.fromValue(jsHeader.getName());
            if (headerOption == null) {
                headerOption = HeaderOption.OTHER;
                header.setOtherHeaderOption(new StringType(jsHeader.getName()));
            } else {
                header.setOtherHeaderOption(new StringType());
            }

            header.setHeaderOption(new OptionType<>(headerOption));
            header.setValue(new StringType(jsHeader.getValue()));
            headers.add(header);
        }

        return headers;
    }

}
