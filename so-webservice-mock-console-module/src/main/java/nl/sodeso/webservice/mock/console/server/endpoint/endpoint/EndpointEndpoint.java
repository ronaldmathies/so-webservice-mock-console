package nl.sodeso.webservice.mock.console.server.endpoint.endpoint;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.webservice.mock.console.client.application.endpoint.Endpoint;
import nl.sodeso.webservice.mock.console.client.application.endpoint.EndpointSummaryItem;
import nl.sodeso.webservice.mock.console.client.application.endpoint.rpc.EndpointRpc;
import nl.sodeso.webservice.mock.console.client.application.service.ServiceOption;
import nl.sodeso.webservice.mock.console.server.endpoint.convertor.EndpointConvertor;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.webservicemock-endpoint")
public class EndpointEndpoint extends XsrfProtectedServiceServlet implements EndpointRpc {

    public ArrayList<EndpointSummaryItem> findSummaries(ServiceOption serviceOption) {
        return EndpointConvertor.toClientSummaries(EndpointRestClient.retreiveAllEndpoints(serviceOption.getIpAddress(), serviceOption.getPort()));
    }

    public Endpoint getDetails(ServiceOption serviceOption, String name) {
        JsEndpoint jsEndpoint = EndpointRestClient.retreiveEndpointByName(serviceOption.getIpAddress(), serviceOption.getPort(), name);
        if (jsEndpoint != null) {
            return EndpointConvertor.toClient(jsEndpoint);
        }

        return null;
    }

    public ArrayList<DefaultOption> asOptions(ServiceOption serviceOption) {
        return EndpointConvertor.toOptions(EndpointRestClient.retreiveAllEndpoints(serviceOption.getIpAddress(), serviceOption.getPort()));
    }

    public ValidationResult isNameUnique(ServiceOption serviceOption, String name) {
        ValidationResult result = new ValidationResult();

        JsEndpoint jsEndpoint = EndpointRestClient.retreiveEndpointByName(serviceOption.getIpAddress(), serviceOption.getPort(), name);
        if (jsEndpoint != null) {
            result.add(new ValidationMessage("name", ValidationMessage.Level.ERROR, "The name is already in use."));
        }

        return result;
    }

    public ValidationResult isPathUnique(ServiceOption serviceOption, String path) {
        ValidationResult result = new ValidationResult();

        JsEndpoint jsEndpoint = EndpointRestClient.retreiveEndpointByPath(serviceOption.getIpAddress(), serviceOption.getPort(), path);
        if (jsEndpoint != null) {
            result.add(new ValidationMessage("path", ValidationMessage.Level.ERROR, "The path is already in use or conflicts with another path."));
        }

        return result;
    }

    public EndpointSummaryItem save(ServiceOption serviceOption, Endpoint endpoint) throws RemoteException {
        if (endpoint.isExisting()) {
            EndpointRestClient.updateEndpoint(serviceOption.getIpAddress(), serviceOption.getPort(), endpoint.getName().getOriginalValue(), EndpointConvertor.toService(endpoint));
        } else {
            EndpointRestClient.addEndpoint(serviceOption.getIpAddress(), serviceOption.getPort(), EndpointConvertor.toService(endpoint));
        }

        return EndpointConvertor.toClientSummary(EndpointRestClient.retreiveEndpointByName(serviceOption.getIpAddress(), serviceOption.getPort(), endpoint.getName().getValue()));
    }

    public EndpointSummaryItem duplicate(ServiceOption serviceOption, String name, String newName, String newPath) {
        JsEndpoint jsEndpoint = EndpointRestClient.retreiveEndpointByName(serviceOption.getIpAddress(), serviceOption.getPort(), name);
        jsEndpoint.setName(newName);
        jsEndpoint.setPath(newPath);

        EndpointRestClient.addEndpoint(serviceOption.getIpAddress(), serviceOption.getPort(), jsEndpoint);
        return EndpointConvertor.toClientSummary(jsEndpoint);
    }

    public ValidationResult canBeDeletedSafely(ServiceOption serviceOption, String name) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public void delete(ServiceOption serviceOption, String name) {
        EndpointRestClient.deleteEndpoint(serviceOption.getIpAddress(), serviceOption.getPort(), name);
    }
}
