package nl.sodeso.webservice.mock.console.server.broadcast;

import nl.sodeso.commons.network.discovery.multicast.MulticastClientConfiguration;
import nl.sodeso.commons.network.discovery.multicast.AbstractMulticastClientWebInitContext;
import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.webservice.mock.console.client.Constants;
import nl.sodeso.webservice.mock.console.server.endpoint.properties.ModulePropertiesContainer;

/**
 * @author Ronald Mathies
 */
public class MulticastClientWebInitContext extends AbstractMulticastClientWebInitContext {

    @Override
    public MulticastClientConfiguration getConfiguration() {
        return new MulticastClientConfiguration() {
            @Override
            public boolean isEnabled() {
                return PropertyConfiguration.getInstance().getBooleanProperty(Constants.MODULE_ID, ModulePropertiesContainer.DISCOVERY_MULTICAST_ENABLED, false);
            }

            @Override
            public String getServiceId() {
                return PropertyConfiguration.getInstance().getStringProperty(Constants.MODULE_ID, ModulePropertiesContainer.DISCOVERY_SERVICE_ID, null);
            }

            @Override
            public String getMulticastGroup() {
                return PropertyConfiguration.getInstance().getStringProperty(Constants.MODULE_ID, ModulePropertiesContainer.DISCOVERY_MULTICAST_GROUP, null);
            }

            @Override
            public int getMulticastPort() {
                return PropertyConfiguration.getInstance().getIntegerProperty(Constants.MODULE_ID, ModulePropertiesContainer.DISCOVERY_MULTICAST_PORT, 0);
            }
        };
    }
}
